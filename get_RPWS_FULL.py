import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime, timedelta
import glob
import numpy as np
import pandas as pd
import time_conversions as tc

#SENSOR / ANTENNA DEFINITION
#0 = Ex, electric dipole X-direction
#1 = Eu, electric U-direction (aka Ex+) (WFR only)
#2 = Ev, electric V-direction (aka Ex-) (WFR only)
#3 = Ew, electric W-direction (aka Ez)
#4 = Bx, magnetic X-direction
#5 = By, magnetic Y-direction (WFR only)
#6 = Bz, magnetic Z-direction (WFR only)
#8 = HF, HFR downconvert (WBR only)
#11 = LP, Langmuir probe sphere
#15 = unknown, antenna cannot be determined"

class FULLRPWSdata(object):
    
    def __init__(self):
        self.rpwsfullpath = '{}/RPWS/CO-V_E_J_S_SS-RPWS-3-RDR-LRFULL-V1.0/DATA/RPWS_LOW_RATE_FULL'.format(datapath)
         
    def cleardata(self):
        self.nameiter = iter(range(1000))
        self.filename = {}
        self.dt = {}
        self.detector = {}
        self.fcent = {}
        self.specdens = {}
        self.receiver = {}
        
    def getFULLRPWS(self, ydoylist):
        print('Getting FULL RPWS data...')
        files = []
        for ydoy in ydoylist:
            tmp = glob.glob('{}/T{}XX/T{}/*.dat'.format(self.rpwsfullpath, ydoy[:5], ydoy))
            files = np.append(files, tmp)
            
        self.cleardata()
        for fff in files:
            thisname = str(next(self.nameiter))
            
            lblfile = '{}.lbl'.format(fff[:-4])
            with open(lblfile, 'r') as f:
                while True:
                    txt = f.readline()
                    if not 'RECORD_BYTES' in txt:
                        continue
                    else:
                        break
                row_bytes = int(txt.split('=')[-1])
                
            
#            row_bytes = 400
            fd = open(fff,'rb')
            
            # read top line (SKIP)
            dt_head = np.dtype('S{}'.format(row_bytes))
            np.fromfile(fd, count=1, sep="", dtype=dt_head)
            
            # common datatype
            dt = np.dtype([
                        ('SCLK_SECOND', '>u4', 1),
                        ('SCLK_PARTITION', '>u1', 1),
                        ('SCLK_FINE', '>u1', 1),
                        ('SCET_DAY', '>u2', 1),
                        ('SCET_MILLISECOND', '>u4', 1),
                        ('Dq', '>u1', 3), # this is wrong
                        ('Sensor', 'B', 1),
                        ('Data', '>f4', int((row_bytes-16)/4)),
                        ])
            
            # read time offset (SKIP)
            np.fromfile(fd, count=1, sep="", dtype=dt)
            
            # read central frequencies
            data = np.fromfile(fd, count=1, sep="", dtype=dt)
            self.fcent[thisname] = np.squeeze(data['Data'])
                        
            # read data     
            data = np.fromfile(fd, count=-1, sep="", dtype=dt)
            
            # get datetimes
            times = np.array([datetime(1958,1,1)+
                     timedelta(days=int(data['SCET_DAY'][iii]))+
                     timedelta(seconds=float(data['SCET_MILLISECOND'][iii]/1000))
                     for iii in range(len(data['SCET_DAY']))])
            self.dt[thisname] = times
            
            # get detector name
            self.detector[thisname] = data['Sensor']
            
            # get spectral densities
            self.specdens[thisname] = data['Data']
            
            # get receiver name
            self.receiver[thisname] = fff.split('_')[-1][:3]

## insert nan in gaps
#tmp = np.where(np.diff(times)>5*np.mean(np.diff(times)))[0]
#ins_times = times[tmp-1]+(times[tmp+1]-times[tmp-1])/2
#times = np.insert(times, tmp, ins_times)
#specdens = np.insert(specdens, tmp, np.full((specdens.shape[1]), np.nan), axis=0)

#plt.pcolormesh(times, fcent, np.log10(specdens).T)

if __name__ == "__main__":
    rp = FULLRPWSdata()
    rp.getFULLRPWS(['2017009'])
    
    import matplotlib.pyplot as plt
    import matplotlib.colors as mcolors
    for det in [0,4]:
        fig, ax = plt.subplots()
        for nnn in [*rp.specdens]:
            valid = np.where(rp.detector[nnn] == det)[0]
            if len(valid):
                times = rp.dt[nnn][valid]
                specdens = rp.specdens[nnn][valid,:]
                specdens[specdens<=0] = 1e-15
                # insert nan in gaps
                diff = np.median(np.diff(times))
                tmp = np.where(np.diff(times)>2*diff)[0]
                lo = times[tmp]+diff
                hi = times[tmp+1]-diff
                ins_times = [val for pair in zip(lo, hi) for val in pair]
                times = np.insert(times, np.repeat(tmp, 2)+1, ins_times)
                specdens = np.insert(specdens, np.repeat(tmp, 2), np.full((specdens.shape[1]), np.nan), axis=0)
                quad = ax.pcolormesh(times, rp.fcent[nnn], specdens.T)
                quad.set_norm(mcolors.LogNorm(1e-10,1e-5))
        ax.set_yscale('log')
        ax.set_xlim([datetime(2017,1,9,10), datetime(2017,1,9,14)])
        ax.set_title('Detector {}'.format(int(det)))
        plt.show()
    
    