# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
boxpath = pr.boxpath
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime
import get_image
import glob
import matplotlib.cm
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import numpy as np
import os
import remove_solar_reflection
import scipy.signal as sgn
import time_conversions as tc
#import uvisdb
#udb = uvisdb.UVISDB(update=False)
#imglist = udb.currentDF

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

def radiusSaturn(colat):
    x = (1100+60268)*np.sin(colat)
    y = (1100+54364)*np.cos(colat)
    return np.sqrt(x**2+y**2)*1e3 #m

# at 20 deg colat
deg2km = radiusSaturn(np.radians(20))*2*np.pi/360/1e3

dpi_hi = 300
dpi_lo = 120
dpi_val = [dpi_hi, dpi_lo]

import pandas as pd
picklefile = '{}/imglists/UVIS_current_pickle'.format(boxpath)
imglist = pd.read_pickle(picklefile)

cd = cassinipy.CassiniData(datapath)
rsr = remove_solar_reflection.RemoveSolarReflection()
samplepath = r'{}\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'.format(datapath)

#%%
# =============================================================================
# get pixel sizes
# =============================================================================
def getLength(dist, angle):
    return dist*np.sin(angle)

distHST = np.sqrt(1.671e18) #km
distUVIS = np.array([1.6,2,5,25,50,12.5])*60268 #km
angHST = 24/1024 #arcsec
angHST = angHST/3600/360*2*np.pi #rad
angUVIS = 1e-3 #rad

print(getLength(distUVIS, angUVIS))
print(getLength(distHST, angHST))

#%%    
def makeplot(ax, samplefile, imagedata=None, cax=None, cbarh=False,
             PPO=False, orbit=False,
             tmin=0, tmax=360, rmin=0, rmax=30, ytitle=1.02,
             KR_MIN=0.5, KR_MAX=30, tickright=False,
             profilelong=None, profilec='k', profilegap=[15,15],
             profilediff=2.5, markmoreLTs=False):
    fmt = '%Y_%jT%H_%M_%S'
    thisdt = datetime.strptime(samplefile.split('\\')[-1].split('/')[-1].split('.')[0], fmt)
    thiset = tc.datetime2et(thisdt)
    thisind = (imglist['ET_START']-thiset).abs().argmin()
    
    if imagedata is None:
        data, _, _ = get_image.getRedUVIS(samplefile)
    else:
        data = imagedata
    data[data<0.1] = 0.1
    thisimg = imglist.loc[thisind]
    fmt = '%Y-%j, %H:%M:%S'
    title = '{}\n{}$\,$s, {} ({:.2f} R$_S$)'.format(datetime.strftime(thisdt, fmt),
             thisimg['EXP'], thisimg['HEMISPHERE'], thisimg['POS_KRTP_R']-1)
    
    # plot
    lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
    loncent = lonbins[:-1] + np.diff(lonbins)/2
    colatcent = colatbins[:-1] + np.diff(colatbins)/2
    quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_facecolor('gray')
    
    # plot colorbar
    if not (cax is None):
        if cbarh:
            cbar = plt.colorbar(quad, cax=cax, extend='both', orientation='horizontal')
            cbar.set_label('Intensity (kR)', labelpad=10, fontsize=14)
        else:
            cbar = plt.colorbar(quad, cax=cax, extend='both')
            cbar.set_label('Intensity (kR)', labelpad=10, fontsize=14, rotation=270)
        cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,KR_MAX+1,10)))
        cbar.ax.tick_params(labelsize=12)
        
    if markmoreLTs:
        ticks = np.arange(0, 2.0001*np.pi, np.pi/4)
        ticklabels = np.array(['{:02d}'.format(iii) for iii in np.arange(0, 24, 3)])
    else:
        ticks = np.arange(0, 2.0001*np.pi, np.pi/2)
        ticklabels = np.array(['{:02d}'.format(iii) for iii in np.arange(0, 24, 6)])
    for iii in range(len(ticklabels)):
        tmp = np.degrees(ticks[iii])
        if tmp > tmax:
            continue
        else:
            if tmp < tmin%360 and not tmp==tmax%360:
                continue
        txt = ax.text(ticks[iii], rmax-2 if markmoreLTs else rmax-3,
                      ticklabels[iii], color='w', fontsize=18 if markmoreLTs else 14, fontweight='bold',
                      ha='center',va='center', zorder=5)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
            
    # PPO phase angle
    if PPO:
        ppo_n = thisimg['PPO_PHASE_N']
        ppo_s = thisimg['PPO_PHASE_S']
        if np.isfinite(ppo_n):
            ax.plot([ppo_n,ppo_n], [0,30], color=myred, lw=2, zorder=5)
            ax.plot([ppo_n,ppo_n], [0,30], color='w', lw=3, zorder=3)
            txt = ax.text(ppo_n+0.12, 27, 'N', color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center', zorder=7)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myred)])
        if np.isfinite(ppo_s):
            ax.plot([ppo_s,ppo_s], [0,30], color=myblue, lw=2, zorder=5)
            ax.plot([ppo_s,ppo_s], [0,30], color='w', lw=3, zorder=3)
            txt = ax.text(ppo_s+0.12, 27, 'S', color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center', zorder=7)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myblue)])
        
    # Cassini orbit
    if orbit:
        delta = 3600*24*1
        _, colat_n, colat_s, loct = cd.get_ionfootp(np.linspace(thiset-delta, thiset+delta, num=200))
        ax.plot(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s,
                c='w', ls='-', lw=1.5, zorder=2)
        # Cassini orbit direct
        _, colat_n, colat_s, loct = cd.get_ionfootp(np.linspace(thisimg['ET_START'],
                                                                thisimg['ET_STOP'], num=100))
        ax.plot(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s,
                c='crimson', ls='-', lw=1.5, zorder=3)
        # arrow
        tmp = (thisimg['ET_START']+thisimg['ET_STOP'])/2
        tmp = [tmp-120, tmp+120]
        _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
        colat = colat_n if thisimg['HEMISPHERE']=='North' else colat_s
        arrowstyle = '->,head_length=9, head_width=3.75'
        ax.add_patch(patches.FancyArrowPatch((loct[0]/12*np.pi, colat[0]),
                                           (loct[1]/12*np.pi, colat[1]),
                                           arrowstyle=arrowstyle,
                                           color='crimson',
                                           linewidth=2,
                                           zorder=3))
#        # Cassini footprint
#        _, colat_n, colat_s, loct = cd.get_ionfootp([thiset])
#        ax.scatter(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s,
#                   color='k', edgecolor='gold', marker='D', lw=2, zorder=5)
    
    
        # Cassini location print
        _, rs, lat, loct = cd.get_locations([thiset], refframe='KRTP')
        ax.text(0, 0, '{0:.2f} Rs\n{1:.2f}$^\circ$ {2}\n{3:.2f} LT'.format(
                        rs, np.abs(lat), thisimg['HEMISPHERE'][0], loct),
                transform=ax.transAxes)
        
    # do profiles
    if not (profilelong is None):
        plon = np.radians(profilelong)
        for iii in [[0, profilegap[0]], [profilegap[1], 30]]:
            if profilediff<2:
                ax.plot([plon,plon], iii, color=profilec, lw=2.5, zorder=5)
                ax.plot([plon,plon], iii, color='w', lw=3.5, zorder=3)
            else:
                lonrange = np.linspace(plon-np.radians(profilediff),
                                       plon+np.radians(profilediff),
                                       num=30)
                ax.fill_between(lonrange,
                                np.full_like(lonrange, iii[0]),
                                np.full_like(lonrange, iii[1]),
                                color=profilec,
                                alpha = 0.7,
                                zorder=5)
        # all longitudes within +-x° longitude
        DIFF = np.radians(profilediff)
        tmp = np.where((np.abs(loncent-plon)<DIFF) |
                        (np.abs(loncent-plon)>(2*np.pi-DIFF)))[0]
        profile = np.nanmean(data[tmp, :], axis=0)
    else:
        profile = None
    
    ax.set_xticks(ticks)
    ax.set_xticks(np.linspace(0, 2*np.pi, 25), minor=True)
    ax.set_xticklabels([])
    
    ax.set_yticks([10,20,30])
    ax.set_yticks([5,15,25], minor=True)
    if tmin==0 and tmax==360:
        ax.set_yticklabels([])
    else:
        ax.set_yticklabels(['$10^\circ$','$20^\circ$'])
        if tickright:
            ax.yaxis.tick_right()
    ax.grid('on', color='0.8', which='major',
            linewidth=1 if tmin==0 and tmax==360 else 1.2)
    ax.grid('on', color='0.8', linestyle='--', which='minor',
            linewidth=0.5 if tmin==0 and tmax==360 else 0.8)

    ax.set_rorigin(0)
    ax.set_rmax(rmax)
    ax.set_rmin(rmin)
    ax.set_theta_zero_location("N")
    ax.set_thetamin(tmin)
    ax.set_thetamax(tmax)
    
    if not ytitle is None:
        ax.set_title(title, y=ytitle)
        
    return profile, colatcent
        
#%%
# =============================================================================
# All single images 
# =============================================================================
            
if False:
    savepath = '{}/UVIS_low_passes/all_orbit'.format(plotpath)
    if not os.path.exists(savepath):
        os.makedirs(savepath)
        
    allfiles = glob.glob('{}/*.fits'.format(samplepath))
    
    for fctr in range(len(allfiles)):
        thisfile = allfiles[fctr]
        
        fig = plt.figure()
        fig.set_size_inches(7, 7)
        gs = gridspec.GridSpec(1,2, width_ratios=(1,)+(0.04,), wspace=0.07)
        
        ax = plt.subplot(gs[0,0], projection='polar')
        cax = plt.subplot(gs[0,-1])
        
        makeplot(ax, thisfile, cax=cax, 
                 PPO=True, orbit=True,
                 ytitle=1.02,
                 KR_MIN=0.1, KR_MAX=30)
        
        t = thisfile.split('\\')[-1].split('/')[-1].split('.')[0]
        plt.savefig('{}/{}_orbit.png'.format(savepath, t), bbox_inches='tight', dpi=300)
        plt.close()
            
    
#%%
savepath = '{}/UVIS_low_passes'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)
        
#%%
# =============================================================================
# Pre-dawn arc
# =============================================================================
        
cm = matplotlib.cm.get_cmap('inferno')
colorlist = [cm(iii) for iii in np.linspace(0.1, 0.8, num=5)]

if True:    
    files = ['2017_030T17_02_47.fits',
             '2017_073T17_20_42.fits',
             '2017_167T21_17_11.fits',
             '2017_219T19_20_58.fits',
             '2017_232T10_37_35.fits',
             '2017_245T16_53_09.fits']
    
    cl = iter(colorlist)
    
    fig = plt.figure()
    mult = 4
    fig.set_size_inches(mult*3.3, mult*3.6)
    gs = gridspec.GridSpec(5, 7, width_ratios=(0.5,)*6+(0.08,), wspace=0.3, hspace=0.15,
                           height_ratios=(1,0.05,1,0.66,0.66))
    
    bx1 = plt.subplot(gs[-2, :3])
    bx2 = plt.subplot(gs[-2, 3:6])#, sharex=bx1, sharey=bx1)
    bx3 = plt.subplot(gs[-1, :3])#, sharex=bx1, sharey=bx1)
    bx4 = plt.subplot(gs[-1, 3:6])#, sharex=bx1, sharey=bx1)
    
    bxs = [bx1, bx2, bx3, bx4]
    bxlet = 'ghijkl'
    bxctr = 0
    pg1 = [0,12,15,17,15,17]
    pg2 = [0,19,21,23,22,23]
    outer_eq = [0, 19.5, 20, 23.8, 21, 23]
    
    for panel in range(0,len(files)):
        if panel:
            thisc = next(cl)
        else:
            thisc = 'k'
        row = panel // 3
        col = panel % 3
        if row==1: row=2
        ax = plt.subplot(gs[row,2*col:2*col+2], projection='polar')
        
        samplefile = r'{}\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(datapath,
                       files[panel]) 
        
        if not panel:
            cax = plt.subplot(gs[:2,-1])
        else:
            cax = None
        p, c = makeplot(ax, samplefile, cax=cax, #cbarh=True, 
                         rmin=10, rmax=25,
                         tmin=10, tmax=80,
                         ytitle=1,
                         KR_MIN=0.1, KR_MAX=30, tickright=False,
                         profilelong=None if not panel else (45 if panel<3 else 37.5),
                         profilec=thisc,
                         profilediff=5,
                         profilegap=[pg1[panel], pg2[panel]],
                         markmoreLTs=True)
        
        txt = ax.text(0, 1.1, '({})'.format('abcdefghijklmnop'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
        if panel:
            bx = bxs[bxctr]
            bx.plot(c, p, color=thisc,
                    label=files[panel].split('T')[0].replace('_','-'))
            bx.axvline(outer_eq[panel], color=thisc, linestyle='--', linewidth=1.8)
            
            bx.set_xlim([9, 25])
            bx.set_yscale('log')
            if bxctr in [1,3]:
                bx.set_ylim([0.1, 20])
            if bxctr==0:
                bx.set_ylim([0.5, 60])
            if bxctr > 1:
                bx.set_xlabel('Colatitude (deg)', fontsize=12)
            if not bxctr % 2:
                bx.set_ylabel('Intensity (kR)', fontsize=12)
            else:
                bx.yaxis.set_label_position('right')
                bx.set_ylabel('Intensity (kR)', rotation=270, labelpad=15, fontsize=12)
            bx.legend(loc=1, shadow=True, facecolor='w', edgecolor='k',
                      fancybox=True, fontsize=11)
            
            bx.tick_params(axis='both', which='both', direction='in',
                           labelsize=11,
                           left=True, labelleft=False if bxctr%2 else True,
                           right=True, labelright=True if bxctr%2 else False,
                           top=True, labeltop=False,
                           bottom=True, labelbottom=True if bxctr>1 else False)
            
            txt = bx.text(0.015, 0.96, '({})'.format(bxlet[bxctr]),
                      transform=bx.transAxes, ha='left', va='top',
                      color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
            txt = bx.text(0.10, 0.96, '{}'.format('South' if bxctr in [1,3] else 'North'),
                      transform=bx.transAxes, ha='left', va='top',
                      color='k', fontsize=15)
            if panel>1: # first two plots in one panel
                bxctr += 1

    for dpi in dpi_val:
        plt.savefig('{}/new_pre_dawn_arc_{}.png'.format(savepath, dpi), bbox_inches='tight', dpi=dpi)
    plt.close()
sys.exit()
#%%
# =============================================================================
# Dawn-dusk full images N and S (6)
# =============================================================================
if False:
    files = ['2017_080T17_03_11.fits',
             '2017_102T08_21_12.fits',
             '2017_167T19_04_16.fits',
             '2017_232T08_28_24.fits',
             '2017_239T07_03_49.fits',
             '2017_245T16_53_09.fits']
    
    fig = plt.figure()
    mult = 4.125
    fig.set_size_inches(mult*3.2, mult*2.2)
    gs = gridspec.GridSpec(2,4, width_ratios=(1,)*3+(0.08,), wspace=0.1)
    
    for panel in range(0,len(files)):
        row = panel // 3
        col = panel % 3
        ax = plt.subplot(gs[row,col], projection='polar')
        
        samplefile = r'{}\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(
                datapath, files[panel]) 
        
        if not panel:
            cax = plt.subplot(gs[:,-1])
        else:
            cax = None
        makeplot(ax, samplefile, cax=cax,
                 ytitle=1,
                 KR_MIN=0.5, KR_MAX=30, tickright=False)
                
        txt = ax.text(0, 1.05, '({})'.format('abcdefghijklmnop'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
    for dpi in dpi_val:
        plt.savefig('{}/dawn_dusk_comp_N_S_{}.png'.format(savepath, dpi), bbox_inches='tight', dpi=dpi)
    plt.close()
    
        
#%%
# =============================================================================
# Dusk zoom images
# =============================================================================
if True:
    files = ['2017_009T12_49_40.fits',
#             '2017_074T00_45_22.fits',
             '2017_168T03_04_14.fits',
             '2017_219T08_30_23.fits',
#             '2017_219T20_34_12.fits',
             '2017_226T06_49_52.fits',
             '2017_239T04_52_23.fits',
             '2017_245T15_45_22.fits',
             '2017_252T02_46_23.fits',
             '2017_252T04_00_35.fits']
    
    cm = matplotlib.cm.get_cmap('inferno')
    colorlist = [cm(iii) for iii in np.linspace(0.25, 0.8, num=4)]
    
    cl = iter(colorlist)
    profiles = [None, None, 305, None,
                None, 272, 325.5, 277.5]
         
    fig = plt.figure()
    mult = 3.143
    fig.set_size_inches(mult*4.2, mult*4)
    gs = gridspec.GridSpec(4,5, width_ratios=(1,)*4+(0.08,), wspace=0.1, hspace=0.1,
                           height_ratios=(1,1,0.66,0.66))
    bx1 = plt.subplot(gs[-2, :2])
    bx2 = plt.subplot(gs[-2, 2:4])#, sharex=bx1, sharey=bx1)
    bx3 = plt.subplot(gs[-1, :2])#, sharex=bx1, sharey=bx1)
    bx4 = plt.subplot(gs[-1, 2:4])#, sharex=bx1, sharey=bx1)
    
    bxs = [bx1, bx2, bx3, bx4]
    bxlet = 'ijkl'
    bxctr = 0
    
    for panel in range(0,len(files)):
        if not (profiles[panel] is None):
            thisc = next(cl)
        else:
            thisc = 'k'
        row = panel // 4
        col = panel % 4
        ax = plt.subplot(gs[row,col], projection='polar')
        
        samplefile = r'{}\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(datapath,
                       files[panel])
        
        if not panel:
            cax = plt.subplot(gs[:2,-1])
        else:
            cax = None
        p, c = makeplot(ax, samplefile, cax=cax, rmin=7, rmax=27,
                        tmin=240, tmax=360, ytitle=1,
                        KR_MIN=0.1, KR_MAX=30, tickright=False,
                        profilelong=profiles[panel],
                        profilec=thisc,
                        profilegap=[9,19] if panel in [2,7] else [11,19],
                        profilediff=0.1 if panel==5 else 2.5)
                
        txt = ax.text(0, 1.1, '({})'.format('abcdefghijklmnop'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
        if not (profiles[panel] is None):
            bx = bxs[bxctr]
            bx.plot(c, p, color=thisc,
                    label=files[panel].split('T')[0].replace('_','-'))
            
            if bxctr in [0,3]: #find parallel arcs and mark them
                peaks, _ = sgn.find_peaks(p, prominence=3)
                for iii in range(4):
                    bx.axvline(c[peaks[iii]],
                               color=thisc, linestyle=':', linewidth=1.8)
                if bxctr==3:
                    pass #highlight outer emission peak
            elif bxctr==1: # determine peak width
#                halfmax = np.nanmax(p)/2
#                tmp = np.where(p>halfmax)[0]
#                width = c[tmp[-1]] - c[tmp[0]]
                bxins = inset_axes(bx, #width="30%", height="60%", loc=9,
                                   width="100%", height="100%", loc=9,
                                   bbox_to_anchor=(.4, .35, .3, .63),
                                   bbox_transform=bx.transAxes)
#                bxins.fill_between(c[tmp], p[tmp], color=thisc, alpha=0.3)
                bxins.tick_params(axis='both', which='both', direction='in',
                                  labelsize=8,
                                  left=True, labelleft=True,
                                  right=True, labelright=False,
                                  top=True, labeltop=False,
                                  bottom=True, labelbottom=True)
                bxins.plot(c, p, color=thisc)
                bxins.set_xlim([13.9,14.6])
                tmp = bxins.get_ylim()
                bxins.set_ylim([0, tmp[1]])
            if bxctr in [2,3]:
                eqb = 23.2 if bxctr==2 else 22.5
                bx.axvline(eqb, color=thisc, linestyle='--', linewidth=1.8)
                
            
            bx.set_xlim([9, 25])
            tmp = bx.get_ylim()
            bx.set_ylim([0, tmp[1]])
            if bxctr > 1:
                bx.set_xlabel('Colatitude (deg)', fontsize=12)
            if not bxctr % 2:
                bx.set_ylabel('Intensity (kR)', fontsize=12)
            else:
                bx.yaxis.set_label_position('right')
                bx.set_ylabel('Intensity (kR)', rotation=270, labelpad=18, fontsize=12)
            bx.legend(loc=1, shadow=True, facecolor='w', edgecolor='k',
                      fancybox=True, fontsize=11)
            
            bx.tick_params(axis='both', which='both', direction='in',
                           labelsize=11,
                           left=True, labelleft=False if bxctr%2 else True,
                           right=True, labelright=True if bxctr%2 else False,
                           top=True, labeltop=False,
                           bottom=True, labelbottom=True if bxctr>1 else False)
            
            txt = bx.text(0.015, 0.96, '({})'.format(bxlet[bxctr]),
                      transform=bx.transAxes, ha='left', va='top',
                      color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
            txt = bx.text(0.10, 0.96, '{}'.format('South' if bxctr else 'North'),
                      transform=bx.transAxes, ha='left', va='top',
                      color='k', fontsize=15)
            bxctr += 1

    for dpi in dpi_val:
        plt.savefig('{}/new_dusk_zoom_{}.png'.format(savepath, dpi), bbox_inches='tight', dpi=dpi)
    plt.close()
    
sys.exit()

#%%
# =============================================================================
# Outer emission short (compressed magnetosphere)
# =============================================================================
if True:
    
    cm = matplotlib.cm.get_cmap('inferno')
    colorlist = [cm(iii) for iii in np.linspace(0.25, 0.8, num=4)]
    
    files1 = ['2017_023T05_00_22.fits',
              '2017_023T07_27_04.fits',
              '2017_023T08_53_01.fits',
              ]
        
    fig = plt.figure()
    mult = 4.375
    fig.set_size_inches(mult*2.2, mult*2)
    gs = gridspec.GridSpec(2,3, width_ratios=(1,1,0.05,), wspace=0.1, hspace=0.25)
    
    paniter = iter('abcdefghijklmnop')
    
    images = []
    for panel in range(0,len(files1)):
        ax = plt.subplot(gs[panel//2,panel%2], projection='polar')
        
        samplefile = r'{}\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(
                datapath, files1[panel]) 
        rsr.calculate(samplefile)
        image = np.copy(rsr.redImage_aur)
        
        if not panel:
            images = np.full((3,)+image.shape, np.nan)
        images[panel] = image
            
        if not panel:
            cax = plt.subplot(gs[0,-1])
        else:
            cax = None
        makeplot(ax, samplefile, imagedata=image, 
                 cax=cax, cbarh=False,
                 ytitle=1.05,
                 KR_MIN=0.1, KR_MAX=30,
                 #PPO=True if not fff else False)
                 PPO=False)
        
        txt = ax.text(0, 1.15, '({})'.format(next(paniter)),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
     
    lonbins = np.linspace(0, 360, num=np.shape(image)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(image)[1]+1)
    data = np.nanmedian(images, axis=(0,1))
    data_dawn = np.nanmedian(images[:,:int(images.shape[1]/2),:], axis=(0,1))
    data_dusk = np.nanmedian(images[:,int(images.shape[1]/2):,:], axis=(0,1))
    ax = plt.subplot(gs[1,1])
    colats = colatbins[:-1] + np.diff(colatbins)/2
    ax.plot(colats, data, color='k', label='total')
#    ax.plot(colats, data_dawn, color=colorlist[1], label='dawn')
#    ax.plot(colats, data_dusk, color=colorlist[3], label='dusk')
#    ax.legend(loc=1, shadow=True, facecolor='w', edgecolor='k',
#                      fancybox=True, fontsize=11)
    ax.set_xlim([6,22])
    ax.set_ylim([0, ax.get_ylim()[1]])
    ax.set_ylabel('Median intensity (kR)', rotation=270, labelpad=15, fontsize=12)
    ax.set_xlabel('Colatitude (deg)', fontsize=12)
    ax.yaxis.set_label_position('right')
    ax.tick_params(axis='both', which='both', direction='in',
                   labelsize=11,
                   left=True, labelleft=False,
                   right=True, labelright=True,
                   top=True, labeltop=False,
                   bottom=True, labelbottom=True)
    txt = ax.text(0, 1.15, '({})'.format(next(paniter)),
              transform=ax.transAxes, ha='center', va='center',
              color='k', fontweight='bold', fontsize=15)
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
    
    for dpi in dpi_val:
        plt.savefig('{}/new_outer_emission_comp_2017_023_{}.png'.format(savepath, dpi), bbox_inches='tight', dpi=dpi)
    plt.close()
    
    

#%%
# =============================================================================
# Outer emission another take
# =============================================================================
if False:
    files1 = ['2017_023T05_00_22.fits',
              '2017_023T07_27_04.fits',
              '2017_023T08_53_01.fits',
              ]
    
    files2 = [#'2017_167T19_04_16.fits',
              '2017_059T08_20_53.fits',
              '2017_232T08_28_24.fits',
#              '2016_316T11_20_16.fits',
              '2017_245T19_00_42.fits',
              '2017_252T05_45_16.fits']
    
    fig = plt.figure()
    mult = 4.375
    fig.set_size_inches(mult*3.2, mult*2)
    gs = gridspec.GridSpec(4,4, height_ratios=(1,0.3,1,0.08,), wspace=0.05)
    
    paniter = iter('abcdefghijklmnop')
    
    for fff in range(2):
        files = files1 if not fff else files2
        images = []
        for panel in range(0,len(files)):
            ax = plt.subplot(gs[fff if fff==0 else fff+1,panel], projection='polar')
            
            samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(files[panel]) 
            rsr.calculate(samplefile)
            image = np.copy(rsr.redImage_aur)
            
            if not fff:
                if not panel:
                    images = np.full((3,)+image.shape, np.nan)
                images[panel] = image
                
            if not panel and not fff:
                cax = plt.subplot(gs[-1,1:3])
            else:
                cax = None
            makeplot(ax, samplefile, imagedata=image, 
                     cax=cax, cbarh=True,
                     ytitle=1.05,
                     KR_MIN=0.1, KR_MAX=30,
                     #PPO=True if not fff else False)
                     PPO=False)
            
            txt = ax.text(0, 1.15, '({})'.format(next(paniter)),
                      transform=ax.transAxes, ha='center', va='center',
                      color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
         
        if not fff:
            lonbins = np.linspace(0, 360, num=np.shape(image)[0]+1)
            colatbins = np.linspace(0, 30, num=np.shape(image)[1]+1)
            data = np.nanmedian(images, axis=(0,1))
            data_dawn = np.nanmedian(images[:,:int(images.shape[1]/2),:], axis=(0,1))
            data_dusk = np.nanmedian(images[:,int(images.shape[1]/2):,:], axis=(0,1))
            ax = plt.subplot(gs[0,-1])
            colats = colatbins[:-1] + np.diff(colatbins)/2
            ax.plot(colats, data, color='k', label='total')
            ax.plot(colats, data_dawn, color='grey', ls='--', label='dawn')
            ax.plot(colats, data_dusk, color='grey', ls=':', label='dusk')
            ax.legend()
            ax.set_xlim([5,25])
            ax.set_ylim([0, ax.get_ylim()[1]])
            ax.set_ylabel('Median brightness (kR)', rotation=270, labelpad=15)
            ax.set_xlabel('Colatitude (deg)')
            ax.yaxis.set_label_position('right')
            ax.tick_params(axis='both', which='both', direction='in',
                           left=True, labelleft=False,
                           right=True, labelright=True,
                           top=True, labeltop=False,
                           bottom=True, labelbottom=True)
            txt = ax.text(0, 1.15, '({})'.format(next(paniter)),
                      transform=ax.transAxes, ha='center', va='center',
                      color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
    for dpi in dpi_val:
        plt.savefig('{}/outer_emission_comp_2017_023_{}.png'.format(savepath, dpi), bbox_inches='tight', dpi=dpi)
    plt.close()
    
    
    
    
sys.exit()
#%%
# =============================================================================
# Dawn-dusk full images N only
# =============================================================================
if False:
    files = ['2017_030T08_15_12.fits',
             '2017_080T17_03_11.fits',
             '2017_102T08_21_12.fits',
             '2017_167T19_04_16.fits',
             '2017_213T00_39_52.fits',
             '2017_232T08_28_24.fits']
        
    fig = plt.figure()
    mult = 5
    fig.set_size_inches(mult*3.2, mult*2.2)
    gs = gridspec.GridSpec(2,4, width_ratios=(1,)*3+(0.08,), wspace=0.1)
    
    for panel in range(0,len(files)):
        row = panel // 3
        col = panel % 3
        ax = plt.subplot(gs[row,col], projection='polar')
        
        samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(files[panel]) 
        
        if not panel:
            cax = plt.subplot(gs[:,-1])
        else:
            cax = None
        makeplot(ax, samplefile, cax=cax,
                 ytitle=1,
                 KR_MIN=0.5, KR_MAX=30, tickright=False)
                
        txt = ax.text(0, 1.05, '({})'.format('abcdefghijklmnop'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
    for dpi in dpi_val:
        plt.savefig('{}/dawn_dusk_comp_N_{}.png'.format(savepath, dpi), bbox_inches='tight', dpi=dpi)
    plt.close()
            
    
#%%
# =============================================================================
# Dawn reconnection
# =============================================================================
if False:
    files = ['2017_219T19_56_42.fits',
             '2017_219T21_43_16.fits',
             '2017_219T23_57_52.fits',
             '2017_220T01_29_13.fits',
             '2017_220T03_12_49.fits',
             
             '2017_226T08_04_51.fits',
             '2017_226T09_01_18.fits',
             '2017_226T10_57_20.fits',
             '2017_226T12_57_21.fits',
             '2017_226T13_50_34.fits']
    
    fig = plt.figure()
    mult = 5
    fig.set_size_inches(mult*3.2, mult*1.45)
    gs = gridspec.GridSpec(2,6, width_ratios=(1,)*5+(0.08,), wspace=0.)
    
    for panel in range(0,len(files)):
        row = panel // 5
        col = panel % 5
        ax = plt.subplot(gs[row,col], projection='polar')
        
        samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(files[panel]) 
        
        if not panel:
            cax = plt.subplot(gs[:,-1])
        else:
            cax = None
        makeplot(ax, samplefile, cax=cax, rmin=5, rmax=28,
                 tmin=0,
                 tmax=120,
                 ytitle=1,
                 KR_MIN=0.1, KR_MAX=30, tickright=True)
        
        txt = ax.text(0, 1.13, '({})'.format('abcdefghijklmnop'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
    plt.savefig('{}/reconnection_s.png'.format(savepath), bbox_inches='tight', dpi=300)
    plt.close()

#%%
# =============================================================================
# 2017-023 outer emission N
# =============================================================================
if False:
    files1 = ['2017_023T05_00_22.fits',
              '2017_023T07_27_04.fits',
              '2017_023T08_53_01.fits',
              ]
    
    files2 = ['2017_232T05_14_53.fits',
              '2017_232T06_50_53.fits',
              '2017_232T08_28_24.fits',]
    
    fig = plt.figure()
    mult = 5
    fig.set_size_inches(mult*3.2, mult*0.7)
    gs = gridspec.GridSpec(1,6, width_ratios=(1,)*3+(0.08,0.2,1), wspace=0.1)
    
    for fff in range(1):
        files = files1 if not fff else files2
        images = []
        for panel in range(0,len(files)):
            ax = plt.subplot(gs[fff,panel], projection='polar')
            
            samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(files[panel]) 
            rsr.calculate(samplefile)
            image = np.copy(rsr.redImage_aur)
            
            if not panel:
                images = np.full((3,)+image.shape, np.nan)
            images[panel] = image
                
            if not panel and not fff:
                cax = plt.subplot(gs[:,3])
            else:
                cax = None
            makeplot(ax, samplefile, imagedata=image, 
                     cax=cax,
                     ytitle=1.05,
                     KR_MIN=0.1, KR_MAX=30)
            
            txt = ax.text(0, 1.15, '({})'.format('abcdefghijklmnop'[panel]),
                      transform=ax.transAxes, ha='center', va='center',
                      color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
            
        lonbins = np.linspace(0, 360, num=np.shape(image)[0]+1)
        colatbins = np.linspace(0, 30, num=np.shape(image)[1]+1)
        data = np.nanmedian(images, axis=(0,1))
        data_dawn = np.nanmedian(images[:,:int(images.shape[1]/2),:], axis=(0,1))
        data_dusk = np.nanmedian(images[:,int(images.shape[1]/2):,:], axis=(0,1))
    #    data[data<0] = 0
        ax = plt.subplot(gs[fff,-1])
        colats = colatbins[:-1] + np.diff(colatbins)/2
        ax.plot(colats, data, color='k', label='total')
        ax.plot(colats, data_dawn, color='royalblue', label='dawn')
        ax.plot(colats, data_dusk, color='crimson', label='dusk')
        ax.legend()
        ax.set_xlim([5,25])
        ax.set_ylim([0, ax.get_ylim()[1]])
        ax.set_ylabel('Median brightness (kR)', rotation=270, labelpad=15)
        ax.set_xlabel('Colatitude (deg)')
        ax.yaxis.set_label_position('right')
        ax.tick_params(axis='both', which='both', direction='in',
                       left=True, labelleft=False,
                       right=True, labelright=True,
                       top=True, labeltop=False,
                       bottom=True, labelbottom=True)
        
    plt.savefig('{}/outer_emission_n_2017_023.png'.format(savepath), bbox_inches='tight', dpi=300)
    plt.close()

#%%
# =============================================================================
# Disappearing arc
# =============================================================================
if False:
    files = ['2017_080T21_51_04.fits',
             '2017_080T22_13_28.fits',
             '2017_080T22_36_00.fits',
             '2017_080T22_58_32.fits',
             '2017_080T23_21_04.fits']
    
    fig = plt.figure()
    mult = 5
    fig.set_size_inches(mult*3.2, mult*0.65)
    gs = gridspec.GridSpec(1,7, width_ratios=(1,)*5+(0.08,0.08,), wspace=0.1)
    
    for panel in range(0,len(files)):
        ax = plt.subplot(gs[0,panel], projection='polar')
        
        samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(files[panel]) 
        
        if not panel:
            cax = plt.subplot(gs[:,-1])
        else:
            cax = None
        makeplot(ax, samplefile, cax=cax, rmin=5, rmax=25, tmin=105, tmax=195, ytitle=0.95,
                 KR_MIN=1, KR_MAX=12, tickright=True)
        
        txt = ax.text(0, 1.08, '({})'.format('abcdefgh'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
    plt.savefig('{}/disapp_lowres.png'.format(savepath), bbox_inches='tight', dpi=300)
    plt.close()

#%%
# =============================================================================
# Secondary arc latitude sums - fancy plot
# =============================================================================
if False:
    files = np.array(['2017_023T05_00_22.fits',
                      '2017_023T07_27_04.fits',
                      '2017_023T08_53_01.fits',
                                        
                      '2017_167T14_36_25.fits',
                      '2017_167T16_25_36.fits',
                      '2017_167T19_04_16.fits',
                                        
                      '2017_232T05_14_53.fits',
                      '2017_232T06_50_53.fits',
                      '2017_232T08_28_24.fits',
                     ])
    
    days = np.array([iii[:8] for iii in files])
    udays = np.unique(days)
    
    fig = plt.figure()
    fig.set_size_inches(15,12)
    gs = gridspec.GridSpec(4, 6,
                           width_ratios=(1,0.1,1,1,1,0.5),
                           height_ratios=(1,1,1,0.1),
                           hspace=0.5,
                           )
    
    lonmin = -45
    lonmax = 90
    colatmin = 5
    colatmax = 25
    
    KR_MIN = 0.5
    KR_MAX = 30
    
    for ddd in range(len(udays)):
        selec = files[np.where(days==udays[ddd])[0]]
        for fff in range(len(selec)):
            samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(selec[fff])
            rsr.calculate(samplefile)
            image = np.copy(rsr.redImage_aur)
            
            if not fff:
                makeplot(plt.subplot(gs[ddd,fff], projection='polar'), samplefile, imagedata=image, ytitle=1,
                         KR_MIN=KR_MIN, KR_MAX=KR_MAX)
            
            if lonmin < 0:
                image[int(image.shape[0]*lonmax/360):int(image.shape[0]*(lonmin+360)/360),:] = np.nan
            else:
                image[:int(image.shape[0]*lonmin/360),:] = np.nan
                image[int(image.shape[0]*lonmax/360):,:] = np.nan
                
            if not fff:
                images = np.copy(image)
            else:
                images = np.append(images, image, axis=0)
               
            # plot
            ax = plt.subplot(gs[ddd,fff+2])
            lonbins = np.linspace(0, 360, num=np.shape(image)[0]+1)
            colatbins = np.linspace(0, 30, num=np.shape(image)[1]+1)
            if lonmin < 0:
                data = np.roll(image, int(-lonmin/360*image.shape[0]), axis=0)
                lons = (lonbins+lonmin)/180*12
            data[data<=0] = 0.01
            quad = ax.pcolormesh(lons,
                                 colatbins,
                                 data.T,
                                 cmap=cmap_UV)
            quad.set_norm(mcolors.LogNorm(KR_MIN, KR_MAX))
            
            if not ddd and not fff:
                cbar = plt.colorbar(quad, cax=plt.subplot(gs[-1,2:5]), extend='both',
                                    orientation='horizontal')
                cbar.set_label('Intensity (kR)', fontsize=11)
                cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                                   np.arange(1,10,1)),
                                         np.arange(10,KR_MAX+1,10)))
                cbar.ax.tick_params(labelsize=10)
                
            
            ax.set_facecolor('gray')
            ax.set_xlim([lonmin/180*12, lonmax/180*12])
            ax.set_ylim([colatmin,colatmax])
            xticks = np.array([-6, -3, 0, 3, 6, 9, 12])
            xticks = xticks[(xticks>= lonmin/180*12) & (xticks<= lonmax/180*12)]
            ax.set_xticks(xticks)
            ax.set_xticklabels(xticks % 24)
            
            if not fff:
                ax.set_ylabel('Colatitude (deg)')
            if ddd == 2:
                ax.set_xlabel('Local time (hr)')
            
            title = (selec[fff].split('T')[0].replace('_','-') + ' ' +
                     selec[fff].split('T')[1].replace('_',':').split('.')[0])
            ax.set_title(title, fontsize=12)
            
            ax.tick_params(axis='both', which='both', direction='out',
                           left=True, labelleft=True if not fff else False,
                           right=True, labelright=False,
                           top=False, labeltop=False,
                           bottom=True, labelbottom=True)
        
        data = np.nanmedian(images, axis=0)
    #    data[data<0] = 0
        ax = plt.subplot(gs[ddd,-1])
        colats = colatbins[:-1] + np.diff(colatbins)/2
        ax.plot(data, colats, color='crimson')
        ax.set_ylim([colatmin,colatmax])
        ax.set_xlim([0, ax.get_xlim()[1]])
        if ddd == 2:
            ax.set_xlabel('Median brightness (kR)')
        ax.set_ylabel('Colatitude (deg)', rotation=270, labelpad=15)
        ax.yaxis.set_label_position('right')
        ax.tick_params(axis='both', which='both', direction='out',
                       left=True, labelleft=False,
                       right=True, labelright=True,
                       top=False, labeltop=False,
                       bottom=True, labelbottom=True)
    plt.savefig('{}/secondary_emission_north.png'.format(savepath), bbox_inches='tight', dpi=300)
    plt.show()
    plt.close()


#%%
if False:
    filesN = ['2017_030T08_15_12.fits',
             '2017_080T17_03_11.fits',
             '2017_102T08_21_12.fits',
             '2017_167T19_04_16.fits',
             '2017_213T00_39_52.fits',
             '2017_232T08_28_24.fits']
    
    filesS = ['2017_168T04_03_21.fits',
             '2017_219T21_43_16.fits',
             '2017_226T08_04_51.fits',
             '2017_239T06_19_54.fits',
             '2017_245T16_53_09.fits',
             '2017_252T04_00_35.fits']
    
    filesSDusk = ['2017_009T12_49_40.fits',
             '2017_168T03_04_14.fits',
             '2017_219T20_34_12.fits',
             '2017_226T06_49_52.fits',
             '2017_239T04_52_23.fits',
             '2017_245T15_45_22.fits']
    
    for files in [filesN,filesS,filesSDusk]:
        fig = plt.figure()
        mult = 5
        fig.set_size_inches(mult*3.2, mult*2.2)
        gs = gridspec.GridSpec(2,4, width_ratios=(1,)*3+(0.08,), wspace=0.1)
        
        for panel in range(0,len(files)):
            row = panel // 3
            col = panel % 3
            ax = plt.subplot(gs[row,col], projection='polar')
            
            samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10\{}'.format(files[panel]) 
            
            if not panel:
                cax = plt.subplot(gs[:,-1])
                makeplot(ax, samplefile, cax)
            else:
                makeplot(ax, samplefile)
            
            txt = ax.text(0, 1, '({})'.format('abcdefgh'[panel]),
                      transform=ax.transAxes, ha='center', va='center',
                      color='k', fontweight='bold', fontsize=20)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
            
        tmp = 'N' if files==filesN else ('S' if files==filesS else 'SDusk')
    #    plt.savefig('{}/dawn_dusk_struct_{}.png'.format(savepath, tmp), bbox_inches='tight', dpi=400)
        plt.savefig('{}/dawn_dusk_struct_{}_lowres.png'.format(savepath, tmp), bbox_inches='tight', dpi=200)
        plt.close()