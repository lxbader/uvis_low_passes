import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime, timedelta
import get_image
import ion_footp
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import matplotlib.patheffects as patheffects
import matplotlib.pyplot as plt
import numpy as np
import os
import time_conversions as tc
import uvisdb

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_short = truncate_colormap(cmap_SPEC, 0.25, 0.75, 100)
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

RS = 60268e3 #m
MU0 = 4*np.pi*1e-7 #Vs/Am

cd = cassinipy.CassiniData(datapath)
udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

cionfp = ion_footp.CassiniIonMap()
L = np.linspace(2, 70, num=2000)
colat_n_shell, colat_s_shell, loct = cionfp.map_ion(np.array([L, np.zeros_like(L), np.zeros_like(L)]))

mainsavepath = '{}/UVIS/PDS_UVIS/PROJECTIONS_HI_RES_RES10'.format(datapath)
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)
    
plotsavepath = '{}/UVIS_low_passes'.format(plotpath)
if not os.path.exists(plotsavepath):
    os.makedirs(plotsavepath)
    
files = np.genfromtxt('{}/uvis_low_passes/dusk_hi_res_files.txt'.format(gitpath), delimiter=',', dtype='U')

panels = 'abcdefghijklmnopqrstuvwxyz'

#%%
# =============================================================================
# Comparison between all low passes
# =============================================================================
fig = plt.figure()
fig.set_size_inches(10,15)
gs = gridspec.GridSpec(len(files)+2, 1, hspace=0.2,
                       height_ratios=(1,)*len(files)+(0.2,0.2))
for fctr in range(len(files)):
    print(fctr)
    # get image data
    thisfile = '{}/{}.fits'.format(mainsavepath, files[fctr])
    image, angles, hem = get_image.getRedUVIS(thisfile)
    latbins = np.linspace(0, 30, num=image.shape[1]+1)
    lonbins = np.linspace(0, 360, num=image.shape[0]+1)
    
    # find right dataframe entry for the image
    fmt = '%Y_%jT%H_%M_%S'
    thiset = tc.datetime2et(datetime.strptime(files[fctr], fmt))
    selec = imglist.loc[(imglist['ET_START']-thiset).abs().argmin()]
    
    # select latitude    
    LATMIN = 10
    LATMAX = 25
    tmp = np.where((latbins>=LATMIN) & (latbins<=LATMAX))[0]
    latbins = latbins[tmp]
    image = image[:,tmp[:-1]]
    
    # select longitude    
    LONMIN = 185
    LONMAX = 55
    if LONMIN>LONMAX:
        lonbins = np.linspace(0, 720, num=image.shape[0]*2+1)
        image = np.tile(image, (2,1))
        LONMAX += 360
    tmp = np.where((lonbins>=LONMIN) & (lonbins<=LONMAX))[0]
    lonbins = lonbins[tmp]
    image = image[tmp[:-1],:]
    ltbins = lonbins/180*12
    
    # plot
    image[image<0.1] = 0.1
    ax = plt.subplot(gs[fctr,0])
    ax.set_facecolor('gray')
    quad = ax.pcolormesh(ltbins, latbins, image.T, cmap=cmap_UV)
    KR_MIN=0.5
    KR_MAX=30
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    
    tmp = np.arange(0, 74, 2)
    tmp = np.array([iii for iii in tmp if ((iii>=ltbins[0]) & (iii <=ltbins[-1]))])
    ax.set_xlim([ltbins[0], ltbins[-1]])
    ax.set_ylim([LATMAX, LATMIN])
    ax.set_xticks(tmp)
    ax.set_xticklabels(tmp % 24)
    if fctr == len(files)-1:
        ax.set_xlabel('LT (hr)')
        cbar = plt.colorbar(quad, cax=plt.subplot(gs[-1,0]), extend='both',
                            orientation='horizontal')
        cbar.set_label('Intensity (kR)', labelpad=10, fontsize=12)
        cbar.set_ticks(np.append(np.linspace(1,9,num=9),
                                 np.linspace(10,90,num=9)))
        cbar.ax.tick_params(labelsize=11)
    ax.set_ylabel('Colat (deg)')
    
    ax.tick_params(axis='both', which='both', direction='out',
                   left=True, labelleft=True,
                   right=True, labelright=False,
                   top=True, labeltop=False,
                   bottom=True, labelbottom=True if fctr == len(files)-1 else False)
    
    # label timestamp
    dtstart = tc.et2datetime(selec['ET_START'])
    dtstop = tc.et2datetime(selec['ET_STOP'])
    fmt = '%H:%M'
    txt = ax.text(0.01, 0.03, '{} {}-{}'.format(datetime.strftime(dtstart, '%Y-%j'),
                                    datetime.strftime(dtstart, fmt),
                                    datetime.strftime(dtstop, fmt)),
            transform=ax.transAxes,
            va='bottom', ha='left',
            color='w')
    txt.set_path_effects([patheffects.withStroke(linewidth=3, foreground='k')])
    
    # extended orbit
    tmp = np.linspace(selec['ET_START']-3600*3, selec['ET_STOP']+3600*6, num=600)
    _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
    colat = colat_n if hem=='North' else colat_s
    loct[loct<ltbins[0]] += 24
    ax.plot(loct[:-10], colat[:-10], c='w', lw=1)
    
    # arrow
    tmp = (selec['ET_START']+selec['ET_STOP'])/2
    tmp = [tmp-120, tmp+120]
    _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
    colat = colat_n if hem=='North' else colat_s
    loct[loct<ltbins[0]] += 24
    arrowstyle = '->,head_length=9, head_width=3.75'
    ax.add_patch(patches.FancyArrowPatch((loct[0], colat[0]),
                                       (loct[1], colat[1]),
                                       arrowstyle=arrowstyle,
                                       color='crimson',
                                       linewidth=2))
    
    # direct footprint
    tmp = np.linspace(selec['ET_START'], selec['ET_STOP'], num=100)
    _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
    colat = colat_n if hem=='North' else colat_s
    loct[loct<ltbins[0]] += 24
    ax.plot(loct, colat, c='crimson', lw=2)
    
    # hour markers
    st = dtstart-timedelta(hours=3)
    start = datetime(st.year, st.month, st.day, st.hour)+timedelta(hours=1)
    tmp = np.array([start])
    while tmp[-1]+timedelta(hours=1) < dtstop+timedelta(hours=6):
        tmp = np.append(tmp, [tmp[-1]+timedelta(hours=1)])
    _, colat_n, colat_s, loct = cd.get_ionfootp(tc.datetime2et(tmp))
    colat = colat_n if hem=='North' else colat_s
    loct[loct<ltbins[0]] += 24
    ax.scatter(loct, colat, color='w', s=3, marker='o', zorder=10)
        
plt.savefig('{}/comp_dusk.png'.format(plotsavepath), bbox_inches='tight', dpi=1000)