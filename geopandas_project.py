import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.convolution import Gaussian2DKernel, convolve_fft, convolve
import cubehelix
import geopandas
import get_image
from matplotlib.collections import PatchCollection
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
from matplotlib.patches import Polygon as mPolygon
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import os
import remove_solar_reflection
from scipy.ndimage import gaussian_filter
from shapely.geometry import Polygon, MultiPolygon

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
rsr = remove_solar_reflection.RemoveSolarReflection()

savepath = '{}/UVIS_low_passes'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

shapefilepath = r'{}\Projects\2019 AB grand finale\country projection\ne_50m_admin_0_countries'.format(boxpath)
shpfile = '{}/ne_50m_admin_0_countries.shp'.format(shapefilepath)

#shpfile = r'{}\Projects\2019 AB grand finale\country projection\ref-countries-2016-03m.shp\CNTR_RG_03M_2016_4326.shp\CNTR_RG_03M_2016_4326.shp'.format(boxpath)
#shpfile = r'{}\Projects\2019 AB grand finale\country projection\ref-nuts-2016-03m.shp\NUTS_RG_03M_2016_4326_LEVL_0.shp\NUTS_RG_03M_2016_4326_LEVL_0.shp'.format(boxpath)


data = geopandas.read_file(shpfile)
data = data[data['CONTINENT']=='Europe']
data = data[data['NAME_EN']!='Russia']
#data.plot()

#%%
# =============================================================================
# Do stuff with Europe map
# =============================================================================
mod = data['geometry'].copy()

# get center of mass of all points
allcoords = np.array([])
for iii in range(len(mod)):
    if type(mod.iloc[iii]) == Polygon:
        if not len(allcoords):
            allcoords = np.array(mod.iloc[iii].exterior.coords)
        else:
            allcoords = np.append(allcoords, np.array(mod.iloc[iii].exterior.coords), axis=0)
        
    else:
        for jjj in range(len(mod.iloc[iii].geoms)):
            if not len(allcoords):
                allcoords = np.array(mod.iloc[iii][jjj].exterior.coords)
            else:
                allcoords = np.append(allcoords, np.array(mod.iloc[iii][jjj].exterior.coords), axis=0)

# center
origcent = np.mean(allcoords, axis=0)

# scale down with respect to center
RS = 60268e3 #m
RE = 6371e3 #m
fact = RE/RS
    
def lonlat2cart(coords):
    coords_rad = np.radians(coords)
    coords_cart = np.array([
        RS*np.cos(coords_rad[:,1])*np.cos(coords_rad[:,0]),
        RS*np.cos(coords_rad[:,1])*np.sin(coords_rad[:,0]),
        RS*np.sin(coords_rad[:,1])]).T
    return coords_cart

def cart2lonlat(new_coords_cart):
    r_temp = np.sqrt(np.sum(new_coords_cart**2, axis=1))
    new_lat = np.degrees(np.arcsin(new_coords_cart[:,2]/r_temp))
    new_lon = np.degrees(np.arctan2(new_coords_cart[:,1], new_coords_cart[:,0]))
    return np.array([new_lon, new_lat]).T

origcent_cart = np.squeeze(lonlat2cart(np.array([origcent])))
    
def scale_sph(coords):
    coords_cart = lonlat2cart(coords)
    vec_dist = coords_cart - origcent_cart
    new_coords_cart = origcent_cart + vec_dist*fact
    return cart2lonlat(new_coords_cart)

# rotate to desired latitude
deslat = 73
newcent = np.array([origcent[0], deslat])
init_vec = lonlat2cart(np.array([origcent]))
fin_vec = lonlat2cart(np.array([newcent]))

u = np.cross(fin_vec, init_vec)
u = np.squeeze(u/np.sqrt(np.sum(u**2)))
theta = -np.arccos(np.sum(init_vec/np.linalg.norm(init_vec)*
                          fin_vec/np.linalg.norm(fin_vec)))

rotmat = np.full((3,3), np.nan)
rotmat[0,0] = np.cos(theta) + u[0]**2*(1-np.cos(theta))
rotmat[0,1] = u[0]*u[1]*(1-np.cos(theta)) - u[2]*np.sin(theta)
rotmat[0,2] = u[0]*u[2]*(1-np.cos(theta)) + u[1]*np.sin(theta)

rotmat[1,0] = u[1]*u[0]*(1-np.cos(theta)) + u[2]*np.sin(theta)
rotmat[1,1] = np.cos(theta) + u[1]**2*(1-np.cos(theta))
rotmat[1,2] = u[1]*u[2]*(1-np.cos(theta)) - u[0]*np.sin(theta)

rotmat[2,0] = u[2]*u[0]*(1-np.cos(theta)) - u[1]*np.sin(theta)
rotmat[2,1] = u[2]*u[1]*(1-np.cos(theta)) + u[0]*np.sin(theta)
rotmat[2,2] = np.cos(theta) + u[2]**2*(1-np.cos(theta))

def rotate_lat(coords):
    coords_cart = lonlat2cart(coords)
    
    # expand vector to padded/repeated array (matmul doesn't broadcast matrix*vector)
    coords_pad = np.expand_dims(coords_cart, axis=-1)
    coords_pad = np.repeat(coords_pad, 3, axis=-1)
    # use stack matrix multiplication
    coords_rot_pad = np.matmul(rotmat, coords_pad)
    # get valid part
    coords_rot = coords_rot_pad[:,:,0]
    
    new_coords = cart2lonlat(coords_rot)
    return new_coords

def move_coords(coords):
    scaled_coords = scale_sph(coords)
    shifted_coords = rotate_lat(scaled_coords)
    shifted_coords[:,0] *= np.pi/180
    shifted_coords[:,0] += 17*np.pi/180
    shifted_coords[:,1] = 90 - shifted_coords[:,1]
    return shifted_coords
    
mod_new = mod.copy()
for iii in range(len(mod)):
    if type(mod.iloc[iii]) == Polygon:
        ext = np.array(mod.iloc[iii].exterior.coords)
        if np.any(ext[:,0]<-40) or np.any(ext[:,1]>73):
            continue
        new_ext = move_coords(ext)
        mod_new.iloc[iii] = Polygon(new_ext)
    else:
        polygonlist = []
        for jjj in range(len(mod.iloc[iii].geoms)):
            ext = np.array(mod.iloc[iii][jjj].exterior.coords)
            if np.any(ext[:,0]<-40) or np.any(ext[:,1]>73):
                continue
            new_ext = move_coords(ext)
            polygonlist.append(Polygon(new_ext))
        mod_new.iloc[iii] = MultiPolygon(polygonlist)


#%%
def makeplot(ax, image, tmin=0, tmax=360, rmin=0, rmax=30, cax=None, sty='color', minorticks=True, tstart='S',
             plot_europe=False):
    if plot_europe:
        mod_new.plot(ax=ax, color='crimson', alpha=0.7, zorder=10)
    
    if sty=='color':
        # plot hexagon background (color)
        hex_proj = r'{}\Projects\2019 AB grand finale\country projection\rgb_mosaic_ns_2016331.png'.format(boxpath)
        data = plt.imread(hex_proj)
        phi = np.linspace(0, 2*np.pi, num=data.shape[1]+1)
        r = np.linspace(0, 180, num=data.shape[0]+1)
        Phi,R = np.meshgrid(phi, r)
        data = np.roll(data, int(data.shape[1]*35/360), axis=1)
        color = data.reshape((data.shape[0]*data.shape[1],data.shape[2]))
        m = ax.pcolormesh(Phi, R, data[:,:,0], color=color, zorder=1)
        m.set_array(None)
    else:
        # plot hexagon background (BW)
        hex_proj = r'{}\Projects\2019 AB grand finale\country projection\cb3_ns_2016331.png'.format(boxpath)
        data = plt.imread(hex_proj)
        phi = np.linspace(0, 2*np.pi, num=data.shape[1]+1)
        r = np.linspace(0, 180, num=data.shape[0]+1)
        Phi,R = np.meshgrid(phi, r)
        data = np.roll(data, int(data.shape[1]*35/360), axis=1)
        ax.pcolormesh(Phi, R, data, cmap='gist_gray', zorder=1)
        
    KR_MIN=0.5
    KR_MAX=30
#    img = image[:int(image.shape[0]/4),:]
#    lonbins = np.linspace(0, 2*np.pi/4, num=np.shape(img)[0]+1)
#    colatbins = np.linspace(0, 30, num=np.shape(img)[1]+1)
    
    img=image
    lonbins = np.linspace(0, 2*np.pi, num=np.shape(img)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(img)[1]+1)
    
    norm = mcolors.LogNorm(KR_MIN,KR_MAX)
    image_norm = norm(img)
    cmap = plt.get_cmap('pink')
    cmap = cmap_UV
    data = cmap(image_norm)
    image_norm[image_norm>1] = 1
    image_norm[image_norm<0] = 0
    if tmin==0 and tmax==360 and tstart=='S':
        data[:,:,-1] = img/np.nanmax(img)
    else:
        data[:,:,-1] = img/np.nanmax(img)
    data[:,:,-1] = data[:,:,-1]
    tmp = np.where(data[:,:,-1]<0)
    data[:,:,-1][tmp] = 0
    tmp = np.where(data[:,:,-1]>1)
    data[:,:,-1][tmp] = 1
    if tmin==0 and tmax==360:
        data[:,:,-1] /= 3    
    data = np.swapaxes(data, 0, 1)
    color = data.reshape((data.shape[0]*data.shape[1],data.shape[2]))
    Phi,R = np.meshgrid(lonbins, colatbins)
    quad = ax.pcolormesh(Phi, R, data[:,:,-1], color=color, cmap=cmap, zorder=1.1)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    quad.set_array(None)
    
    
#    quad = ax.pcolormesh(lonbins, colatbins, img_short.T, cmap=cmap_UV, zorder=1.1)
#    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
#    ax.set_facecolor('gray')
    
    # plot colorbar
    if not (cax is None):
        cbar = plt.colorbar(quad, cax=cax, extend='both')
        cbar.set_label('Intensity (kR)', labelpad=10, fontsize=11, rotation=270)
        cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,KR_MAX+1,10)))
        cbar.ax.tick_params(labelsize=10)
    
    ticks_maj = np.linspace(0, 2*np.pi, 13)
    ax.set_xticks(ticks_maj)        
    
    if tmin==0 and tmax==360:
        ax.set_xticklabels([])
    else:
        ax.set_xticklabels(np.round(np.degrees(ticks_maj)).astype(int))
        ticks_min = np.linspace(0, 2*np.pi, 73)
        ax.set_xticks(ticks_min, minor=True)
        ax.set_xticklabels(np.round(np.degrees(np.setdiff1d(ticks_min, ticks_maj))).astype(int), minor=True)
        ax.text(np.radians(30), 22.3, 'Longitude ($^\circ$)',
#                transform=ax.transAxes,
                ha='center', va='center',
                rotation=30)
    
    ticks_maj = np.arange(10,31,10)
    ax.set_yticks(ticks_maj)
    
    if tmin==0 and tmax==360:
        ax.set_yticklabels([])
        ax.set_rlabel_position(305)
    else:
        ax.set_yticklabels(np.round(ticks_maj).astype(int))
        ticks_min = np.arange(10, 31, 2)
        ax.set_yticks(ticks_min, minor=True)
        ax.set_yticklabels(np.round(np.setdiff1d(ticks_min, ticks_maj)).astype(int), minor=True)
        ax.text(np.radians(14), 17.5, 'Colatitude ($^\circ$)',
#                transform=ax.transAxes,
                ha='center', va='center',
                rotation=284)
        
    ax.grid(which='major', color='0.8', linewidth=0.5 if tmin==0 and tmax==360 else 1, zorder=5)
    if minorticks:
        ax.grid(which='minor', color='0.8', linewidth=0.5, linestyle=':', zorder=5)
    
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        tmp = np.degrees(ticks[iii])
        if tmp > tmax:
            continue
        else:
            if tmp < tmin%360:
                continue
        txt = ax.text(ticks[iii], rmax-3, ticklabels[iii], color='w', fontsize=12, fontweight='bold',
                      ha='center',va='center', zorder=5)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    
    ax.set_rorigin(0)
    ax.set_rmax(rmax)
    ax.set_rmin(rmin)
    ax.set_theta_zero_location(tstart)
    ax.set_thetamin(tmin)
    ax.set_thetamax(tmax)
    
#%%
# plot

samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
#samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES50'
samplefile = '{}/2017_167T21_17_11.fits'.format(samplepath)

image, _, _ = get_image.getRedUVIS(samplefile)

if False:
    gaussian_2D_kernel = Gaussian2DKernel(x_stddev=0.5, y_stddev=1,
                                          x_size=image.shape[0]/180+1,
                                          y_size=image.shape[1]/30+1)
    image_smooth = convolve(image, gaussian_2D_kernel, nan_treatment='interpolate')
    
    #number of pixels used for filtering
    num = convolve(np.isfinite(image), np.ones_like(gaussian_2D_kernel),
                   nan_treatment='fill', normalize_kernel=False)
    image_smooth[num<200] = np.nan
    
#for sty in ['color', 'bw']:
for sty in ['bw']:  
    fig = plt.figure()
    fig.set_size_inches(10,5)
    
    gs = gridspec.GridSpec(1, 3, width_ratios = (1,1,0.05))
    
    ax1 = plt.subplot(gs[0,0], projection='polar')
    makeplot(ax1, image-2, cax=plt.subplot(gs[0,-1]), sty=sty,
             minorticks=False, rmax=25, plot_europe=True)
    
    ax2 = plt.subplot(gs[0,1], projection='polar')
    makeplot(ax2, image, tmin=18, tmax=42, rmin=14, rmax=21, sty=sty, plot_europe=True)
    
    plt.savefig('{}/europe_overlay_{}.png'.format(savepath, sty), bbox_inches='tight', dpi=1000)
    plt.close()
    
    
#%%
# =============================================================================
# plot single full images on top of hexagon
# =============================================================================
    
names = [
         '2016_316T11_20_16',
         '2016_339T01_54_31',
         '2017_023T08_53_01',
         '2017_030T08_15_12',
         '2017_167T19_04_16',
         '2017_219T11_37_22',
         '2017_232T08_28_24',
         ]

for name in names:

    samplepath_2 = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
    samplefile_2 = '{}/{}.fits'.format(samplepath, name)
    
    image_2, _, _ = get_image.getRedUVIS(samplefile_2)
    rsr.calculate(samplefile_2)
    image_2 = np.copy(rsr.redImage_aur)-2
        
    fig = plt.figure()
    fig.set_size_inches(6,6)
        
    ax1 = plt.subplot(projection='polar')
    makeplot(ax1, image_2, sty='bw',
             minorticks=False, rmax=25, tstart='N')
    
    
    plt.savefig('{}/overlay_{}_bw.png'.format(savepath, name), bbox_inches='tight', dpi=1000)
    plt.close()
    
#%%
# =============================================================================
# plot several images together on hexagon
# =============================================================================
names = [
         '2016_316T11_20_16',
         '2016_339T01_54_31',
         '2017_023T08_53_01',
         '2017_030T08_15_12',
         '2017_219T11_37_22',
         '2017_232T08_28_24',
         ]

fig = plt.figure()
fig.set_size_inches(12,8)

gs = gridspec.GridSpec(2, 3, wspace=0.1, hspace=0.1)

for nnn in range(len(names)):
    row = nnn // 3
    col = nnn % 3

    samplepath_2 = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
    samplefile_2 = '{}/{}.fits'.format(samplepath, names[nnn])
    
    image_2, _, _ = get_image.getRedUVIS(samplefile_2)
    rsr.calculate(samplefile_2)
    image_2 = np.copy(rsr.redImage_aur)-2
        
    ax1 = plt.subplot(gs[row, col], projection='polar')
    makeplot(ax1, image_2, sty='bw',
             minorticks=False, rmax=25, tstart='N')
    
    
plt.savefig('{}/overlay_tile_bw.png'.format(savepath), bbox_inches='tight', dpi=300)
plt.close()


#%%
names = [
         '2016_316T11_20_16',
#         '2016_339T01_54_31',
         '2017_023T08_53_01',
#         '2017_030T08_15_12',
#         '2017_219T11_37_22',
         '2017_232T08_28_24',
         ]

fig = plt.figure()
fig.set_size_inches(12,4)

gs = gridspec.GridSpec(1, 3, wspace=0.1, hspace=0.1)

for nnn in range(len(names)):
    row = nnn // 3
    col = nnn % 3

    samplepath_2 = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
    samplefile_2 = '{}/{}.fits'.format(samplepath_2, names[nnn])
    
    image_2, _, _ = get_image.getRedUVIS(samplefile_2)
    rsr.calculate(samplefile_2)
    image_2 = np.copy(rsr.redImage_aur)-2
        
    ax1 = plt.subplot(gs[row, col], projection='polar')
    makeplot(ax1, image_2, sty='bw',
             minorticks=False, rmax=25, tstart='N')
    
    
plt.savefig('{}/overlay_tile_3_bw.png'.format(savepath), bbox_inches='tight', dpi=300)
plt.close()
    

#%%

samplepath_2 = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
samplefile_2 = '{}/{}.fits'.format(samplepath_2, '2017_023T08_53_01')

image_2, _, _ = get_image.getRedUVIS(samplefile_2)
rsr.calculate(samplefile_2)
image_2 = np.copy(rsr.redImage_aur)-2

#%%
    
fig = plt.figure()
fig.set_size_inches(10,10)

BGROT=145

lon_0=(90+BGROT) % 360
lat_0=60

# altitude of camera (in km).
h = 2000.
# resolution = None means don't process the boundary datasets.
m1 = Basemap(projection='nsper',satellite_height=h*1000,
        lon_0=lon_0,lat_0=lat_0,resolution=None)
# add an axes with a black background
ax = fig.add_axes([0.1,0.1,0.8,0.8])
# plot just upper right quadrant (corners determined from global map).
# keywords llcrnrx,llcrnry,urcrnrx,urcrnry used to define the lower
# left and upper right corners in map projection coordinates.
# llcrnrlat,llcrnrlon,urcrnrlon,urcrnrlat could be used to define
# lat/lon values of corners - but this won't work in cases such as this
# where one of the corners does not lie on the earth.
bmap = Basemap(projection='nsper',satellite_height=h*1000,
               lon_0=lon_0,lat_0=lat_0,resolution='l',
               llcrnrx=-m1.urcrnrx/3,llcrnry=m1.urcrnry/5,
               urcrnrx=m1.urcrnrx/3,urcrnry=m1.urcrnry/2)

# draw background
saturn_map = r'{}\Projects\2019 AB grand finale\country projection\cb3_ns_2016331.png'.format(boxpath)
m1.warpimage(saturn_map,
             scale=10
             )

# draw lat/lon grid lines every 30 degrees.
bmap.drawmeridians((np.arange(0,360,30)+BGROT)%360, color='0.4')
bmap.drawparallels(np.arange(-90,90,10), color='0.4')

# load image and define mesh
KR_MIN=0.5
KR_MAX=30
img=np.copy(image_2)
img = np.roll(img, int(BGROT/360*np.shape(img)[0]), axis=0)
lonbins = np.linspace(0, 360, num=np.shape(img)[0]+1)
latbins = np.linspace(90, 60, num=np.shape(img)[1]+1)
LON,LAT = np.meshgrid(lonbins, latbins)

# cut off limb to avoid artifacts
loncent = lonbins[:-1]+np.mean(np.diff(lonbins))/2
latcent = latbins[:-1]+np.mean(np.diff(latbins))/2
aa,bb = np.meshgrid(loncent, latcent)
# project coordinates
X, Y = bmap(aa, bb)
# which points are off the planet
inv = np.where((X>1e20) | (Y>1e20))
X[inv] = np.nan
# expand region a bit by filtering
X_filt = gaussian_filter(X, 11, mode='reflect')
inv = np.where(np.isnan(X_filt))
img[inv[1],inv[0]] = np.nan

# plot
norm = mcolors.LogNorm(KR_MIN,KR_MAX)
image_norm = norm(img)
cmap = plt.get_cmap('pink')
cmap = cmap_UV
data = cmap(image_norm)
image_norm[image_norm>1] = 1
image_norm[image_norm<0] = 0
data[:,:,-1] = img/np.nanmax(img)
data[:,:,-1] = data[:,:,-1]
tmp = np.where(data[:,:,-1]<0)
data[:,:,-1][tmp] = 0
tmp = np.where(data[:,:,-1]>1)
data[:,:,-1][tmp] = 1
#data[:,:,-1] /= 3    
data = np.swapaxes(data, 0, 1)
color = data.reshape((data.shape[0]*data.shape[1],data.shape[2]))
quad = bmap.pcolormesh(LON, LAT, data[:,:,-1],
                       color=color, cmap=cmap, latlon=True)
quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
quad.set_array(None)


# add map of Europe
if False:
    p = []
    offset = BGROT+60 # deg, 0 = 1 LT / 45deg
    for shape in mod_new:
        if type(shape) == Polygon:
            ext = np.array(shape.exterior.coords)
            new_x, new_y = bmap(ext[:,0]*180/np.pi+offset, 90-ext[:,1])
            tmp = mPolygon(np.array([new_x, new_y]).T)
            p.append(tmp)
        else:
            for sss in shape.geoms:
                ext = np.array(sss.exterior.coords)
                new_x, new_y = bmap(ext[:,0]*180/np.pi+offset, 90-ext[:,1])
                tmp = mPolygon(np.array([new_x, new_y]).T)
                p.append(tmp)
            
    plt.gca().add_collection(PatchCollection(p, facecolor='crimson', edgecolor=None, alpha=0.7, zorder=5))

plt.savefig('{}/europe_overlay_globe.png'.format(savepath), bbox_inches='tight', dpi=300)
plt.show()
plt.close()

