import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime, timedelta
import get_image
import get_MAG
import get_RPWS_FULL
import glob
import INCA_ion_mtof
import internal_field_model
import ion_footp
import LEMMS_data
import matplotlib.cm as mcm
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import matplotlib.patheffects as patheffects
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import scipy.interpolate as sint
import scipy.signal as sgn
import time_conversions as tc
import uvisdb

present = True
presentfact = 0.6

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_short = truncate_colormap(cmap_SPEC, 0.25, 0.75, 100)

cmap_data_lines = cmap_SPEC_short
cmap_data = cmap_UV

#cmap_data = mcm.get_cmap('viridis')
#cmap_data_lines = mcm.get_cmap('viridis')

RS = 60268e3 #m
MU0 = 4*np.pi*1e-7 #Vs/Am

cd = cassinipy.CassiniData(datapath)
udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

cionfp = ion_footp.CassiniIonMap()
L = np.linspace(2, 70, num=2000)
colat_n_shell, colat_s_shell, loct = cionfp.map_ion(np.array([L, np.zeros_like(L), np.zeros_like(L)]))

mainsavepath = '{}/UVIS/PDS_UVIS/PROJECTIONS_HI_RES_RES10'.format(datapath)
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)
    
plotsavepath = '{}/UVIS_low_passes/comp'.format(plotpath)
if not os.path.exists(plotsavepath):
    os.makedirs(plotsavepath)
    
files = np.genfromtxt('{}/uvis_low_passes/low_passes_comp_files.txt'.format(gitpath), delimiter=',', dtype='U')

panels = 'abcdefghijklmnopqrstuvwxyz'

fac_container = {}
meanenergy = True

#%%
# =============================================================================
# Data plots
# =============================================================================
# [0,13] paper
# [2,11,12] also interesting
for fctr in [0, 13]:
    #%%
    f = files[fctr]
    # find right dataframe entry for the image
    fmt = '%Y_%jT%H_%M_%S'
    thiset = tc.datetime2et(datetime.strptime(files[fctr], fmt))
    selec = imglist.loc[(imglist['ET_START']-thiset).abs().argmin()]
    # get magnetometer data
    starttime = selec['ET_START']
    stoptime = selec['ET_STOP']
    dtstart = tc.et2datetime(selec['ET_START'])
    dtstop = tc.et2datetime(selec['ET_STOP'])
    magobj = get_MAG.MAGdata()
    magobj.getMAG_PDS(starttime-2*24*3600, stoptime+2*24*3600)
    # set up some data and things
    inio = INCA_ion_mtof.INCAPA(magobj)
    ld = LEMMS_data.LEMMSdata(magobj)
    rp = get_RPWS_FULL.FULLRPWSdata()
    LATMIN=8
    LATMAX=22
    LONMIN = 0
    LONMAX = 360
    KR_MIN=1
    KR_MAX=20
    if '2017_009T11_30_12' in f:
        inio.setStart(2017,9,11,20, dt=1.5)
        charged = 0
        ld.setStart(2017,9,9,0, dt=9)
    elif '2017_074T00_45_22' in f:
        inio.setStart(2017,74,0,20, dt=1.67, alt=2)
        charged = 0
        ld.setStart(2017,73,23,0, dt=9)
    elif '2017_080T22_36_00' in f:
        inio.setStart(2017,80,22,30, dt=1.4, alt=2)
        charged = 0
        ld.setStart(2017,80,18,30, dt=9)
    elif '2017_081T03_05_04' in f:
        inio.setStart(2017,81,3,30, dt=2, alt=2)
        charged = 0
        ld.setStart(2017,81,0,30, dt=9)
        KR_MAX=30
    elif '2017_102T08_21_12' in f:
        #inio.setStart(2017,102,9,35, dt=1.5)
        inio.setStart(2017,102,9,50, dt=1.55)
        charged = 0
        ld.setStart(2017,102,6,0, dt=9)
        KR_MAX=30
#        beamstart = tc.datetime2et(datetime(2017,4,12,10,3))
#        beamstop = tc.datetime2et(datetime(2017,4,12,10,27))
        starttime = tc.datetime2et(datetime(2017,4,12,9,14))
        stoptime = tc.datetime2et(datetime(2017,4,12,10,23))
    else:
        f = files[fctr]
        inio.setStart(int(f[0:4]), int(f[5:8]), int(f[9:11])-1, 0, dt=3)
        ld.setStart(int(f[0:4]), int(f[5:8]), int(f[9:11])-2,0, dt=9)
    days = np.unique([datetime.strftime(dtstart+timedelta(days=iii), '%Y%j') for iii in [-1,0,1]])
    rp.getFULLRPWS(days)
    
    #%%
    
    tmp = dtstart-timedelta(hours=1)
    dtstart_data = datetime(tmp.year, tmp.month, tmp.day, tmp.hour)
    if '2017_102T08_21_12' in f:
        dtstart_data += timedelta(hours=1)
    tmp = dtstop+timedelta(hours=2)
    dtstop_data = datetime(tmp.year, tmp.month, tmp.day, tmp.hour)
    etstart_data = tc.datetime2et(dtstart_data)
    etstop_data = tc.datetime2et(dtstop_data)
    
    # get image data
    if '2017_080T22_36_00' in f:
        flist_tmp = ['2017_080T21_51_04',
                     '2017_080T22_13_28',
                     '2017_080T22_36_00',
                     '2017_080T22_58_32',
                     '2017_080T23_21_04',
                     ]
        images = None
        for fff in range(len(flist_tmp)):
            thisfile = '{}/{}.fits'.format(mainsavepath, flist_tmp[fff])
            image, angles, hem = get_image.getRedUVIS(thisfile)
            if images is None:
                images = np.full(((len(flist_tmp),)+np.shape(image)), np.nan)
            images[fff,:,:] = image
        image = np.nanmean(images, axis=0)
        
    else:
        try:
            thisfile = '{}/{}.fits'.format(mainsavepath, files[fctr])
            image, angles, hem = get_image.getRedUVIS(thisfile)
        except:
            tmp = '{}/UVIS/PDS_UVIS/PROJECTIONS_RES2_ISS/COUVIS_0058'.format(datapath)
            thisfile = '{}/{}.fits'.format(tmp, files[fctr])
            image, angles, hem = get_image.getRedUVIS(thisfile)
    latbins = np.linspace(0, 30, num=image.shape[1]+1)
    lonbins = np.linspace(0, 360, num=image.shape[0]+1)
    
    # select latitude    
    tmp = np.where((latbins>=LATMIN) & (latbins<=LATMAX))[0]
    latbins = latbins[tmp]
    image = image[:,tmp[:-1]]
    
    # select longitude    
    if LONMIN>LONMAX:
        lonbins = np.linspace(0, 720, num=image.shape[0]*2+1)
        image = np.tile(image, (2,1))
        LONMAX += 360
    tmp = np.where((lonbins>=LONMIN) & (lonbins<=LONMAX))[0]
    lonbins = lonbins[tmp]
    image = image[tmp[:-1],:]
    ltbins = lonbins/180*12

    
    # averaging box
    deg = 0.5
    latdiff = deg #deg
    londiff = 2*deg/180*12 #hr LT

    # get intensity at footprint for selected time
    ettimes = np.linspace(etstart_data, etstop_data, num=500)
    _, colat_n, colat_s, loct = cd.get_ionfootp(ettimes)
    colat = colat_n if hem=='North' else colat_s
    if colat[-1] > colat[0]:
        eqward = True
    else:
        eqward = False
    loct[loct<ltbins[0]] += 24
    # check where averaging box moves out of the image
    argcolat_min = np.digitize(colat-latdiff, latbins)-1
    argcolat_max = np.digitize(colat+latdiff, latbins)-1
    argloct_min = np.digitize(loct-londiff, ltbins)-1
    argloct_max = np.digitize(loct+londiff, ltbins)-1
    invalid = np.where((argcolat_min<0) | (argcolat_min>=len(latbins)-1) |
                        (argcolat_max<0) | (argcolat_max>=len(latbins)-1) |
                        (argloct_min<0) | (argloct_min>=len(lonbins)-1) |
                        (argloct_max<0) | (argloct_max>=len(lonbins)-1))
    argcolat_min[invalid] = 0
    argcolat_max[invalid] = 0
    argloct_min[invalid] = 0
    argloct_max[invalid] = 0
    
    # check where the exact footpoint has no intensity
    argcolat_ex = np.digitize(colat, latbins)-1
    argloct_ex = np.digitize(loct, ltbins)-1
    argcolat_ex[invalid] = 0
    argloct_ex[invalid] = 0
    invalid2 = np.where(np.isnan(image[argloct_ex, argcolat_ex]))
    
    footp_intens = np.array([np.nanmean(image[argloct_min[iii]:argloct_max[iii],
                                              argcolat_min[iii]:argcolat_max[iii]+1])
                            for iii in range(len(argloct_min))])
    footp_intens[invalid] = np.nan
    footp_intens[invalid2] = np.nan
    
    def setupAxes(ax, top_ax=None):
        ax.tick_params(axis='both', which='both', direction='out',
                       left=True, labelleft=True,
                       right=False, labelright=False,
                       top=False, labeltop=False,
                       bottom=True, labelbottom=False)
        ax.grid('on', which='both')
        ax.grid(which='minor', linewidth=0.2, color='0.7')
        tmpax = ax if top_ax is None else top_ax
        if not present:
            txt = tmpax.text(0.023, 0.95,
                             '({})'.format(next(pan_iter)),
                              transform=ax.transAxes, ha='center', va='top',
                              color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([patheffects.withStroke(linewidth=3, foreground='w')])
            
    def make_patch_spines_invisible(ax):
        ax.set_frame_on(True)
        ax.patch.set_visible(False)
        for sp in ax.spines.values():
            sp.set_visible(False)
    
    if True:
        shortplot = True #if '2017_009T11_30_12' in f else False
        # get some more data
        ld.update()
        cd.set_timeframe([etstart_data-300,etstop_data+300])
        cd.update()
        
        pan_iter = iter(panels)
        fig = plt.figure()
        mult = 0.85
        if present:
            mult *= presentfact*1.3
        fig.set_size_inches(14*mult ,11*mult if shortplot else 18*mult)
        NUM = 6 if shortplot else 10
        panelctr = iter(range(NUM))
        gs = gridspec.GridSpec(NUM, 2,
                               wspace = 0.12, width_ratios=(1,0.025),
                               hspace=0.12,
                               height_ratios=((1,0.25,)+(1,)*(NUM-4)+(0.5,)+(1,)
                                               if not shortplot else
                                              (1,0.25,)+(1,)*4))
                
        # plot image
        thisrow = next(panelctr)
        image[image<0.1] = 0.1
        ax = plt.subplot(gs[thisrow,0])
        ax.set_facecolor('gray')
        quad = ax.pcolormesh(ltbins, latbins, image.T, cmap=cmap_UV)
        quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
        
        tmp = np.arange(0, 74, 2)
        tmp = np.array([iii for iii in tmp if ((iii>=ltbins[0]) & (iii <=ltbins[-1]))])
        ax.set_xlim([ltbins[0], ltbins[-1]])
        ax.set_ylim([LATMAX, LATMIN])
        ax.set_xticks(tmp)
        ax.set_xticklabels(tmp % 24)
        ax.set_xlabel('LT (hr)')
        cbar = plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
        cbar.set_label('Intensity (kR)', labelpad=12, rotation=270)
        ax.set_ylabel('$\\theta_i$ (deg)')
        
        # label timestamp
        fmt = '%H:%M'
        txt = ax.text(0.01, 0.03, '{} {}-{}'.format(datetime.strftime(dtstart, '%Y-%j'),
                                        datetime.strftime(dtstart, fmt),
                                        datetime.strftime(dtstop, fmt)),
                transform=ax.transAxes,
                va='bottom', ha='left',
                color='w', fontsize=12)
        txt.set_path_effects([patheffects.withStroke(linewidth=4, foreground='k')])
        
        # extended orbit
        tmp = np.linspace(selec['ET_START']-3600*3, selec['ET_STOP']+3600*6, num=600)
        _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
        colat = colat_n if hem=='North' else colat_s
        loct[loct<ltbins[0]] += 24
        ax.plot(loct[:-10], colat[:-10], c='w', lw=1)
        
        # arrow
        tmp = (selec['ET_START']+selec['ET_STOP'])/2
        tmp = [tmp-120, tmp+120]
        _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
        colat = colat_n if hem=='North' else colat_s
        loct[loct<ltbins[0]] += 24
        arrowstyle = '->,head_length=9, head_width=3.75'
        ax.add_patch(patches.FancyArrowPatch((loct[0], colat[0]),
                                           (loct[1], colat[1]),
                                           arrowstyle=arrowstyle,
                                           color='crimson',
                                           linewidth=2))
        
        # direct footprint
        tmp = np.linspace(selec['ET_START'], selec['ET_STOP'], num=100)
        _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
        colat = colat_n if hem=='North' else colat_s
        loct[loct<ltbins[0]] += 24
        ax.plot(loct, colat, c='crimson', lw=2)
        
        # hour markers
        st = dtstart-timedelta(hours=3)
        start = datetime(st.year, st.month, st.day, st.hour)+timedelta(hours=1)
        tmp = np.array([start])
        while tmp[-1]+timedelta(hours=1) < dtstop+timedelta(hours=6):
            tmp = np.append(tmp, [tmp[-1]+timedelta(hours=1)])
        _, colat_n, colat_s, loct = cd.get_ionfootp(tc.datetime2et(tmp))
        colat = colat_n if hem=='North' else colat_s
        loct[loct<ltbins[0]] += 24
        ax.scatter(loct, colat, color='w', s=3, marker='o', zorder=10)
        # empty row for spacing
        thisrow = next(panelctr)
        
        # small box to show averaging
        _, colat_n, colat_s, loct = cd.get_ionfootp(selec['ET_START']-300)
        colat = colat_n if hem=='North' else colat_s
        loct[loct<ltbins[0]] += 24
        box = patches.Rectangle((loct[0]-londiff, colat[0]-latdiff), 2*londiff, 2*latdiff,
                                edgecolor = 'crimson', linewidth=1.5, facecolor='none')
        ax.add_patch(box)
        setupAxes(ax)
        ax.tick_params(axis='both', which='both', labelbottom=True)
        
        ax.tick_params(axis='both', which='both', direction='out',
                       left=True, labelleft=True,
                       right=True, labelright=True,
                       top=False, labeltop=False,
                       bottom=True, labelbottom=True)
        
        ###############################################################################
                
        thisrow = next(panelctr)
        ax_int = plt.subplot(gs[thisrow,0])
        ax_int.plot(ettimes, footp_intens, color='grey', zorder=5)
        tmp = np.where((ettimes>starttime) & (ettimes<stoptime))[0]
        ax_int.plot(ettimes[tmp], footp_intens[tmp], color='k', zorder=6)
        ax_int.set_ylabel('Intensity (kR)')
        ax_int.set_ylim([0, ax_int.get_ylim()[1]])
        
        
        # L shell
        L_tt = np.linspace(selec['ET_START']-3600*3, selec['ET_STOP']+3600*6, num=600)
        _, colat_n, colat_s, loct = cd.get_ionfootp(L_tt)
        colat = colat_n if hem=='North' else colat_s
        colat_tt = L_tt
        colat_val = np.copy(colat)
        colat_shell = colat_n_shell if hem=='North' else colat_s_shell
        tmp = np.digitize(colat, colat_shell)-1
        L_val = L[tmp]
#        ax_shell = plt.twinx(ax_int)
#        ax_shell.plot(L_tt, L_val, c='k', ls=':')
#        ax_shell.set_ylabel('L shell', rotation=270, labelpad=15)
#        ax_shell.set_ylim([0,45])
        
        # Cassini altitude
        alt_tt = np.linspace(selec['ET_START']-3600*3, selec['ET_STOP']+3600*6, num=600)
        _, radius, lat, loct = cd.get_locations(alt_tt, refframe='KRTP')
        alt_val = radius-1
#        ax_rad = plt.twinx(ax_int)
#        ax_rad.plot(alt_tt, alt_val, c='k', ls='--')
#        ax_rad.set_ylabel('Altitude (R$_\mathrm{s}$)', rotation=270, labelpad=15)
#        ax_rad.spines['right'].set_position(('axes', 1.08))
#        make_patch_spines_invisible(ax_rad)
#        ax_rad.spines['right'].set_visible(True)
#        ax_rad.set_ylim([1,6 if '2017_102T08_21_12' in f else 4.5])
#        ax_rad.plot(alt_tt[0], radius[0], color='k', ls=':', label='L shell')
#        ax_rad.plot(alt_tt[0], radius[0], color='k', ls='--', label='Altitude')
#        ax_rad.legend(loc=1, shadow=True, facecolor='w', edgecolor='k', ncol=2,
#                      fancybox=True)
        
#        setupAxes(ax_int, top_ax=ax_rad)
        setupAxes(ax_int)
    
        
        # plot MAG data
        magdata = ld.fullmag[(ld.fullmag['Datetime']>=dtstart_data-timedelta(minutes=10)) & 
                             (ld.fullmag['Datetime']<=dtstop_data+timedelta(minutes=10))]
        magdata['ET'] = tc.datetime2et(magdata['Datetime'].dt.to_pydatetime())
        tmp = np.where(np.diff(magdata['ET']) <=0)[0]
        magdata['ET'].iloc[tmp+1] = magdata['ET'].iloc[tmp+1]+0.1
        _, r, lat, loct = cd.get_locations(magdata['ET'], 'KRTP')
        lat = np.radians(lat)
        lon = loct/12*180
        Brint, Btint, Bpint = internal_field_model.Internal_Field(r, np.pi/2-lat, lon, model='Cassini11')
        magdata['BR'] = magdata['BX_KRTP(nT)']-Brint
        magdata['BT'] = magdata['BY_KRTP(nT)']-Btint
        magdata['BP'] = magdata['BZ_KRTP(nT)']-Bpint
        if '2017_102T08_21_12' in f:
            rangechange = magdata['BR'][magdata['BR']<-5].index
            magdata['BR'].at[rangechange[0]] = magdata['BR'].loc[rangechange-1]
            magdata['BT'].at[rangechange[0]] = magdata['BT'].loc[rangechange-1]
            magdata['BP'].at[rangechange[0]] = magdata['BP'].loc[rangechange-1]
        magdata['I'] = r*RS*np.cos(lat)*magdata['BP']*1e-9/MU0*(-1 if hem=='North' else 1) #A/rad
        thisrow = next(panelctr)
        ax_mag = plt.subplot(gs[thisrow,0], sharex=ax_int)
        ax_mag.plot(magdata['ET'], magdata['BR'], label='$B_r$', color='k')
        ax_mag.plot(magdata['ET'], magdata['BT'], label='$B_\\theta$', color='crimson')
        ax_mag.plot(magdata['ET'], magdata['BP'], label='$B_\\phi$', color='royalblue')
        ax_mag.set_ylabel('$B$ (nT)')
        if not present:
            ax_mag.legend(loc=1, shadow=True, facecolor='w', edgecolor='k', ncol=3,
                          fancybox=True)
        else:
            ax_mag.legend(bbox_to_anchor=(1.01, 1), loc='upper left',
                          shadow=True,
                          facecolor='w', edgecolor='k',
                          fancybox=True)
        setupAxes(ax_mag)
        
        # FAC current density
        fac = pd.DataFrame()
        fac['ET'] = np.arange(magdata['ET'].iloc[0], magdata['ET'].iloc[-1], 30)
        _, colat_n, colat_s, _ = cd.get_ionfootp(fac['ET'])
        spl_colat = sint.UnivariateSpline(fac['ET'], np.radians(colat_n if hem=='North' else colat_s), s=0)
        def radiusSaturn(colat):
            x = (1100+60268)*np.sin(colat)
            y = (1100+54364)*np.cos(colat)
            return np.sqrt(x**2+y**2)*1e3 #m
        fac['r'] = radiusSaturn(spl_colat(fac['ET'])) #m
        fac['th'] = spl_colat(fac['ET']) #rad
        fac['dth/det'] = np.abs(spl_colat.derivative()(fac['ET'])) #rad/s
        fac['dth/det'][fac['dth/det']<1e-5] = np.nan # invalid where footprint doesn't move
        spl_I = sint.UnivariateSpline(magdata['ET'], magdata['I'], s=len(magdata)*2e9) 
        fac['I'] = spl_I(fac['ET']) #A/rad
        fac['dI/det'] = spl_I.derivative()(fac['ET']) #A/rad/s
        fac['j'] = fac['dI/det'] / fac['r']**2 / np.sin(fac['th']) / fac['dth/det'] *(-1 if hem=='North' else 1) #A/m**2
                
        # auroral intensity [kR]
        val = np.where(np.isfinite(footp_intens))
        fac['kR'] = sint.UnivariateSpline(ettimes[val], footp_intens[val], s=0, ext=1)(fac['ET'])
        fac['kR'][fac['kR']==0] = np.nan
        # energy flux [W/m**2]
        fac['Ef'] = fac['kR']*0.1e-3
        
        # mean electron energy
        fac['Wmean'] = fac['Ef']/fac['j']
        fac_container[files[fctr]] = fac.copy()
        
        # loss cone angle
        magdata['BTOT'] = np.sqrt(Brint**2+Btint**2+Bpint**2)
        fac['B_sc'] = sint.UnivariateSpline(magdata['ET'], magdata['BTOT'], s=0, ext=1)(fac['ET'])
        Br, Bt, Bp = internal_field_model.Internal_Field((fac['r']+10e6)/58232e3, fac['th'], 0,
                                                     model='Cassini11')
        fac['B_ion'] = np.sqrt(Br**2+Bt**2+Bp**2)
        fac['loss_cone_deg'] = np.degrees(np.arcsin(fac['B_sc']/fac['B_ion']))
                
        thisrow = next(panelctr)
        ax_I = plt.subplot(gs[thisrow,0], sharex=ax_int)
        ax_I.plot(fac['ET'], fac['I']/1e6, color='k')
        ax_I.set_ylabel('$I_P$ (MA/rad)')
        
        ax_colat = plt.twinx(ax_I)
        ax_colat.plot(fac['ET'][0], fac['I'][0]/1e6, c='k', label='$I_P$')
        ax_colat.plot(fac['ET'], np.degrees(fac['th']), c='dimgrey', ls='--', label='$\\theta_i$')
        ax_colat.set_ylabel('$\\theta_i$ (deg)', rotation=270, labelpad=15)
        if not present:
            ax_colat.legend(loc=4, shadow=True, facecolor='w', edgecolor='k', ncol=2,
                            fancybox=True)
        setupAxes(ax_I, top_ax=ax_colat)
        
        
        thisrow = next(panelctr)
        ax_j = plt.subplot(gs[thisrow,0], sharex=ax_int)
        ax_j.plot(fac['ET'], fac['j']*1e9, color='k')
        ax_j.fill_between(fac['ET'], fac['j']*1e9, color='crimson',
                          where=fac['j']>0, alpha=0.3, label='upward')
        ax_j.fill_between(fac['ET'], fac['j']*1e9, color='royalblue',
                          where=fac['j']<0, alpha=0.3, label='downward')
        ax_j.set_ylabel('$j_\parallel$ (nA/m$^2$)')
        if not present:
            ax_j.legend(loc=1, shadow=True, facecolor='w', edgecolor='k', ncol=2,
                        fancybox=True)
        setupAxes(ax_j)
        ax = ax_j
                    
        if not shortplot:
#            # plot LEMMS protons
#            thisrow = next(panelctr)
#            ax_lemms_p = plt.subplot(gs[thisrow,0], sharex=ax_int)
#            channels = np.array(['A{}'.format(iii) for iii in range(0, 6)])
#            for iii in range(len(channels)):
#                # get color
#                l = len(channels)
#                v = iii*1/l
#                c = cmap_SPEC_short(v)
#                # get data
#                xs = ld.accdata['Start_Ephemeris_s']
#                ys = ld.accdata[channels[iii]]
#                ys[ys<1e-5] = np.nan
#                ax_lemms_p.plot(xs, ys, color=c, lw=0.5)
#                ax_lemms_p.scatter(xs, ys, color=c, s=1)
#                cal = ld.acccalib[(ld.acccalib['Channel'] == channels[iii][0]) &
#                                  (ld.acccalib['Channel_Index'] == channels[iii][1])]
#                tmp = ax_lemms_p.get_position().bounds
#                ax_lemms_p.text(tmp[0]+tmp[2]+0.01,
#                                tmp[1]+tmp[3]-(iii+0.5)/len(channels)*(tmp[3]),
#                                '{}-{} keV'.format(int(cal['Low']),int(cal['Hi'])),
#                                color=c, transform=fig.transFigure)
#            
#            ax_lemms_p.set_yscale('log')
#            ax_lemms_p.set_ylim([5e-3, 1e3])
#            ax_lemms_p.set_ylabel('Diff. proton flux\n(/s/sr/cm$^2$/keV)')    
#            setupAxes(ax_lemms_p)
            
            # get CHEMS spectrogram and plot
            tmp = f[:8]
            chemspath = '{}/MIMI_CHEMS'.format(datapath)
            chemsfile = glob.glob('{}/*{}*'.format(chemspath, tmp))[0]
            chems_en = np.genfromtxt(chemsfile, max_rows=1)[2:]
            chems_starttimes = np.genfromtxt(chemsfile, usecols=(0,1,2), dtype=str)[1:]
            chems_starttimes = np.array([chems_starttimes[iii,0]+ '_' +
                                         chems_starttimes[iii,1]+ '_' +
                                         chems_starttimes[iii,2]
                                         for iii in range(chems_starttimes.shape[0])])
            fmt = '%Y_%j_%H:%M:%S.%f'
            chems_dtstart = np.array([datetime.strptime(iii, fmt) for iii in chems_starttimes])
            chems_data = np.genfromtxt(chemsfile,
                                       usecols=np.arange(4,len(chems_en)+4),
                                       skip_header=1)
            thisrow = next(panelctr)
            ax_chems = plt.subplot(gs[thisrow,0], sharex=ax_int)
#            fig, ax_chems = plt.subplots()
            ax_chems.set_facecolor('dimgrey')
            quad = ax_chems.pcolormesh(tc.datetime2et(chems_dtstart),
                                        chems_en,
                                        chems_data.T,
                                        cmap=cmap_data)
            quad.set_norm(mcolors.LogNorm(1e-1, 3e0))
            ax_chems.set_yscale('log')
            ax_chems.set_ylabel('Energy (keV)')  
            cbar = plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
            cbar.set_label('Diff. proton flux\n(/s/sr/cm$^2$/keV)', labelpad=27, rotation=270)
            setupAxes(ax_chems)
            
            # plot LEMMS electrons
            thisrow = next(panelctr)
            ax_lemms_e = plt.subplot(gs[thisrow,0], sharex=ax_int)
            channels = np.array(['C{}'.format(iii) for iii in range(1,7)])
            for iii in range(len(channels)):
                # get color
                l = len(channels)
                v = iii*1/l
                c = cmap_data_lines(v)
                # get data
                xs = ld.accdata['Start_Ephemeris_s']
                ys = ld.accdata[channels[iii]]
                ys[ys<1e-5] = np.nan
                ax_lemms_e.plot(xs, ys, color=c, lw=0.8)
                ax_lemms_e.scatter(xs, ys, color=c, s=1)
                cal = ld.acccalib[(ld.acccalib['Channel'] == channels[iii][0]) &
                                  (ld.acccalib['Channel_Index'] == channels[iii][1])]
                tmp = ax_lemms_e.get_position().bounds
                ax_lemms_e.text(tmp[0]+tmp[2]+0.01,
                                tmp[1]+tmp[3]-(iii+0.5)/len(channels)*(tmp[3]),
                                '{}-{} keV'.format(int(cal['Low']),int(cal['Hi'])),
                                color=c, transform=fig.transFigure)
            
            ax_lemms_e.set_yscale('log')
            ax_lemms_e.set_ylim([1e-2, 1e2])
            ax_lemms_e.set_ylabel('Diff. electron flux\n(/s/sr/cm$^2$/keV)')
            setupAxes(ax_lemms_e)
            
            # LEMMS pitch angle
            thisrow = next(panelctr)
            ax_lemms_pa = plt.subplot(gs[thisrow,0], sharex=ax_int)
            ax_lemms_pa.plot(ld.accdata['Start_Ephemeris_s'], ld.LEMMS_PA_LET, c='k', ls='-')
            ax_lemms_pa.set_ylabel('LEMMS\npitch angle\n(deg)')
            ax_lemms_pa.set_ylim([0,180])
            ax_lemms_pa.set_yticks([0,45,90,135,180])
            ax_lemms_pa.text(0.1, 0.9, 'upward' if hem=='South' else 'downward', va='top', ha='left',
                             transform=ax_lemms_pa.transAxes)
            ax_lemms_pa.text(0.1, 0.1, 'upward' if hem=='North' else 'downward', va='bottom', ha='left',
                             transform=ax_lemms_pa.transAxes)
            setupAxes(ax_lemms_pa)
            
            ax_rpws_lp = ax_lemms_pa.twinx()
            ax_rpws_lp.plot(cd.crpws.NEPROXY['ET'],
                            cd.crpws.NEPROXY['ELECTRON_NUMBER_DENSITY'],
                            c='crimson', lw=1)
            ax_rpws_lp.scatter(cd.crpws.NEPROXY['ET'], 
                               cd.crpws.NEPROXY['ELECTRON_NUMBER_DENSITY'],
                               color='crimson', s=2)
            ax_rpws_lp.set_yscale('log')
            ax_rpws_lp.set_ylabel('n$_e$ (cm$^{-3}$)', rotation=270, labelpad=14, color='crimson')
#            ax_rpws_lp.grid('off')
            #---------------
            # get RPWS stuff
            thisrow = next(panelctr)
            ax_rpws = plt.subplot(gs[thisrow,0], sharex=ax_int)
            
            quad = ax_rpws.pcolormesh(cd.crpws.ettimes_orig,
                                      cd.crpws.freq_orig,
                                      cd.crpws.data_orig.T,
                                      cmap=cmap_data,
                                      vmin=0,
                                      vmax=40)
            ax_rpws.set_yscale('log')
            ax_rpws.set_ylim([1e0, 5e6])
            ax_rpws.set_ylabel('Frequency (Hz)')
            cbar = plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
            cbar.set_label('dB over background', labelpad=12, rotation=270)
            cbar.set_ticks(np.arange(0,31,10))
            mp = 1.6726e-27 #kg
            qp = 1.602e-19 #C
            B = np.sqrt(Brint**2+Btint**2+Bpint**2)*1e-9
            fp = qp*B/2/np.pi/mp
            fe = 2.8e10*B
    #        ax_rpws.plot(magdata['ET'], fp, color='k', ls='-', lw=2.5)
            ax_rpws.plot(magdata['ET'], fp, color='crimson', ls='-', lw=1.5)
            txt = ax_rpws.text(etstart_data+800, fp[0]+20, '$f_{cp}$', color='crimson',
                               ha='left', va='bottom', fontsize=15)
            txt.set_path_effects([patheffects.withStroke(linewidth=2, foreground='lightgrey')])
    #        ax_rpws.plot(magdata['ET'], fe, color='k', ls='-', lw=2.5)
            ax_rpws.plot(magdata['ET'], fe, color='crimson', ls='-', lw=1.5)
            txt = ax_rpws.text(etstart_data+800, fe[0]+10000, '$f_{ce}$', color='crimson',
                               ha='left', va='bottom', fontsize=15)
            txt.set_path_effects([patheffects.withStroke(linewidth=2, foreground='lightgrey')])
#            if '2017_009T11_30_12' in f:
#                ax_rpws.add_patch(patches.Ellipse((8.7/16, 0.4),
#                                                   4.5/16, 0.5,
#                                                   edgecolor='cornflowerblue',
#                                                   facecolor=None,
#                                                   linewidth=1.5, fill=False,
#                                                   transform=ax_rpws.transAxes,
#                                                   zorder=4))
#                ax_rpws.add_patch(patches.Ellipse((8.7/16, 0.4),
#                                                   4.5/16, 0.5,
#                                                   edgecolor='lightgrey',
#                                                   facecolor=None,
#                                                   linewidth=3, fill=False,
#                                                   transform=ax_rpws.transAxes,
#                                                   zorder=3))
            setupAxes(ax_rpws)
            ax_rpws.grid('off')
            ax = ax_rpws
        
        #---------------------
        # set xrange and stuff
        xticks = np.array([dtstart_data])
        while xticks[-1]+timedelta(minutes=15) < dtstop_data+timedelta(minutes=15):
            xticks = np.append(xticks, [xticks[-1]+timedelta(minutes=15)])
        ax.set_xticks(tc.datetime2et(xticks), minor=True)
        
        ax.set_xlim([etstart_data, etstop_data])
        xticks = np.array([dtstart_data])
        while xticks[-1]+timedelta(hours=1) < dtstop_data+timedelta(hours=1):
            xticks = np.append(xticks, [xticks[-1]+timedelta(hours=1)])
        ax.set_xticks(tc.datetime2et(xticks))
        
        Lticks = L_val[np.digitize(tc.datetime2et(xticks), L_tt)-1]
        colatticks = colat_val[np.digitize(tc.datetime2et(xticks), colat_tt)-1]
        altticks = alt_val[np.digitize(tc.datetime2et(xticks), alt_tt)-1]
        
        if not present:
            labels = ['{}\n{:0.1f}\n{:0.1f}\n{:0.1f}'.format(
                    datetime.strftime(xticks[iii], '%H:%M'),
                    colatticks[iii],
                    Lticks[iii],
                    altticks[iii],
                    ) for iii in range(len(xticks))]
            
            ax.set_xticklabels(labels)
            ax.tick_params(axis='both', which='both', labelbottom=True)
    #        ax.set_xlabel('UTC')
            ax.text(-0.05, -0.075, 'UTC\n$\\theta_i$ (deg)\nL shell\nAltitude (R$_\mathrm{{S}}$)',
                    ha='right', va='top', transform=ax.transAxes)
        else:
            labels = ['{}\n{:0.1f}\n{:0.1f}'.format(
                    datetime.strftime(xticks[iii], '%H:%M'),
                    Lticks[iii],
                    altticks[iii],
                    ) for iii in range(len(xticks))]
            
            ax.set_xticklabels(labels)
            ax.tick_params(axis='both', which='both', labelbottom=True)
    #        ax.set_xlabel('UTC')
            ax.text(-0.05, -0.09, 'UTC\nL shell\nAltitude (R$_\mathrm{{S}}$)',
                    ha='right', va='top', transform=ax.transAxes)
                
        plt.savefig('{}/low_pass_{}{}{}.png'.format(plotsavepath,
                    thisfile.split('/')[-1].split('\\')[-1].split('.')[0],
                    '_short' if shortplot else '', '_present' if present else ''),
                    bbox_inches='tight', dpi=500)
        plt.savefig('{}/low_pass_{}{}_lowres{}.png'.format(plotsavepath,
                    thisfile.split('/')[-1].split('\\')[-1].split('.')[0],
                    '_short' if shortplot else '', '_present' if present else ''),
                    bbox_inches='tight', dpi=200)
        plt.close()
    #%%
    for species in ['H']:
        try:
            inio.update(charged=0, species=species)
#            if len(inio.etstart) > 1:
#                tmp = inio.plotLarge('{}/UVIS_low_passes/inca'.format(plotpath), species=species,
#                                     fp_intens=footp_intens, ettimes=ettimes, eqward=eqward)
#                inio.plotLargeSunangle('{}/UVIS_low_passes/inca'.format(plotpath))
#                if not tmp:
#                    inio.plot('{}/UVIS_low_passes/inca'.format(plotpath), species=species)
        except Exception as e:
            print(e)
        
#%%
# plotting wave + particle overview
    if shortplot:
        print('Plotting wave + particle overview...')
        fmt = '%Y_%j'
        tmp = datetime.strftime(tc.et2datetime(inio.etstart[0]), fmt)
                        
        data = np.copy(inio.images)
        data[data==0] = np.nan
            
        cols = data[::inio.alt].shape[0]
        rows = 6
        tttiter = iter(range(rows+5+1))
        
        pan_iter = iter(panels)
        
        if cols < 16:

            hr = (1,)*rows+(0.2,)+(2,)*3+(2,)*2
            wr = (1,)*cols+(0.1,)*2
            gs = gridspec.GridSpec(rows+5+1, cols+2,
                                   width_ratios=wr, height_ratios=hr,
                                   wspace=0.07, hspace=0.1)
            fig = plt.figure()
            mult = 0.3
            if present:
                mult *= presentfact*1.1
            fig.set_size_inches(mult*3*sum(wr), mult*4.2*sum(hr))
            
            for ttt in range(1, data.shape[1]-1):
                cmax = inio.lim[ttt,1]
                cmin = inio.lim[ttt,0]
                thisrow = next(tttiter)
                
                for iii in range(data[::inio.alt].shape[0]):
                    ax = plt.subplot(gs[thisrow, iii])
                    ax.set_facecolor('dimgrey')
                    cont = ax.contour(inio.pamaps[::inio.alt][iii,:,:],
                                      levels=np.arange(30,180,30, dtype=np.int),
                                      colors='gold')
                    ax.clabel(cont, inline=1, fontsize=8, fmt='%d$^\circ$')
                    quad = ax.pcolormesh(data[::inio.alt][iii,ttt,:,:], cmap=cmap_data)
                    quad.set_norm(mcolors.LogNorm(cmin, cmax))
                    if not thisrow:
                        fmt = '%H:%M'
                        tmp = '{}'.format(datetime.strftime(tc.et2datetime(
                                inio.etstart[::inio.alt][iii]), fmt))
                        if present:
                            ax.set_title(tmp, fontsize=10)
                        else:
                            ax.set_title(tmp)
                    ax.set_xticks([])
                    ax.set_yticks([])
                    if not iii:
                        low = inio.calib[inio.calib['Channel_Index'].astype(int)==ttt]['Low']
                        hi = inio.calib[inio.calib['Channel_Index'].astype(int)==ttt]['Hi']
                        fmt = '%Y-%j'
                        tmp = '{}-{} keV'.format(int(low), int(hi))
                        if present:
                            ax.set_ylabel(tmp, rotation=0, labelpad=35)
                        else:
                            ax.set_ylabel(tmp)
                        plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
                    if not iii and not thisrow and not present:
                        txt = ax.text(0.1, 0.9,
                                         '({})'.format(next(pan_iter)),
                                          transform=ax.transAxes, ha='left', va='top',
                                          color='k', fontweight='bold', fontsize=15)
                        txt.set_path_effects([patheffects.withStroke(linewidth=3, foreground='w')])
                            
                fmt = '%Y_%j'
                tmp = datetime.strftime(tc.et2datetime(inio.etstart[0]), fmt)
            ax.text(2.5 if present else 2.1, rows/2*1.1, 'Diff. particle flux (/s/sr/cm$^2$/keV)',
                     va='center', ha='left', rotation=270, fontsize=11,
                     transform=ax.transAxes, color='k')
            
            thisrow = next(tttiter) # skip empty row for spacing
            # intensity
            thisrow = next(tttiter)
            
#            ax_int = plt.subplot(gs[thisrow, :-2])
#            ax_int.plot(tc.et2datetime(ettimes), footp_intens, 'k-', lw=2)
#            ax_int.fill_between(tc.et2datetime(ettimes), footp_intens, color='k', alpha=0.15)
#            ax_int.set_ylabel('Intensity (kR)')
#            ax_int.grid('on')
             
            # FACs
#            ax_j = plt.twinx(ax_int)
            ax_j = plt.subplot(gs[thisrow, :-2])
            ax_j.plot(tc.et2datetime(fac['ET']), fac['j']*1e9, color='k')
#            ax_j.fill_between(tc.et2datetime(fac['ET']), fac['j']*1e9, color='k', alpha=0.2)
            ax_j.fill_between(tc.et2datetime(fac['ET']), fac['j']*1e9, color='crimson',
                          where=fac['j']>0, alpha=0.3, label='upward')
            ax_j.fill_between(tc.et2datetime(fac['ET']), fac['j']*1e9, color='royalblue',
                              where=fac['j']<0, alpha=0.3, label='downward')
            ax_j.set_ylabel('$j_\parallel$ (nA/m$^2$)')
            if not present:
                ax_j.legend(loc=1, shadow=True, facecolor='w', edgecolor='k', ncol=2,
                            fancybox=True)
#            ax_j.set_ylabel('$j_\parallel$ (nA/m$^2$)', rotation=270, labelpad=15, color='crimson')
#            ax_j.tick_params(axis='y', colors='crimson')
            
#            imin, imax = ax_int.get_ylim()
#            jmin, jmax = ax_j.get_ylim()
#            neg_frac = jmin/jmax
#            if neg_frac>0:
#                ax_j.set_ylim([0, jmax])
#            else:
#                ax_int.set_ylim([neg_frac*imax, imax])
                
#            txt = ax_j.text(0.023, 0.95,
#                             '({})'.format(next(pan_iter)),
#                              transform=ax_j.transAxes, ha='center', va='top',
#                              color='k', fontweight='bold', fontsize=15)
#            txt.set_path_effects([patheffects.withStroke(linewidth=3, foreground='w')])
            
            if not eqward is None:
                left = 'poleward' if eqward else 'equatorward'
                right = 'equatorward' if eqward else 'poleward'
                fs_tmp = 12 if present else 15
                ax_j.text(0.01, 1.05, left, ha='left', va='bottom', fontweight='bold',
                            transform = ax_j.transAxes, fontsize=fs_tmp)
                ax_j.text(0.99, 1.05, right, ha='right', va='bottom', fontweight='bold',
                            transform = ax_j.transAxes, fontsize=fs_tmp)
            
            setupAxes(ax_j)
            ax_int = ax_j

            # Cassini altitude
            alt_tt = np.linspace(inio.etstart[0]-60, inio.etstop[-1]+60, num=600)
            _, radius, lat, loct = cd.get_locations(alt_tt, refframe='KRTP')
            alt_val = radius-1
            
            # L shell
            L_tt = np.linspace(inio.etstart[0]-60, inio.etstop[-1]+60, num=600)
            _, colat_n, colat_s, loct = cd.get_ionfootp(L_tt)
            colat = colat_n if hem=='North' else colat_s
            colat_tt = L_tt
            colat_val = np.copy(colat)
            colat_shell = colat_n_shell if hem=='North' else colat_s_shell
            tmp = np.digitize(colat, colat_shell)-1
            L_val = L[tmp]
                        
            

            # get CHEMS spectrogram and plot
            tmp = f[:8]
            chemspath = '{}/MIMI_CHEMS'.format(datapath)
            chemsfile = glob.glob('{}/*{}*'.format(chemspath, tmp))[0]
            chems_en = np.genfromtxt(chemsfile, max_rows=1)[2:]
            chems_starttimes = np.genfromtxt(chemsfile, usecols=(0,1,2), dtype=str)[1:]
            chems_starttimes = np.array([chems_starttimes[iii,0]+ '_' +
                                         chems_starttimes[iii,1]+ '_' +
                                         chems_starttimes[iii,2]
                                         for iii in range(chems_starttimes.shape[0])])
            fmt = '%Y_%j_%H:%M:%S.%f'
            chems_dtstart = np.array([datetime.strptime(iii, fmt) for iii in chems_starttimes])
            chems_data = np.genfromtxt(chemsfile,
                                       usecols=np.arange(4,len(chems_en)+4),
                                       skip_header=1)
            thisrow = next(tttiter)
            ax_chems = plt.subplot(gs[thisrow,:-2], sharex=ax_int)
#            fig, ax_chems = plt.subplots()
            ax_chems.set_facecolor('dimgrey')
            quad = ax_chems.pcolormesh(chems_dtstart,
                                        chems_en,
                                        chems_data.T,
                                        cmap=cmap_data)
            quad.set_norm(mcolors.LogNorm(1e-1, 3e0))
            ax_chems.set_yscale('log')
            ax_chems.set_ylabel('Energy (keV)')  
            cbar = plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
            cbar.set_label('Diff. proton flux\n(/s/sr/cm$^2$/keV)', labelpad=27, rotation=270)
            setupAxes(ax_chems)
                                        
#            # plot LEMMS protons
#            thisrow = next(tttiter)
#            ax_lemms_p = plt.subplot(gs[thisrow,:-2], sharex=ax_int)
#            channels = np.array(['A{}'.format(iii) for iii in range(1, 5)])
#            for iii in range(len(channels)):
#                # get color
#                l = len(channels)
#                v = iii*1/l
#                c = cmap_SPEC_short(v)
#                # get data
#                xs = tc.et2datetime(ld.accdata['Start_Ephemeris_s'])
#                ys = ld.accdata[channels[iii]]
#                ys[ys<1e-5] = np.nan
#                ax_lemms_p.plot(xs, ys, color=c, lw=1)
#                ax_lemms_p.scatter(xs, ys, color=c, s=4)
#                cal = ld.acccalib[(ld.acccalib['Channel'] == channels[iii][0]) &
#                                  (ld.acccalib['Channel_Index'] == channels[iii][1])]
#                tmp = ax_lemms_p.get_position().bounds
#                ax_lemms_p.text(1.01, 0.9-iii/7,
#                                '{}-{} keV'.format(int(cal['Low']),int(cal['Hi'])),
#                                ha='left', va='top',
#                                color=c, transform=ax_lemms_p.transAxes)
#            
#            ax_lemms_p.set_yscale('log')
#            ax_lemms_p.set_ylim([4e-2, 2e1])
#            ax_lemms_p.set_ylabel('Diff. proton flux\n(/s/sr/cm$^2$/keV)')    
#            setupAxes(ax_lemms_p)
            
            # plot LEMMS electrons
            thisrow = next(tttiter)
            ax_lemms_e = plt.subplot(gs[thisrow,:-2], sharex=ax_int)
            channels = np.array(['C{}'.format(iii) for iii in range(2,6)])
            for iii in range(len(channels)):
                # get color
                l = len(channels)
                v = iii*1/l
                c = cmap_data_lines(v)
                # get data
                xs = tc.et2datetime(ld.accdata['Start_Ephemeris_s'])
                ys = ld.accdata[channels[iii]]
                ys[ys<1e-5] = np.nan
                ax_lemms_e.plot(xs, ys, color=c, lw=1)
                ax_lemms_e.scatter(xs, ys, color=c, s=4)
                cal = ld.acccalib[(ld.acccalib['Channel'] == channels[iii][0]) &
                                  (ld.acccalib['Channel_Index'] == channels[iii][1])]
                tmp = ax_lemms_e.get_position().bounds
                ax_lemms_e.text(1.01, 0.9-iii/7,
                                '{}-{} keV'.format(int(cal['Low']),int(cal['Hi'])),
                                ha='left', va='top',
                                color=c, transform=ax_lemms_e.transAxes)            
            ax_lemms_e.set_yscale('log')
            ax_lemms_e.set_ylim([9e-2, 2e1])
            ax_lemms_e.set_ylabel('Diff. electron flux\n(/s/sr/cm$^2$/keV)')
            setupAxes(ax_lemms_e)
            
#            # LEMMS pitch angle
#            thisrow = next(tttiter)
#            ax_lemms_pa = plt.subplot(gs[thisrow,:-2], sharex=ax_int)
#            ax_lemms_pa.plot(tc.et2datetime(ld.accdata['Start_Ephemeris_s']),
#                             ld.LEMMS_PA_LET, c='k', ls='-')
#            ax_lemms_pa.set_ylabel('LEMMS\npitch angle\n(deg)')
#            ax_lemms_pa.set_ylim([0,180])
#            ax_lemms_pa.set_yticks([0,45,90,135,180])
#            ax_lemms_pa.text(0.1, 0.9, 'upward' if hem=='South' else 'downward', va='top', ha='left',
#                             transform=ax_lemms_pa.transAxes)
#            ax_lemms_pa.text(0.1, 0.1, 'upward' if hem=='North' else 'downward', va='bottom', ha='left',
#                             transform=ax_lemms_pa.transAxes)
#            setupAxes(ax_lemms_pa)
#            
#            # electron density
#            ax_rpws_lp = ax_lemms_pa.twinx()
#            ax_rpws_lp.plot(tc.et2datetime(cd.crpws.NEPROXY['ET']),
#                            cd.crpws.NEPROXY['ELECTRON_NUMBER_DENSITY'],
#                            c='crimson', lw=1)
#            ax_rpws_lp.scatter(tc.et2datetime(cd.crpws.NEPROXY['ET']), 
#                               cd.crpws.NEPROXY['ELECTRON_NUMBER_DENSITY'],
#                               color='crimson', s=2)
#            ax_rpws_lp.set_yscale('log')
#            ax_rpws_lp.set_ylabel('n$_e$ (cm$^{-3}$)', rotation=270, labelpad=14, color='crimson')
#            ax_rpws_lp.grid('off')
            
            mp = 1.6726e-27 #kg
            qp = 1.602e-19 #C
            B = np.sqrt(Brint**2+Btint**2+Bpint**2)*1e-9
            fp = qp*B/2/np.pi/mp
            fe = 2.8e10*B

            # plot RPWS Ex
            thisrow = next(tttiter)
            ax_rpws_ex = plt.subplot(gs[thisrow,:-2], sharex=ax_int)
            ax_rpws_ex.set_facecolor('dimgrey')
            fmax = 0
            for nnn in [*rp.specdens]:
                if rp.receiver[nnn] not in ['MFD', 'LFR']:
                    continue
                fmax = np.max([fmax, np.max(rp.fcent[nnn])])
                valid = np.where(rp.detector[nnn] == 0)[0]
                if len(valid):
                    times = rp.dt[nnn][valid]
                    specdens = rp.specdens[nnn][valid,:]
                    specdens[specdens<=0] = np.nan
                    # insert nan in gaps
                    diff = np.median(np.diff(times))
                    tmp = np.where(np.diff(times)>2*diff)[0]
                    lo = times[tmp]+diff
                    hi = times[tmp+1]-diff
                    ins_times = [val for pair in zip(lo, hi) for val in pair]
                    times = np.insert(times, np.repeat(tmp, 2)+1, ins_times)
                    specdens = np.insert(specdens, np.repeat(tmp, 2),
                                         np.full((specdens.shape[1]), np.nan), axis=0)
                    quad = ax_rpws_ex.pcolormesh(times, rp.fcent[nnn], specdens.T, cmap=cmap_data)
                    quad.set_norm(mcolors.LogNorm(1e-9,5e-7))
            cbar = plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
            if present:
                cbar.set_label('E$_\mathrm{x}$ spec dens\n(V$^2$/m$^2$/Hz)', labelpad=27, rotation=270)
            else:
                cbar.set_label('E$_\mathrm{x}$ spectral density\n(V$^2$/m$^2$/Hz)', labelpad=27, rotation=270)
            ax_rpws_ex.set_yscale('log')
            ax_rpws_ex.set_ylim([1, fmax])
            if present:
                ax_rpws_ex.set_ylabel('f (Hz)')
            else:
                ax_rpws_ex.set_ylabel('Frequency (Hz)')
            ax_rpws_ex.plot(tc.et2datetime(magdata['ET']), fp, color='crimson', ls='-', lw=1.5)
            txt = ax_rpws_ex.text(0.025, 0.4,
                                  '$f_{c}$', color='crimson',
                                  ha='left', va='bottom',
                                  transform=ax_rpws_ex.transAxes, fontsize=15)
            txt.set_path_effects([patheffects.withStroke(linewidth=2, foreground='lightgrey')])
            setupAxes(ax_rpws_ex)
            
            # plot RPWS Bx
            thisrow = next(tttiter)
            ax_rpws_bx = plt.subplot(gs[thisrow,:-2], sharex=ax_int)
            ax_rpws_bx.set_facecolor('dimgrey')
            fmax = 0
            for nnn in [*rp.specdens]:
                if rp.receiver[nnn] not in ['MFD', 'LFR']:
                    continue
                fmax = np.max([fmax, np.max(rp.fcent[nnn])])
                valid = np.where(rp.detector[nnn] == 4)[0]
                if len(valid):
                    times = rp.dt[nnn][valid]
                    specdens = rp.specdens[nnn][valid,:]
                    specdens[specdens<=0] = np.nan
                    # insert nan in gaps
                    diff = np.median(np.diff(times))
                    tmp = np.where(np.diff(times)>2*diff)[0]
                    lo = times[tmp]+diff
                    hi = times[tmp+1]-diff
                    ins_times = [val for pair in zip(lo, hi) for val in pair]
                    times = np.insert(times, np.repeat(tmp, 2)+1, ins_times)
                    specdens = np.insert(specdens, np.repeat(tmp, 2),
                                         np.full((specdens.shape[1]), np.nan), axis=0)
                    quad = ax_rpws_bx.pcolormesh(times, rp.fcent[nnn], specdens.T, cmap=cmap_data)
                    quad.set_norm(mcolors.LogNorm(1e-8,9e-5))
            cbar = plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
            if present:
                cbar.set_label('B$_\mathrm{x}$ spec dens\n(nT$^2$/Hz)', labelpad=27, rotation=270)
            else:
                cbar.set_label('B$_\mathrm{x}$ spectral density\n(nT$^2$/Hz)', labelpad=27, rotation=270)
            ax_rpws_bx.set_yscale('log')
            ax_rpws_bx.set_ylim([1, fmax])
            if present:
                ax_rpws_bx.set_ylabel('f (Hz)')
            else:
                ax_rpws_bx.set_ylabel('Frequency (Hz)')
            ax_rpws_bx.plot(tc.et2datetime(magdata['ET']), fp, color='crimson', ls='-', lw=1.5)
            txt = ax_rpws_bx.text(0.025, 0.4,
                                  '$f_{c}$', color='crimson',
                                  ha='left', va='bottom',
                                  transform=ax_rpws_bx.transAxes, fontsize=15)
            txt.set_path_effects([patheffects.withStroke(linewidth=2, foreground='lightgrey')])
            setupAxes(ax_rpws_bx)
             
            # sort out ticks with ancillary information
            ax = ax_rpws_bx
            ax.tick_params(axis='both', which='both', direction='out',
                           bottom=True, labelbottom=True)
            ax.set_xlim(tc.et2datetime([inio.etstart[0], inio.etstop[-1]]))
            
            dtstart = tc.et2datetime(inio.etstart[0])
            d = datetime(dtstart.year, dtstart.month, dtstart.day, dtstart.hour)
            while d < dtstart:
                d = d + timedelta(minutes=15)
            xticks = np.array([d])
            while xticks[-1]+timedelta(minutes=15) < tc.et2datetime(inio.etstop[-1]):
                xticks = np.append(xticks, [xticks[-1]+timedelta(minutes=15)])
            ax.set_xticks(xticks)
            
            Lticks = L_val[np.digitize(tc.datetime2et(xticks), L_tt)-1]
            colatticks = colat_val[np.digitize(tc.datetime2et(xticks), colat_tt)-1]
            altticks = alt_val[np.digitize(tc.datetime2et(xticks), alt_tt)-1]
            if not present:
                labels = ['{}\n{:0.1f}\n{:0.1f}\n{:0.1f}'.format(
                        datetime.strftime(xticks[iii], '%H:%M'),
                        colatticks[iii],
                        Lticks[iii],
                        altticks[iii],
                        ) for iii in range(len(xticks))]
                
                ax.set_xticklabels(labels)
                ax.tick_params(axis='both', which='both', labelbottom=True)
                ax.text(-0.05, -0.06, 'UTC\n$\\theta_i$ (deg)\nL shell\nAltitude (R$_\mathrm{{S}}$)',
                        ha='right', va='top', transform=ax.transAxes)
            else:
                labels = ['{}\n{:0.1f}\n{:0.1f}'.format(
                        datetime.strftime(xticks[iii], '%H:%M'),
                        Lticks[iii],
                        altticks[iii],
                        ) for iii in range(len(xticks))]
                
                ax.set_xticklabels(labels)
                ax.tick_params(axis='both', which='both', labelbottom=True)
                ax.text(-0.05, -0.07, 'UTC\nL shell\nAltitude (R$_\mathrm{{S}}$)',
                        ha='right', va='top', transform=ax.transAxes)

            plt.savefig('{}/low_pass_{}_wave_particle{}.png'.format(plotsavepath,
                        thisfile.split('/')[-1].split('\\')[-1].split('.')[0], '_present' if present else ''),
                        bbox_inches='tight', dpi=500)
            plt.savefig('{}/low_pass_{}_wave_particle_lowres{}.png'.format(plotsavepath,
                        thisfile.split('/')[-1].split('\\')[-1].split('.')[0], '_present' if present else ''),
                        bbox_inches='tight', dpi=200)
            plt.close()
            
#            sys.exit()
            
            # plot reviewer 1 plot
            ind = 5
            fluxes = np.copy(inio.images[ind,:,:,:])
            pamaps = np.tile(inio.pamaps[ind,:,:], [8,1,1])
            tmp = np.flip(np.sort(inio.calib['Midpoint_Value']), axis=0)
            ecent = np.swapaxes(np.tile(tmp, [16,16,1]), 0, 2)
            
            flux_flat = np.ravel(fluxes)
            pa_flat = np.ravel(pamaps)
            e_flat = np.ravel(ecent)
            
            ebins = np.sort(np.unique(np.append(inio.calib['Low'], inio.calib['Hi'])))
            pabins = np.arange(0, 60.1, 2.5)
            
            arg_e = np.digitize(e_flat, ebins)-1
            arg_pa = np.digitize(pa_flat, pabins)-1
            
            grid = np.full((len(pabins)-1, len(ebins)-1), np.nan)
            for ppp in range(len(pabins)-1):
                for eee in range(len(ebins)-1):
                    tmp = np.where((arg_e == eee) & (arg_pa == ppp))[0]
                    grid[ppp, eee] = np.nanmean(flux_flat[tmp])
                    
            plotgrid = np.log10(grid)
            
            fig = plt.figure()
            ax = plt.subplot()
            ax.set_facecolor('dimgrey')
            quad = ax.pcolormesh(pabins, ebins, grid.T, cmap = cmap_UV)
            quad.set_norm(mcolors.LogNorm(1e-2,1e0))
            ax.axvline(5, c='r', ls='--')
            ax.set_xlabel('Pitch angle (deg)')
            ax.set_ylabel('Energy (keV)')
            ax.set_title('Differential proton flux (2017-102 10:21)')
            plt.colorbar(quad, ax=ax)
            plt.savefig('{}/low_pass_{}_single_dist{}.png'.format(plotsavepath,
                        thisfile.split('/')[-1].split('\\')[-1].split('.')[0], '_present' if present else ''),
                        bbox_inches='tight', dpi=300)
            
            
            
#%%
if meanenergy:
    names = ['2017_009T11_30_12','2017_102T08_21_12']
    
    pan_iter = iter(panels)
    def setupAxes(ax, higherlabel=False):
        ax.tick_params(axis='both', which='both', direction='out',
                       left=True, labelleft=True,
                       right=False, labelright=False,
                       top=False, labeltop=False,
                       bottom=True, labelbottom=False)
        ax.grid('on', which='both')
        ax.grid(which='minor', linewidth=0.2, color='0.7')
        txt = ax.text(0.07 if present else 0.05, 0.98 if higherlabel else 0.95,
                         '({})'.format(next(pan_iter)),
                          transform=ax.transAxes, ha='center', va='top',
                          color='k', fontweight='bold', fontsize=15)
        txt.set_path_effects([patheffects.withStroke(linewidth=3, foreground='w')])
    
    fig = plt.figure()
    if present:
        frac = 6/7
        fig.set_size_inches(frac*14, frac*8)
    else:
        fig.set_size_inches(12,8)
    gs = gridspec.GridSpec(3,2, #height_ratios=(1,1,0.15,1,0.15,1)
                           hspace=0.1,
                           )
    
    minfrac = 1/5
    
    for iii in range(len(names)):
        thisfac = fac_container[names[iii]]
        title = '{}-{} {}'.format(names[iii][:4],
                 names[iii][5:8],
                 names[iii][9:].replace('_',':'))
        # plot energy flux
        ax_ef = plt.subplot(gs[0,iii])
        ax_ef.plot(thisfac['ET'], thisfac['Ef']*1e3, color='k')
        ax_ef.set_ylabel('$E_f$ (mW/m$^2$)')
        peaktime = thisfac['ET'].iloc[np.nanargmax(thisfac['j'])]
        ax_ef.set_xlim([peaktime-3600, peaktime+3600])
        ax_ef.set_title(title)
        
        # plot FAC
        ax_j = plt.subplot(gs[1,iii], sharex=ax_ef)
        ax_j.plot(thisfac['ET'], thisfac['j']*1e9, color='grey', ls='--', label='original')
        ax_j.set_ylabel('$j_\parallel$ (nA/m$^2$)')
        
        # get shift
        valid = np.where(np.isfinite(thisfac['Ef']) & np.isfinite(thisfac['j']))
        corr = sgn.correlate(thisfac['Ef'].iloc[valid], thisfac['j'].iloc[valid], mode='same', method='direct')
        dt = np.mean(np.diff(thisfac['ET']))
        dtfull = dt*(len(corr)-1)
        xsteps = np.linspace(-dtfull/2, dtfull/2, num=len(corr))/60
        maxabs = 30
        corr[(xsteps>maxabs+1) | (xsteps<-maxabs-1)] = 0
        tmp = np.argmax(corr)
        shift_seconds = xsteps[tmp]*60
        
        # plot shifted FAC
        print(shift_seconds)
        thisfac['j_shifted'] = np.roll(thisfac['j'], int(shift_seconds/dt))
        ax_j.plot(thisfac['ET'], thisfac['j_shifted']*1e9, color='k', label='shifted')
        ax_j.legend(loc=1, shadow=True, facecolor='w', edgecolor='k',
                    fancybox=True)
        ax_j.axhline(np.max(thisfac['j_shifted'])*1e9*minfrac,
                     linewidth=1, color='crimson')
        
        # plot <W>
        ax_sc = plt.subplot(gs[2,iii], sharex=ax_ef)
        thisfac['Wmean'] = thisfac['Ef']/thisfac['j_shifted']
        thisfac['Wmean'][thisfac['j_shifted']<np.max(thisfac['j_shifted'])*minfrac] = np.nan
        ax_sc.plot(thisfac['ET'], thisfac['Wmean']*1e-3, color='k')
        ax_sc.set_ylabel('$\\langle W \\rangle$ (keV)')
        ax_sc.set_xlabel('UTC')
        ax_sc.set_yscale('log')
        ax_sc.set_ylim([1,100])
        
        for ax in [ax_j, ax_ef, ax_sc]:
            tmp = ax.get_ylim()
            ax.fill_between(thisfac['ET'],
                            np.full_like(thisfac['ET'], tmp[0]),
                            np.full_like(thisfac['ET'], tmp[1]),
                            where = thisfac['j_shifted']>np.max(thisfac['j_shifted'])*minfrac,
                            color='crimson',
                            alpha=0.2)
            ax.set_ylim(tmp)
        
        setupAxes(ax_ef)
        setupAxes(ax_j)
        setupAxes(ax_sc)
        ax_sc.tick_params(bottom=True, labelbottom=True)
        
        dtstart = tc.et2datetime(peaktime-3600)
        d = datetime(dtstart.year, dtstart.month, dtstart.day, dtstart.hour)
        while d < dtstart:
            d = d + timedelta(minutes=15)
        xticks = np.array([d])
        while xticks[-1]+timedelta(minutes=15) < tc.et2datetime(peaktime+3600):
            xticks = np.append(xticks, [xticks[-1]+timedelta(minutes=15)])
        ax_sc.set_xticks(tc.datetime2et(xticks))
        labels = ['{}'.format(
                datetime.strftime(xticks[iii], '%H:%M'),
                ) for iii in range(len(xticks))]
        ax_sc.set_xticklabels(labels)
        
#            ax_corr = plt.subplot(gs[3,iii])
#            ax_corr.plot(xsteps, corr*1e10, color='k')
#            ax_corr.axvline(xsteps[tmp], linestyle='--', color='k')
#            ax_corr.set_xlabel('Offset (min)')
#            ax_corr.set_ylabel('Correlation (a.u.)')
#            ax_corr.set_xlim([-maxabs, maxabs])
#            setupAxes(ax_corr)
#            ax_corr.tick_params(bottom=True, labelbottom=True)
        

        
        
    plt.savefig('{}/mean_energy{}.png'.format(plotsavepath, '_present' if present else ''),
                bbox_inches='tight', dpi=500)
    plt.savefig('{}/mean_energy{}.pdf'.format(plotsavepath, '_present' if present else ''),
                bbox_inches='tight')
    plt.close()
        
#%%  
tstamp = '2017-009 11:37'
fmt = '%Y-%j %H:%M'
thisdt = datetime.strptime(tstamp, fmt)
thiset = tc.datetime2et(thisdt)

tmp = (cd.crpws.NEPROXY['ET']-thiset).abs().argmin()
rho = cd.crpws.NEPROXY['ELECTRON_NUMBER_DENSITY'].loc[tmp]*1e6*1.67*1e-27 # kg/m-3
mu0 = 4*np.pi*1e-7 # SI

tmp = np.argmin(np.abs(magdata['ET'].as_matrix()-thiset))
B = np.sqrt(Brint[tmp]**2+Btint[tmp]**2+Bpint[tmp]**2)*1e-9 #nT
thisfp = fp[tmp]

va = B/np.sqrt(mu0*rho)

# E
dens_values = []

for nnn in [*rp.specdens]:
    if rp.receiver[nnn] not in ['MFD', 'LFR']:
        continue
    valid = np.where(rp.detector[nnn] == 0)[0]
    if len(valid):
        times = tc.datetime2et(rp.dt[nnn][valid])
        specdens = rp.specdens[nnn][valid,:]
        freq = rp.fcent[nnn]

        tmp_et = np.digitize(thiset, times)
        if tmp_et==0 or tmp_et==len(times):
            continue
        
        tmp_f = np.digitize(thisfp, freq)
        if tmp_f==0 or tmp_f==len(freq):
            continue
        
        dens_values = np.append(dens_values, specdens[tmp_et-1, tmp_f-1])
E_spec = np.mean(dens_values)

# B
dens_values = []

for nnn in [*rp.specdens]:
    if rp.receiver[nnn] not in ['MFD', 'LFR']:
        continue
    valid = np.where(rp.detector[nnn] == 4)[0]
    if len(valid):
        times = tc.datetime2et(rp.dt[nnn][valid])
        specdens = rp.specdens[nnn][valid,:]
        freq = rp.fcent[nnn]

        tmp_et = np.digitize(thiset, times)
        if tmp_et==0 or tmp_et==len(times):
            continue
        
        tmp_f = np.digitize(thisfp, freq)
        if tmp_f==0 or tmp_f==len(freq):
            continue
        
        dens_values = np.append(dens_values, specdens[tmp_et-1, tmp_f-1])
B_spec = np.mean(dens_values)*1e-18
        
b = E_spec/va

new_va = np.sqrt(E_spec/B_spec)
amp_B = np.sqrt(B_spec*thisfp)*1e9 #nT
amp_E = np.sqrt(E_spec*thisfp) #V/m

q_p = 1.6e-19
m_p = 1.67e-27
v_p = np.sqrt(2*50e3*q_p/m_p)
dW_dt = E_spec*q_p**2/m_p/q_p #eV/s



