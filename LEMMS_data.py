import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime, timedelta
import glob
import numpy as np
import pandas as pd
import spiceypy as spice
import time_conversions as tc


class LEMMSdata(object):

    def __init__(self, magobj):
        self.magobj = magobj
        self.lemmspath = '{}/MIMI_LEMMS/CO-S-MIMI-4-LEMMS-CALIB-V1.0/DATA'.format(datapath)
        self.calibpath = '{}/MIMI_LEMMS/CO-S-MIMI-4-LEMMS-CALIB-V1.0/CALIBRATION/MIMI_CALIBRATION_0011.csv'.format(datapath)

        # initialize SPICE
        self.initSpice()
        
    # set up SPICE
    def initSpice(self):
        spice.kclear()
        spice.furnsh('{}/SPICE/metakernels/cassini_generic.mk'.format(datapath))
        print('Number of loaded kernels:', spice.ktotal('ALL'))

    def setStart(self, yyyy, doy, hh, mm, dt=1):
        self.YEAR = yyyy
        self.DOY = doy
        self.HOUR = hh
        self.MIN = mm
        self.thisdt = datetime.strptime('{}-{}-{}'.format(self.YEAR, self.DOY, self.HOUR, self.MIN), '%Y-%j-%H')
        self.thiset = tc.datetime2et(self.thisdt)
        self.deltat = dt #hours after start
        return    
    
    def loadAccData(self):
        print('Loading LEMMS ACC data...')
        daystrings = [datetime.strftime(self.thisdt+timedelta(hours=iii), '%Y%j') for iii in [-24,0,24]]
        self.fulldata = pd.DataFrame()
        for ddd in daystrings:
            try:
                thisfile = glob.glob('{}/LACCAVG0_1MIN/*/*{}*.csv'.format(self.lemmspath, ddd))[0]
            except:
                continue
            tmp = pd.read_csv(thisfile)
            self.fulldata = self.fulldata.append(tmp)
        if not len(self.fulldata):
            raise Exception('No data files')
        
        self.accdata = self.fulldata.copy(deep=True)
        self.accdata = self.accdata[(self.accdata['Start_Ephemeris_s'] > self.thiset) &
                                    (self.accdata['Start_Ephemeris_s'] < self.thiset+3600*self.deltat)].reset_index()
        
        self.acc_desc = {}
        self.acc_desc['Channel_name'] = np.array(['A', 'C', 'E', 'P'])
        self.acc_desc['Particle'] = np.array(['p', 'e', 'e', 'p'])
        self.acc_desc['Telescope'] = np.array(['LET', 'LET', 'HET', 'HET'])
        self.acc_desc['Channel_min'] = np.array([0, 0, 0, 1])
        self.acc_desc['Channel_max'] = np.array([7, 7, 7, 9])

    def readCalib(self):
        print('Loading LEMMS ACC calibration file...')
        self.fullcalib = pd.read_csv(self.calibpath, usecols=np.arange(16))
        # select data we want
        cond = {}
        cond['Purpose'] = 'ENERGY'
        cond['Data_Type'] = 'ACC'
        cond['Sensor'] = 'LEMMS'
        cond['Source'] = 'TEAM'
        cond['End_Year'] = 2030

        self.acccalib = self.fullcalib.copy(deep=True)
        for ccc in [*cond]:
            self.acccalib = self.acccalib[self.acccalib[ccc] == cond[ccc]]
            
        multicond = {}
        multicond['Channel'] = ['A','C','E','P']
        multicond['Particle'] = ['e', 'P']
        for ccc in [*multicond]:
            self.acccalib = self.acccalib[self.acccalib[ccc].isin(multicond[ccc])]
            
    def getMAG(self):
        self.fullmag = self.magobj.fullmag
        if len(self.accdata['Start_Ephemeris_s']) > 1:
            self.mag_KRTP = np.full((len(self.accdata['Start_Ephemeris_s']), 3), np.nan)
            for ttt in range(len(self.accdata['Start_Ephemeris_s'])):
                thisdt = tc.et2datetime(self.accdata['Start_Ephemeris_s'].iloc[ttt])
                delta = timedelta(minutes=10)
                selec = self.fullmag[(self.fullmag['Datetime']>thisdt-delta) & 
                                     (self.fullmag['Datetime']<thisdt+delta)]
                avg = selec.median(axis=0)
                self.mag_KRTP[ttt,0] = avg['BX_KRTP(nT)']
                self.mag_KRTP[ttt,1] = avg['BY_KRTP(nT)']
                self.mag_KRTP[ttt,2] = avg['BZ_KRTP(nT)']
            
    def getPA(self):
        # boresights
        self.LEMMS_boresight_LET_SC = np.array([-np.sin(np.radians(77)),0,-np.cos(np.radians(77))])
        self.LEMMS_boresight_HET_SC = np.array([np.sin(np.radians(77)),0,np.cos(np.radians(77))])
        # convert to KRTP
        self.LEMMS_PA_LET = np.full((len(self.accdata['Start_Ephemeris_s'])), np.nan)
        self.LEMMS_PA_HET = np.full((len(self.accdata['Start_Ephemeris_s'])), np.nan)
        for ttt in range(len(self.accdata['Start_Ephemeris_s'])):
            try:
                tmp = spice.pxform('CASSINI_SC_COORD', 'CASSINI_KRTP', self.accdata['Start_Ephemeris_s'].iloc[ttt])
                dir_LET = np.matmul(tmp, self.LEMMS_boresight_LET_SC)
                dir_HET = np.matmul(tmp, self.LEMMS_boresight_HET_SC)
                mag = self.mag_KRTP[ttt,:]
                self.LEMMS_PA_LET[ttt] = 180-np.degrees(np.arccos(np.dot(dir_LET, mag)/np.linalg.norm(dir_LET)/np.linalg.norm(mag)))
                self.LEMMS_PA_HET[ttt] = 180-np.degrees(np.arccos(np.dot(dir_HET, mag)/np.linalg.norm(dir_HET)/np.linalg.norm(mag)))
            except:
                pass
        return
            
    def update(self):
        self.loadAccData()
        self.readCalib()
        self.getMAG()
        self.getPA()

if __name__=='__main__':
    ld = LEMMSdata()
    ld.setStart(2017, 9, 11, 30, dt=2)
    ld.loadAccData()
    ld.readCalib()
    