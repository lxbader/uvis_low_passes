import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
from datetime import datetime
import internal_field_model
import matplotlib.pyplot as plt
import numpy as np
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

#t = '2017-009T11_22'
#t = '2017-009T12_45'

#t = '2017-102T10_03'
t = '2017-102T10_24'
start_time = datetime.strptime(t, '%Y-%jT%H_%M')
etstart = tc.datetime2et(start_time)
pa_start = 30 #deg

_, rstart, thstart, ltstart = cd.get_locations([etstart], refframe='KRTP')
print(rstart)

empty = np.full((100), np.nan)
empty_3 = np.full((3,100), np.nan)

pa = np.copy(empty)
loc = np.copy(empty_3)
B = np.copy(empty_3)

loc[0,0] = rstart
loc[1,0] = np.radians(thstart)
loc[2,0] = ltstart/12*np.pi
pa[0] = np.radians(pa_start)

iii = 0
while loc[0,iii]  > 2.5:
    iii += 1
    if not iii % 100:
        print(iii)
    if iii == len(pa):
        print('Increasing array size')
        pa = np.append(pa, empty)
        loc = np.append(loc, empty_3, axis=1)
        B = np.append(B, empty_3, axis=1)
    # get B field of previous step
    Br, Bt, Bp = internal_field_model.Internal_Field(loc[0,iii-1],
                                                     np.pi/2-loc[1,iii-1],
                                                     loc[2,iii-1],
                                                     model='Cassini11')
    B[:,iii-1] = np.array([Br, Bt, Bp])
    # convert location to KSM
    r = loc[0,iii-1]
    t = loc[1,iii-1]
    p = loc[2,iii-1]
    x = r*np.cos(t)*np.sin(p)
    y = r*np.cos(t)*np.cos(p)
    z = r*np.sin(t)
    
    # RTP vectors in Cartesian
    vr = np.array([x,y,z])
    vr /= np.linalg.norm(vr)
    vp = np.cross(np.array([0,0,1]), vr)
    vp /= np.linalg.norm(vp)
    vt = -np.cross(vr, vp)
    vt /= np.linalg.norm(vt)
    
    # find magnetic field vector in Cartesian
    Bcart = Br*vr + Bt*vt + Bp*vp
    
    # get pitch angle
    if iii>1:
        B1 = np.linalg.norm(B[:,iii-1])
        B0 = np.linalg.norm(B[:,0])
        pa[iii-1] = np.arcsin(np.sin(pa[0])*B1**2/B0**2)
        
    # determine new location in KSM
    scale = r*1/1000 #Rs
    loc_new = np.array([x,y,z]) - scale*Bcart/np.linalg.norm(Bcart)
    loc[0,iii] = np.linalg.norm(loc_new)
    loc[1,iii] = np.arctan(loc_new[2]/np.linalg.norm(loc_new[:2]))
    loc[2,iii] = np.arctan2(loc_new[1], loc_new[0])
  
#%%
# do some mirror force calculations
tmp = np.argmax(pa)

Bstart = B[:,tmp]*1e-9
Bstop = B[:,tmp-1]*1e-9
dB = np.abs(np.linalg.norm(Bstop)-np.linalg.norm(Bstart))
Btot = np.linalg.norm(Bstart)

locstart = loc[:,tmp]
locstop = loc[:,tmp-1]

xstart = locstart[0]*np.cos(locstart[1])
zstart = locstart[0]*np.sin(locstart[1])

xstop = locstop[0]*np.cos(locstop[1])
zstop = locstop[0]*np.sin(locstop[1])

RS = 60268*1e3
dz = np.sqrt((xstop-xstart)**2 + (zstop-zstart)**2)*RS

eps_perp = 50e3 #eV proton energy
E = eps_perp/Btot*dB/dz
print('Max perpendicular energy of trapped protons: {:0.1f} eV'.format(eps_perp))
print('Required parallel electric field strength: {:0.4f} mV/m'.format(E*1e3))
rsdist = 0.5
V = E*rsdist*RS
print('Corresponding to a potential drop of {} V over {:0.1f} Rs'.format(int(V), rsdist))


