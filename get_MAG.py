import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime, timedelta
import glob
import numpy as np
import pandas as pd
import time_conversions as tc

class MAGdata(object):
            
    def getMAG(self, etstart, etstop):
        print('Getting magnetometer data...')
        def dateparse(timestr):
            fmt = '%d/%m/%Y %H:%M:%S.%f'
            return datetime.strptime(timestr, fmt)
        
        # find right MAG files
        files = glob.glob('{}/MAG/sarah_2017_krtp_1s/*.txt'.format(datapath))
        shortfiles = [fff.split('/')[-1].split('\\')[-1].split('.')[0] for fff in files]
        startstr = [fff.split('_')[0] for fff in shortfiles]
        stopstr = [fff.split('_')[1] for fff in shortfiles]
        fmt = '%y%jT%H%M%S'
        startdt = [datetime.strptime(sss, fmt) for sss in startstr]
        stopdt = [datetime.strptime(sss, fmt) for sss in stopstr]
        startet = np.array([tc.datetime2et(sss) for sss in startdt])
        stopet = np.array([tc.datetime2et(sss) for sss in stopdt])
        tmp1 = np.where((etstart>startet) &
                        (etstart<stopet))
        tmp2 = np.where((etstop>startet) &
                        (etstop<stopet))
        valfiles = np.unique(np.append(tmp1, tmp2))
        self.fullmag = pd.DataFrame()
        for vvv in valfiles:
            if '17001' in files[vvv]:
                header_lines = 107
            elif '17011' in files[vvv]:
                header_lines = 187
            elif '17101' in files[vvv]:
                header_lines = 145
            elif '17151' in files[vvv]:
                header_lines = 205
            else:
                header_lines = 0
            
            tmp = pd.read_csv(files[vvv], sep='\t', header=header_lines)
            tmp['Datetime'] = tmp['Timestamp(UTC)'].apply(dateparse)
            self.fullmag = self.fullmag.append(tmp)
            
    def getMAG_PDS(self, etstart, etstop):
        print('Getting magnetometer data...')
        def dateparse(timestr):
            fmt = '%Y-%m-%dT%H:%M:%S.%f'
            return datetime.strptime(timestr, fmt)
                
        # find right MAG files
        files = glob.glob('{}/MAG/CO-E_SW_J_S-MAG-4-SUMM-1SECAVG-V2.0/DATA/2017/*_KRTP_1S.TAB'.format(datapath))
        shortfiles = [fff.split('/')[-1].split('\\')[-1].split('.')[0] for fff in files]
        startstr = [fff.split('_')[0] for fff in shortfiles]
        stopstr = [fff.split('_')[1] for fff in shortfiles]
        fmt = '%y%j'
        startdt = [datetime.strptime(sss, fmt) for sss in startstr]
        stopdt = [datetime.strptime(sss, fmt) for sss in stopstr]
        startet = np.array([tc.datetime2et(sss) for sss in startdt])
        stopet = np.array([tc.datetime2et(sss) for sss in stopdt])
        tmp1 = np.where((etstart>startet) &
                        (etstart<stopet))
        tmp2 = np.where((etstop>startet) &
                        (etstop<stopet))
        valfiles = np.unique(np.append(tmp1, tmp2))
        self.fullmag = pd.DataFrame()
        for vvv in valfiles:
            tmp = pd.read_csv(files[vvv],
                              sep='[ \t]+',
                              header=0,
                              names=['Timestamp(UTC)', 'BX_KRTP(nT)',
                                     'BY_KRTP(nT)', 'BZ_KRTP(nT)',
                                     'BTOTAL(nT)', 'SAMPLES'],
                              )
            tmp['Datetime'] = tmp['Timestamp(UTC)'].apply(dateparse)
            self.fullmag = self.fullmag.append(tmp)
#        self.fullmag = self.fullmag.sort_values(by=['Datetime']) 
        