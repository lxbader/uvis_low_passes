import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
import get_image
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import os
import remove_solar_reflection
from scipy.ndimage import gaussian_filter

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
rsr = remove_solar_reflection.RemoveSolarReflection()

savepath = '{}/UVIS_low_passes'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)
   
#%%
samplepath_2 = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
samplefile_2 = '{}/{}.fits'.format(samplepath_2, '2017_023T08_53_01')

image_2, _, _ = get_image.getRedUVIS(samplefile_2)
rsr.calculate(samplefile_2, multbg=True)
image_2 = np.copy(rsr.redImage_aur)-2

#%%
    
fig = plt.figure()
fig.set_size_inches(10,10)

BGROT=145

lon_0=(90+BGROT) % 360
lat_0=60

# altitude of camera (in km).
h = 2000.
# resolution = None means don't process the boundary datasets.
m1 = Basemap(projection='nsper',satellite_height=h*1000,
        lon_0=lon_0,lat_0=lat_0,resolution=None)
# add an axes with a black background
ax = fig.add_axes([0.1,0.1,0.8,0.8])
# plot just upper right quadrant (corners determined from global map).
# keywords llcrnrx,llcrnry,urcrnrx,urcrnry used to define the lower
# left and upper right corners in map projection coordinates.
# llcrnrlat,llcrnrlon,urcrnrlon,urcrnrlat could be used to define
# lat/lon values of corners - but this won't work in cases such as this
# where one of the corners does not lie on the earth.
bmap = Basemap(projection='nsper',satellite_height=h*1000,
               lon_0=lon_0,lat_0=lat_0,resolution='l',
               llcrnrx=-m1.urcrnrx/3,llcrnry=m1.urcrnry/5,
               urcrnrx=m1.urcrnrx/3,urcrnry=m1.urcrnry/2)

# draw lat/lon grid lines every 30 degrees.
bmap.drawmeridians((np.arange(0,360,30)+BGROT)%360, color='0.4')
bmap.drawparallels(np.arange(-90,90,10), color='0.4')

# load image and define mesh
KR_MIN=0.5
KR_MAX=30
img=np.copy(image_2)
img = np.roll(img, int(BGROT/360*np.shape(img)[0]), axis=0)
lonbins = np.linspace(0, 360, num=np.shape(img)[0]+1)
latbins = np.linspace(90, 60, num=np.shape(img)[1]+1)
LON,LAT = np.meshgrid(lonbins, latbins)

# cut off limb to avoid artifacts
loncent = lonbins[:-1]+np.mean(np.diff(lonbins))/2
latcent = latbins[:-1]+np.mean(np.diff(latbins))/2
aa,bb = np.meshgrid(loncent, latcent)
# project coordinates
X, Y = bmap(aa, bb)
# which points are off the planet
inv = np.where((X>1e20) | (Y>1e20))
X[inv] = np.nan
# expand region a bit by filtering
X_filt = gaussian_filter(X, 11, mode='reflect')
inv = np.where(np.isnan(X_filt))
img[inv[1],inv[0]] = np.nan

# plot
norm = mcolors.LogNorm(KR_MIN,KR_MAX)
image_norm = norm(img)
cmap = plt.get_cmap('pink')
cmap = cmap_UV
data = cmap(image_norm)
image_norm[image_norm>1] = 1
image_norm[image_norm<0] = 0
data[:,:,-1] = img/np.nanmax(img)
data[:,:,-1] = data[:,:,-1]
tmp = np.where(data[:,:,-1]<0)
data[:,:,-1][tmp] = 0
tmp = np.where(data[:,:,-1]>1)
data[:,:,-1][tmp] = 1
#data[:,:,-1] /= 3    
data = np.swapaxes(data, 0, 1)
color = data.reshape((data.shape[0]*data.shape[1],data.shape[2]))
quad = bmap.pcolormesh(LON, LAT, data[:,:,-1],
                       color=color, cmap=cmap, latlon=True)
quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
quad.set_array(None)

plt.savefig('{}/aurora_globe.png'.format(savepath), bbox_inches='tight', dpi=300)
plt.show()
plt.close()

