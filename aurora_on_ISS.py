import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime
import glob
import imageio
import matplotlib.cm as mcm
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import os
import remove_solar_reflection
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)
rsr = remove_solar_reflection.RemoveSolarReflection()

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

imagepath = '{}/UVIS_ISS_superpos'.format(boxpath)

#%%
def angle_axis(in_vec, u, theta):
    rotmat = np.full((3,3), np.nan)
    rotmat[0,0] = np.cos(theta) + u[0]**2*(1-np.cos(theta))
    rotmat[0,1] = u[0]*u[1]*(1-np.cos(theta)) - u[2]*np.sin(theta)
    rotmat[0,2] = u[0]*u[2]*(1-np.cos(theta)) + u[1]*np.sin(theta)

    rotmat[1,0] = u[1]*u[0]*(1-np.cos(theta)) + u[2]*np.sin(theta)
    rotmat[1,1] = np.cos(theta) + u[1]**2*(1-np.cos(theta))
    rotmat[1,2] = u[1]*u[2]*(1-np.cos(theta)) - u[0]*np.sin(theta)

    rotmat[2,0] = u[2]*u[0]*(1-np.cos(theta)) - u[1]*np.sin(theta)
    rotmat[2,1] = u[2]*u[1]*(1-np.cos(theta)) + u[0]*np.sin(theta)
    rotmat[2,2] = np.cos(theta) + u[2]**2*(1-np.cos(theta))
    return np.matmul(rotmat,in_vec)

# calculate intersection of a line with Saturn
# startpoint and viewing direction in KSMAG
def saturn_intersect(start, view):
    c = np.array([0,0,0])
    r = 1.00
    o = start
    l = view
    
    # determinant
    tmp = np.dot(l, (o-c))**2 - (np.linalg.norm(o-c)**2-r**2)
    if tmp > 0:
        # select smaller of the two solutions
        d = -np.dot(l, (o-c)) - np.sqrt(tmp)
        return o + d*l
    elif tmp == 0:
        # only one solution
        d = -np.dot(l, (o-c))
        return o + d*l
    else:
        # no solution
        return np.array([np.nan, np.nan, np.nan])

#%%
# read image
saturn_img = imageio.imread('{}/49091972676_99defe3fe2_o.png'.format(imagepath))

MULT = 4 # higher resolution

# try reading pre-calculated planet grid
npzfile = '{}/MULT{}_planetgrid_49091972676_99defe3fe2_o'.format(imagepath, MULT)

try:
    print('Loading pre-calculated mapping')
    tmp = np.load('{}.npz'.format(npzfile))
    loncolat = tmp['loncolat']
    loncolat_mult = tmp['loncolat_mult']
    MULT = tmp['MULT']
    print('Success')
except:
    print('Loading failed, generating new mapping')
    # =============================================================================
    # get Cassini location
    # =============================================================================
    dt = datetime(2016,4,25,9,21)
    tmp = cd.get_locations([tc.datetime2et(dt)], 'KSM')
    cas_pos_ksm = np.array(tmp[1:])
    
    # =============================================================================
    # north pole
    # =============================================================================
    north_px = np.array([236, 1024])
    
    # =============================================================================
    # left corner of the rings
    # =============================================================================
    # select some vertical stripes including the ring edge and sum them up
    tmp = np.sum(saturn_img[:,235:245,0], axis=1)
    # determine brightness maximum location using center of mass like calculation
    xloc = np.sum(tmp*np.arange(len(tmp)))/np.sum(tmp)
    # use first intensity increase to mark y location
    yloc = np.where(np.sum(saturn_img[:,:,0], axis=0)>100)[0][0]
    rings_left_px = np.array([xloc, yloc])
    
    # =============================================================================
    # right corner of the rings
    # =============================================================================
    tmp = np.sum(saturn_img[:,1775:1785,0], axis=1)
    xloc = np.sum(tmp*np.arange(len(tmp)))/np.sum(tmp)
    yloc = np.where(np.sum(saturn_img[:,:,0], axis=0)>100)[0][-1]
    rings_right_px = np.array([xloc, yloc])
    
    # =============================================================================
    # in the middle of the two ring corners should be Saturn's center, roughly
    # =============================================================================
    sat_center_px = (rings_left_px+rings_right_px)/2
    
    # =============================================================================
    # opening angle corresponding to one pixel unit
    # =============================================================================
    # elevation of Cassini above Saturn's center
    cas_rcyl = np.sqrt(np.sum(cas_pos_ksm[:-1]**2))
    alpha = np.arctan2(cas_pos_ksm[-1], cas_rcyl)
    # elevation of Cassini above Saturn's pole
    beta = np.arctan2(cas_pos_ksm[-1]-1, cas_rcyl)
    # angle between apparent north pole and Saturn center
    gamma = alpha-beta
    # corresponding pixel distance
    dist_px = np.sqrt(np.sum((sat_center_px-north_px)**2))
    # opening angle per (vertical) pixel unit
    rad_per_px = gamma/dist_px
    
    # =============================================================================
    # tilt angle
    # =============================================================================
    tmp = rings_left_px-rings_right_px
    rad_tilt = np.pi/2+np.arctan2(tmp[1], tmp[0])
    
    # =============================================================================
    # angular distance of each pixel from pole
    # =============================================================================
    x_px = np.zeros(saturn_img.shape[:-1])
    y_px = np.zeros(saturn_img.shape[:-1])
    for iii in range(x_px.shape[0]):
        for jjj in range(x_px.shape[1]):
            x_px[iii,jjj]=iii
            y_px[iii,jjj]=jjj
    # experimentally determined values for pixel stretching
    # (a pixel seems to be thinner than high)
    # doesn't fit completely but well enough for the polar region
    rad_px = np.sqrt((x_px-north_px[0])**2*(1.00*rad_per_px)**2+
                     (y_px-north_px[1])**2*(0.85*rad_per_px)**2)
    
    # MULT
    x_px_mult = np.zeros((saturn_img.shape[0]*MULT, saturn_img.shape[1]*MULT))
    y_px_mult = np.zeros((saturn_img.shape[0]*MULT, saturn_img.shape[1]*MULT))
    for iii in range(x_px_mult.shape[0]):
        for jjj in range(x_px_mult.shape[1]):
            x_px_mult[iii,jjj]=iii/MULT
            y_px_mult[iii,jjj]=jjj/MULT
    rad_px_mult = np.sqrt((x_px_mult-north_px[0])**2*(1.00*rad_per_px)**2+
                          (y_px_mult-north_px[1])**2*(0.85*rad_per_px)**2)
    
    # =============================================================================
    # viewing direction of each pixel
    # =============================================================================
    # viewing direction of pole pixel
    view_ksm_pole = (np.array([0,0,1])-cas_pos_ksm)/np.linalg.norm(
                                        (np.array([0,0,1])-cas_pos_ksm))
    # base rotation axis
    # cross product between pole viewing direction and pole vector
    base_axis = np.cross(view_ksm_pole, np.array([0,0,1]))/np.linalg.norm(view_ksm_pole)
    # viewing direction of every pixel in KSM    
    view_ksm_px = np.full(saturn_img.shape, np.nan)
    # rotate pole viewing direction to each pixel
    for iii in range(view_ksm_px.shape[0]):
        for jjj in range(view_ksm_px.shape[1]):
            # angle the rotation axis to allow axis-angle 
            # rotation to this specific pixel
            axis_angle = np.arctan2(x_px[iii,jjj]-north_px[0],
                                    y_px[iii,jjj]-north_px[1]) + np.pi/2 + rad_tilt
            rot_axis = angle_axis(base_axis, view_ksm_pole, axis_angle)
            # rotate
            view_ksm_px[iii,jjj,:] = angle_axis(view_ksm_pole,
                       rot_axis, rad_px[iii,jjj])
    
    # MULT
    view_ksm_px_mult = np.full((saturn_img.shape[0]*MULT,
                                saturn_img.shape[1]*MULT,
                                saturn_img.shape[2]),
                                np.nan)
    for iii in range(view_ksm_px_mult.shape[0]):
        for jjj in range(view_ksm_px_mult.shape[1]):
            axis_angle = np.arctan2(x_px_mult[iii,jjj]-north_px[0],
                                    y_px_mult[iii,jjj]-north_px[1]) + np.pi/2 + rad_tilt
            rot_axis = angle_axis(base_axis, view_ksm_pole, axis_angle)
            view_ksm_px_mult[iii,jjj,:] = angle_axis(view_ksm_pole,
                            rot_axis, rad_px_mult[iii,jjj])
    
    # =============================================================================
    # intersection of each pixel with the planet, lon and lat
    # =============================================================================
    intersection = np.full(saturn_img.shape, np.nan)
    # rotate pole viewing direction to each pixel
    for iii in range(intersection.shape[0]):
        for jjj in range(intersection.shape[1]):
            intersection[iii,jjj,:] = saturn_intersect(cas_pos_ksm, view_ksm_px[iii,jjj,:])
            
    r = np.linalg.norm(intersection, axis=-1)
    loncolat = np.full(intersection.shape[:-1]+(2,), np.nan)
    loncolat[:,:,0] = np.degrees(np.arctan2(intersection[:,:,1], intersection[:,:,0])) + 180
    loncolat[:,:,1] = np.degrees(np.arccos(intersection[:,:,-1]/r))

    # MULT
    intersection_mult = np.full((saturn_img.shape[0]*MULT,
                                 saturn_img.shape[1]*MULT,
                                 saturn_img.shape[2]),
                                 np.nan)
    for iii in range(intersection_mult.shape[0]):
        for jjj in range(intersection_mult.shape[1]):
            intersection_mult[iii,jjj,:] = saturn_intersect(cas_pos_ksm, view_ksm_px_mult[iii,jjj,:])
            
    r = np.linalg.norm(intersection_mult, axis=-1)
    loncolat_mult = np.full(intersection_mult.shape[:-1]+(2,), np.nan)
    loncolat_mult[:,:,0] = np.degrees(np.arctan2(intersection_mult[:,:,1], intersection_mult[:,:,0])) + 180
    loncolat_mult[:,:,1] = np.degrees(np.arccos(intersection_mult[:,:,-1]/r))
    
    #plt.figure()
    #plt.pcolormesh(saturn_img[:,:,0], cmap='Greys')
    #plt.pcolormesh(np.isfinite(intersection[:,:,0]), alpha=0.05, cmap='inferno')    
    #plt.show()
    
    np.savez(npzfile, loncolat=loncolat, loncolat_mult=loncolat_mult, MULT=MULT)

#%%
# =============================================================================
# load auroral images
# =============================================================================
    
group = '2014_087_flashing'
groups = ['2008_flashing', '2014_087_flashing', '2014_145', '2014_156', '2016_2017_GF_selec', '2017_PPO']

for group in ['2016_2017_GF_selec']:

    if group == '2008_flashing':
        # some flashing from 2008
        samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_SELEC_FLASHES_RES10'
        samplefiles = glob.glob('{}/*.fits'.format(samplepath))
    elif group == '2014_087_flashing':
        # strong flashing from 2014
        samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0046'
        samplefiles = glob.glob('{}/2014_087*.fits'.format(samplepath))[:-2]
    elif group == '2014_145':
        samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0047'
        samplefiles = glob.glob('{}/2014_145*.fits'.format(samplepath))
    elif group == '2014_156':
        samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0047'
        samplefiles = glob.glob('{}/2014_156*.fits'.format(samplepath))
    elif group == '2016_2017_GF_selec':    
        # specific GF files
        samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_HI_RES_RES10'
        filenames = ['2016_232T15_43_33',
                     '2016_232T17_20_55',
                     '2016_316T11_20_16',
                     '2017_023T08_53_01',
                     '2017_167T19_04_16',
                     '2017_219T08_30_23',
                     '2017_219T11_37_22',
                     '2017_232T08_28_24',
                     ]
        samplefiles = ['{}/{}.fits'.format(samplepath, iii) for iii in filenames]
    elif group == '2017_PPO':
        # PPO from 2017-079
        samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0058'
        samplefiles = glob.glob('{}/2017_079*.fits'.format(samplepath))
        samplefiles = samplefiles + glob.glob('{}/2017_080*.fits'.format(samplepath))
        samplefiles = samplefiles[:-8]
    
    if not os.path.exists('{}/{}'.format(imagepath, group)):
        os.makedirs('{}/{}'.format(imagepath, group))
    
    for samplefile in samplefiles:
    
        rsr.calculate(samplefile, multbg=True)
        image = np.copy(rsr.redImage_aur)
        
        lonbins = np.linspace(0, 360, num=image.shape[0]+1)
        colatbins = np.linspace(0, 30, num=image.shape[1]+1)
        
        overlay = np.full(saturn_img.shape[:-1], np.nan)
        for iii in range(overlay.shape[0]):
            for jjj in range(overlay.shape[1]):
                if loncolat[iii,jjj,1] < 25:
                    arglon = np.digitize(loncolat[iii,jjj,0], lonbins)-1
                    argcolat = np.digitize(loncolat[iii,jjj,1], colatbins)-1
                    overlay[iii,jjj] = image[arglon, argcolat]
                    
        overlay_mult = np.full((saturn_img.shape[0]*MULT,
                                saturn_img.shape[1]*MULT),
                                np.nan)
        for iii in range(overlay_mult.shape[0]):
            for jjj in range(overlay_mult.shape[1]):
                if loncolat_mult[iii,jjj,1] < 25:
                    arglon = np.digitize(loncolat_mult[iii,jjj,0], lonbins)-1
                    argcolat = np.digitize(loncolat_mult[iii,jjj,1], colatbins)-1
                    overlay_mult[iii,jjj] = image[arglon, argcolat]
                    
        sys.exit()
        
        #%%
        # =============================================================================
        # get colormap ready
        # =============================================================================
        # get colormap
        style = 'classic'
        if style == 'classic':
            cmap_UV = cubehelix.cmap(reverse=False, start=0.0, rot=-0.1, gamma=4)#, sat=2)
#            cmap_UV = cubehelix.cmap(reverse=False, start=0.2, rot=-0.2, gamma=4, minLight=0.8, sat=2)
            alphatype = 'sigmoid'
            thisnorm = mcolors.LogNorm(1,40)
        elif style == 'hst':
#            cmap_UV = cubehelix.cmap(reverse=False, start=0.0, rot=-0.1, gamma=6, minLight=0.6, maxLight=0.98)
            cmap_UV = mcm.get_cmap('Blues_r')
            alphatype = 'sigmoid'
            thisnorm = mcolors.LogNorm(1,40)
        # get colormap colors
        tmp_cmap = cmap_UV(np.arange(cmap_UV.N))
        if alphatype == 'linear':
            # set alpha (linearly increasing)
            tmp_cmap[:,-1] = np.linspace(0, 1, cmap_UV.N)
        elif alphatype == 'sigmoid':
            # set alpha (sigmoid)
            center = 0.3 #0.3 ###0.5?
            scaling = 12 #12 ###6?
            xspace = np.linspace(-center*scaling, (1-center)*scaling, cmap_UV.N)
            sigmoid = np.exp(xspace)/(np.exp(xspace)+1)*1.04-0.02
            sigmoid[sigmoid<0] = 0
            sigmoid[sigmoid>1] = 1
            tmp_cmap[:,-1] = sigmoid
        # make new colormap
        cmap_UV_alpha = mcolors.ListedColormap(tmp_cmap)
        
        imagedata = overlay_mult
        
        # put noise on image
#        noise = np.random.normal(-0.12, 0.12, size=overlay_mult.shape)
#        overlay_mult_noise = 10**(np.log10(overlay_mult) + noise)
#        overlay_mult_noise[np.logical_not(overlay_mult)] = np.nan
        
#        import scipy.ndimage as snd
#        filt = snd.gaussian_filter(imagedata, 20, mode='reflect')
#        noise = np.random.normal(1, 1, size=imagedata.shape)
#        imagedata_noise = imagedata+(filt-imagedata)**0.25*noise
#        imagedata_noise[np.logical_not(imagedata)] = np.nan
        
        # =============================================================================
        # make figure
        # =============================================================================
        fig = plt.figure(frameon=False)
        fig.set_size_inches(20.15, 10.17)
        ax = plt.subplot()
        fig.subplots_adjust(bottom = 0)
        fig.subplots_adjust(top = 1)
        fig.subplots_adjust(right = 1)
        fig.subplots_adjust(left = 0)
        # plot Saturn image
        ax.imshow(saturn_img)
        
        # plot UVIS data
        #ax.imshow(overlay_mult**0.5,
        #          extent=(-0.5, overlay_mult.shape[1]/MULT-0.5,
        #                  overlay_mult.shape[0]/MULT-0.5, -0.5),
        #          cmap=cmap_UV_alpha,
        #          vmin=0, vmax=40**0.5)
                      
        ax.imshow(imagedata,
                  extent=(-0.5, imagedata.shape[1]/MULT-0.5,
                          imagedata.shape[0]/MULT-0.5, -0.5),
                  cmap=cmap_UV_alpha,
                  norm = thisnorm,
                  )
        
        ax.axis('off')
        
        dpi=300
        
        fig.set_size_inches(16, 9.12)
        ax.set_xlim([424//2, 3624//2]) # width 3200//2 px
        ax.set_ylim([1954//2, 130//2]) # height 1808 px (16:9 adjusted to fit with macro_block_size=16)
        name = samplefile.split('/')[-1].split('\\')[-1].split('.')[0]
        plt.savefig('{}/{}/{}_{}dpi_test.png'.format(imagepath, group, name, dpi), dpi=dpi, pad_inches=0)
        
#        fig.set_size_inches(16, 9)
#        ax.set_xlim([652, 1363])
#        ax.set_ylim([450, 50])
#        
#        plt.savefig('{}/{}/zoom_{}_{}dpi.png'.format(imagepath, group, name, dpi), dpi=dpi, pad_inches=0)
        plt.close()
        sys.exit()
    
#%%
#for group in groups:
    # =============================================================================
    # make gif
    # =============================================================================
    group = '2017_PPO'
    filenames = glob.glob('{}/{}/2*.png'.format(imagepath, group))
    
    fps = 50
    if group == '2008_flashing':
        truefps = 5
        fps = 25
    elif group == '2014_087_flashing':
        truefps = 8
    elif group == '2014_145':
        truefps = 8
    elif group == '2014_156':
        truefps = 8
    elif group == '2016_2017_GF_selec': 
        truefps = None
    elif group == '2017_PPO':
        truefps = 5
        fps = 25
        
    if not truefps is None:
        images = []
        
        for filename in filenames:
            tmp = imageio.imread(filename)
            for iii in range(fps//truefps):
                images.append(tmp)
        imageio.mimsave('{}/{}/video_zoom_{}dpi.mp4'.format(imagepath, group, dpi),
                        images, fps=fps, #quality=6, 
                        bitrate=5e6)
        
    













