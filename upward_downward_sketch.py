import path_register

pr = path_register.PathRegister()
boxpath = pr.boxpath

import matplotlib as mpl
from matplotlib import patheffects
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
import numpy as np
import os

plotsavepath = r'{}\Projects\2019 AB low passes 2016 2017\Sketch'.format(boxpath)
if not os.path.exists(plotsavepath):
    os.makedirs(plotsavepath)
    

def makeplot(plot_orbits=True):    
    # planet and ionosphere
    planet = ptch.Circle((0,0), 1,
                         edgecolor='k', facecolor='none',
                         linewidth=3, zorder=7)
    ax.add_patch(planet)
    ax.text(0, 0.98, 'SATURN', color='k',
            ha='center', va='center', zorder=8)
    
    ionosph_outer = ptch.Circle((0,0), 1.06,
                                 edgecolor='none', facecolor='k', alpha=0.1,
                                 linewidth=3, zorder=5)
    ax.add_patch(ionosph_outer)    
    ionosph_inner = ptch.Circle((0,0), 1.02,
                                 edgecolor='none', facecolor='white',
                                 linewidth=3, zorder=6)
    ax.add_patch(ionosph_inner)    
    ax.text(1.04*np.cos(np.radians(82)),
            1.04*np.sin(np.radians(82)),
            'ionosphere', color='k', ha='center', va='center', rotation=-9)
    
    # currents
    rmin = 1.04
    rmax = 1.25
    tmin = np.radians(86)
    tmax = np.radians(94)
    style = {'color':'k', 'linewidth':2, 'zorder':7}
    ax.plot(np.cos(tmin)*np.array([rmax+0.01,rmin]),
            np.sin(tmin)*np.array([rmax+0.01,rmin]),
            **style)
    ax.plot(np.cos(tmax)*np.array([rmin,rmax+0.01]),
            np.sin(tmax)*np.array([rmin,rmax+0.01]),
            **style)
    trange = np.linspace(tmin,tmax,100)
    ax.plot(rmin*np.cos(trange),
            rmin*np.sin(trange),
            **style)
    
    dist = rmin+0.18*(rmax-rmin)
    style='Simple,head_length=20, head_width=12, tail_width=5'
    a1 = ptch.FancyArrowPatch(((dist+0.01)*np.cos(tmin), (dist+0.01)*np.sin(tmin)),
                              ((dist-0.01)*np.cos(tmin), (dist-0.01)*np.sin(tmin)),
                              arrowstyle=style,
                              color='k', zorder=8)
    ax.add_patch(a1)
    a2 = ptch.FancyArrowPatch(((dist-0.01)*np.cos(tmax), (dist-0.01)*np.sin(tmax)),
                              ((dist+0.01)*np.cos(tmax), (dist+0.01)*np.sin(tmax)),
                              arrowstyle=style,
                              color='k', zorder=8)
    ax.add_patch(a2)
    tmp = int(len(trange)*0.52)
    r = rmin
    a3 = ptch.FancyArrowPatch((r*np.cos(trange[tmp]), r*np.sin(trange[tmp])),
                              (r*np.cos(trange[tmp+1]), r*np.sin(trange[tmp+1])),
                              arrowstyle=style,
                              color='k', zorder=8)
    ax.add_patch(a3)
    
    # current direction labels
    offs = 0.03
    style = {'color':'k', 'ha':'center', 'va':'center', 'zorder':7}
    ax.text((rmax+offs)*np.cos(tmax),
            (rmax+offs)*np.sin(tmax),
            'upward current',
            **style)
    ax.text((rmax+offs)*np.cos(tmin),
            (rmax+offs)*np.sin(tmin),
            'downward current',
            **style)
    
    # latitude labels
    toffs = np.radians(5.8)
    tmpr = 1.081 if plot_orbits else 0.98
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax.text(tmpr*np.cos(tmin-toffs),
            tmpr*np.sin(tmin-toffs),
            'poleward', color='k', ha='right', va='center',
            bbox=props, zorder=10,
            fontsize=mpl.rcParams['font.size']-(0 if plot_orbits else 1.5))
    ax.text(tmpr*np.cos(tmax+toffs),
            tmpr*np.sin(tmax+toffs),
            'equatorward', color='k', ha='left', va='center',
            bbox=props, zorder=10,
            fontsize=mpl.rcParams['font.size']-(0 if plot_orbits else 1.5))
    
    
    # electric field structures
    cent = rmin+0.85*(rmax-rmin)
    
    for ttt in [tmin, tmax]:
        style = {'edgecolor':'k', 'facecolor':'none', 'linewidth':2, 'zorder':5}
        for mult in [1,2,3]:
            e = ptch.Ellipse((cent*np.cos(ttt), cent*np.sin(ttt)),
                              width=0.12*np.sqrt(mult), height=0.03*mult,
                              angle=np.degrees(ttt), **style)
            ax.add_patch(e)
        style = {'edgecolor':'none', 'facecolor':'w', 'zorder':6}
        e = ptch.Rectangle((cent*np.cos(ttt+np.radians(2.7)),
                            cent*np.sin(ttt+np.radians(2.7))),
                            width=0.12, height=0.12,
                            angle=np.degrees(ttt)-90,
                              **style)
        ax.add_patch(e)
        
        # add arrows
        startdist = 0.09 if ttt>np.pi/2 else 0.03
        stopdist = 0.03 if ttt>np.pi/2 else 0.09
        for angle in [ttt-np.radians(90), ttt+np.radians(90)]:
            midpoint = np.array([(cent+0.01)*np.cos(ttt),
                                 (cent+0.01)*np.sin(ttt)])
            vec = np.array([np.cos(angle), np.sin(angle)])
            start = midpoint+startdist*vec/2
            stop = midpoint+stopdist*vec/2
            ax.annotate('', stop, start,
                        arrowprops=dict(color='k', width=1,
                                        headlength=8, headwidth=6),
                                        zorder=7)
            dtttabs = 1.5 if ttt>np.pi/2 else 1.2
            dttt = np.radians(-dtttabs if angle<ttt else dtttabs)
            ax.text((cent+0.02)*np.cos(ttt+dttt),
                    (cent+0.02)*np.sin(ttt+dttt),
                    'E', color='k',
                    ha='center', va='center',
                    zorder=7)
            
        # add waves
        for tmpmult in [0.4, 0.35]:
            wavecent = rmin+tmpmult*(rmax-rmin)
            trange1 = np.linspace(ttt-np.radians(1.4), ttt-np.radians(0.6), 200)
            trange2 = np.linspace(ttt+np.radians(0.6), ttt+np.radians(1.4), 200)
            if ttt<np.pi/2:
                tranges = [trange1, trange2]
            else:
                tranges = [np.linspace(ttt-np.radians(0.8), ttt+np.radians(0.8), 200)]
            for trange in tranges:
                r = wavecent + 0.003*np.sin(trange*1000)
                ax.plot(r*np.cos(trange), r*np.sin(trange),
                        color='orange',zorder=8)
            
        # add wave labels
        r_ann = rmin+0.38*(rmax-rmin)
        t_ann = ttt-np.radians(1.6) if ttt<np.pi/2 else ttt+np.radians(1)
        r_text = rmin+0.37*(rmax-rmin)
        t_text = ttt-np.radians(3) if ttt<np.pi/2 else ttt+np.radians(2.5)
        props = dict(color='orange', width=1, headlength=8, headwidth=6,
                     connectionstyle='arc3,rad=.15' if ttt<np.pi/2 else'arc3,rad=-.15')
        ax.annotate('ELF' if ttt<np.pi/2 else 'EIC\nEMIC',
                    (r_ann*np.cos(t_ann), r_ann*np.sin(t_ann)),
                    (r_text*np.cos(t_text), r_text*np.sin(t_text)),
                    color='orange', va='top',
                    ha='left' if ttt<np.pi/2 else 'right',
                    arrowprops=props,
                    fontsize=10,
                    zorder=8)
                    
        # add electron beams
        rstart = rmin+(rmax-rmin)*(0.15 if ttt<np.pi/2 else 0.25)
        rdist = (rmax-rmin)*(0.1 if ttt<np.pi/2 else -0.1)
        for diff in [-0.7,+0.7]:
            raddiff = np.radians(diff)
            start = rstart*np.array([np.cos(ttt+raddiff), np.sin(ttt+raddiff)])
            stop = (rstart+rdist)*np.array([np.cos(ttt+raddiff), np.sin(ttt+raddiff)])
            ax.annotate('', stop, start,
                        arrowprops=dict(color='royalblue', width=1.1,
                                        headlength=10, headwidth=7.5),
                                        zorder=8)
        x = (rstart+rdist/2)*np.cos(ttt+np.radians(-1.5 if ttt>np.pi/2 else 1.5))
        y = (rstart+rdist/2)*np.sin(ttt+np.radians(-1.5 if ttt>np.pi/2 else 1.5))
        ax.text(x, y, 'e$^-$',
                ha='center', va='center',
                color='royalblue',
                zorder=8)
        
        
        # add ion beams
        rstart = rmin+(rmax-rmin)*(0.97)
        rdist = (rmax-rmin)*(0.1)
        for diff in [-0.4,+0.4]:
            raddiff = np.radians(diff)
            start = rstart*np.array([np.cos(ttt+raddiff), np.sin(ttt+raddiff)])
            stop = (rstart+rdist)*np.array([np.cos(ttt+raddiff), np.sin(ttt+raddiff)])
            ax.annotate('', stop, start,
                        arrowprops=dict(color='crimson', width=1.1,
                                        headlength=10, headwidth=7.5),
                                        zorder=8)
        x = (rstart+rdist/2)*np.cos(ttt+np.radians(-1.1 if ttt>np.pi/2 else 1.1))
        y = (rstart+rdist/2)*np.sin(ttt+np.radians(-1.1 if ttt>np.pi/2 else 1.1))
        ax.text(x, y, 'i$^+$',
                ha='center', va='center',
                color='crimson',
                zorder=8)
        
        if ttt<np.pi/2:
            # add helix
            
#            t = np.linspace(0,100, 2000)
#            r = (100-t)**4/100**4
#            theta = np.radians(20)
#            x = r*np.cos(t)*np.cos(theta) + 0.6*t*np.sin(theta)
#            y = r*np.sin(t)
            
            t = np.linspace(0,100, 2000)
            r = t**2/100**2
            theta = np.radians(20)
            x = r*np.cos(t)*np.cos(theta) + 0.6*t**10*np.sin(theta)/6e17
            y = r*np.sin(t)
            
            # scale to reasonable values
            #x = radial component
            rad_min = rmin+0.32*(rmax-rmin)
            rad_max = rmin+0.95*(rmax-rmin)
            x_scaled = rad_min+x/np.max(x)*(rad_max-rad_min)
            r_scaled = y*0.5
            
            plot_x = x_scaled*np.cos(ttt+np.radians(r_scaled))
            plot_y = x_scaled*np.sin(ttt+np.radians(r_scaled))
            
            style = {'color':'crimson', 'linewidth':2}
            upper = np.where(np.gradient(np.gradient(x_scaled))>0)
            lower = np.where(np.gradient(np.gradient(x_scaled))<0)
            tmpx = np.copy(plot_x)
            tmpx[upper] = np.nan
            ax.plot(tmpx, plot_y, **style, zorder=6)
            tmpx = np.copy(plot_x)
            tmpx[lower] = np.nan
            ax.plot(tmpx, plot_y, **style, zorder=7)
            
            # add conic labels
            lbl = ['trapped ions', 'ion conic', 'ion beam']
            ann = [0.58,0.73,0.83]
            ann = [0.32,0.63,1]
            txt = [0.28,0.62,1]
            tann = [0.5, 0.6, 0.8]
            for lll, aaa, txx, taa in zip(lbl, ann, txt, tann):
                r_ann = rmin+aaa*(rmax-rmin)
                t_ann = ttt-np.radians(taa)
                r_text = rmin+txx*(rmax-rmin)
                t_text = ttt-np.radians(2.5)
                props = dict(color='crimson', width=1, headlength=8, headwidth=6,
                             connectionstyle='arc3,rad=-.25' if aaa<0.6 else'arc3,rad=.1')
                ax.annotate(lll,
                            (r_ann*np.cos(t_ann), r_ann*np.sin(t_ann)),
                            (r_text*np.cos(t_text), r_text*np.sin(t_text)),
                            color='crimson', va='top',
                            ha='left' if ttt<np.pi/2 else 'right',
                            arrowprops=props,
                            fontsize=10,
                            zorder=8)
            
            
    
    
    # aurora
    aurora = ptch.Wedge((0,0), 1.05, np.degrees(tmax)-1.2, np.degrees(tmax)+1.2,
                        width=0.02,
                        facecolor='royalblue',
                        edgecolor='royalblue',
                        alpha=0.8,
                        zorder=4,
                        )
    ax.add_patch(aurora)
    ax.text(1.04*np.cos(tmax+np.radians(2.6 if plot_orbits else 3.5)),
            1.04*np.sin(tmax+np.radians(2.6 if plot_orbits else 3.5)),
            'aurora',
            color='royalblue', ha='center', va='center', rotation=8.6)
    
    if plot_orbits:
        # trajectories
        lbl = ['2017-009', '2017-102']
        rstart = [1.14,1.238]
        rstop = [1.16,1.20]
        tstart = np.radians([99.3, 80.5])
        tstop = np.radians([80.5, 98.3])
        for lll, r1, r2, t1, t2 in zip(lbl, rstart, rstop, tstart, tstop):
            props = dict(color='grey', width=1, headlength=8, headwidth=6,
                         connectionstyle='arc3,rad=.03' if lll=='2017-009' else'arc3,rad=-.05')
            ax.annotate(lll,
                        (r2*np.cos(t2), r2*np.sin(t2)),
                        (r1*np.cos(t1), r1*np.sin(t1)),
                        color='grey', va='top',
                        ha='left' if lll == '2017-009' else 'right',
                        arrowprops=props,
                        fontsize=10,
                        zorder=7)

for plot_orbits in [False, True]:
    for xkcd in [False, True]:
        
        fig = plt.figure()
        if plot_orbits:
            mult = 1
        else:
            mult = 0.75
        fig.set_size_inches(mult*8, mult*6.5)
        ax = plt.subplot()
        
        def_par = dict(mpl.rcParams)
        par = [*def_par]
        par = [iii for iii in par if 'font' in iii]
        def_font = {}
        for iii in range(len(par)):
            def_font[par[iii]] = def_par[par[iii]]
          
        if xkcd:
            with plt.xkcd(scale=2, length=300, randomness=3):
                xkcdstyle = dict(mpl.rcParams)
                plt.rcParams['path.effects'] = [patheffects.withStroke(linewidth=0, foreground='w')]           
                mpl.rcParams.update(def_font)
                plt.rcParams['font.size'] = 12.0
                plt.rcParams['font.weight'] = 'bold'
                makeplot(plot_orbits=plot_orbits)
        else:  
            plt.rcParams['font.size'] = 12.0
            plt.rcParams['font.weight'] = 'bold'
            makeplot(plot_orbits=plot_orbits)        
            
        ax.axis('equal')
        ax.set_xlim([-0.2, 0.2])
        ax.set_ylim([0.96, 1.28])
        ax.axis('off')
        ax.tick_params(axis='both', which='both', direction='in',
                       left=False, labelleft=False,
                       right=False, labelright=False,
                       top=False, labeltop=False,
                       bottom=False, labelbottom=False)
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        ax.spines['bottom'].set_color('none')
        ax.spines['left'].set_color('none')
            
        plt.savefig('{}/current_sketch_{}{}.png'.format(plotsavepath, 'xkcd' if xkcd else 'clean',
                    '_noorb' if not plot_orbits else ''),
                    bbox_inches='tight', dpi=500)
plt.close()
