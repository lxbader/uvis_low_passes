import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)
sys.path.insert(0, '%s/uvis_stitch' % gitpath)

import cubehelix
from datetime import datetime
import glob
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
from matplotlib.ticker import NullLocator
import numpy as np
import os
import plot_HST_UVIS
import skimage.filters as skf
import skimage.restoration as skr
import stitch_calibrate_project
import time_conversions as tc
import uvisdb

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

projectdata = False

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
cp = plot_HST_UVIS.CassPlotter()

resolution = 8

mainsavepath = '{}/UVIS/PDS_UVIS/PROJECTIONS_LOW_PASSES_RES{}'.format(datapath, int(resolution))
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)
    
plotsavepath = '{}/UVIS_low_passes/all'.format(plotpath)
if not os.path.exists(plotsavepath):
    os.makedirs(plotsavepath)
    
us = stitch_calibrate_project.UVISstitcher()

#%%
info = np.genfromtxt('{}/uvis_low_passes/low_passes_proc.txt'.format(gitpath), delimiter=',', dtype='U')

if projectdata:
    us.setResolution(resolution)
    us.setSavePath(mainsavepath)
    us.saveSpecs(False)
    
    currentset = []
    for fctr in range(19, 21):
        if info[fctr,1] == 'SKIP':
            continue
        file = '{}/UVIS/PDS_UVIS/{}'.format(datapath, info[fctr,0])
        LASP = True if 'COUVIS_0060' in file else False
                
        if 'COMBINE_START' in info[fctr,1]:
            currentset = [file]
            continue
        elif 'CONT' in info[fctr,1]:
            currentset.append(file)
            continue
        elif 'COMBINE_STOP' in info[fctr,1]:
            currentset.append(file)
#            try:
            print(currentset)
            if 'CLEANTRUE' in info[fctr,1]:
                if 'SENS' in info[fctr,1]:
                    us.stitchFiles(currentset, LASP=LASP, clean=True, sens=int(info[fctr,1][-1]))
                else:
                    us.stitchFiles(currentset, LASP=LASP, clean=True)
            else:
                us.stitchFiles(currentset, LASP=LASP, clean=False)
#            except Exception as e:
#                print(e)
#                pass
        elif 'SPLIT_AUTO' in info[fctr,1]:
#            try:
            if 'SENS' in info[fctr,1]:
                us.splitFile(file, LASP=LASP, sens=int(info[fctr,1][-1]))
            else:
                us.splitFile(file, LASP=LASP)
#            except Exception as e:
#                print(e)
#                pass
        elif 'SINGLE' in info[fctr,1]:
#            try:
            if 'NOCLEAN' in info[fctr,1]:
                us.stitchFiles([file], LASP=LASP, clean=False)
            else:
                if 'SENS' in info[fctr,1]:
                    us.stitchFiles([file], LASP=LASP, sens=int(info[fctr,1][-1]))
                else:
                    us.stitchFiles([file], LASP=LASP)
#            except Exception as e:
#                print(e)
#                pass
        else:
            print('Unrecognized flag: %s' % info[fctr,1])

#%%
# =============================================================================
# plot
# =============================================================================
            
#filelist = glob.glob('{}/*.fits'.format(mainsavepath))
#
#for fctr in range(len(filelist)):
#    fname = filelist[fctr].split('/')[-1].split('\\')[-1].split('.')[0]
#    fmt = '%Y_%jT%H_%M_%S'
#    dtstart = datetime.strptime(fname, fmt)
#    etstart = tc.datetime2et(dtstart)
#    tmp = (imglist['ET_START']-etstart).abs().argmin()
#    cp.makeplot(imglist.loc[tmp,:], '{}/{}'.format(plotsavepath, fname), 
#                extrafilepath=filelist[fctr],
#                KR_MIN=0.5, KR_MAX=30, dpi=1000)
#    
#sys.exit()
#%%
# =============================================================================
# prepare chapter heading for thesis
# =============================================================================
            
def makeplot(data, savename):
    data[np.isnan(data)] = 0
    data[data<0.1] = 0.1
    data = np.repeat(data, 4, axis=0)
    data = np.repeat(data, 4, axis=1)
    sgm = np.array([1,1])
    data_filt = skf.gaussian(data, sigma=1*sgm, mode='wrap')
    #data_filt = skr.denoise_bilateral(data_filt, sigma_spatial=1, sigma_color=1, multichannel=False)
    
    fig = plt.figure()
    fig.set_size_inches(15,4)
    ax = plt.subplot()
    quad = ax.pcolormesh(data_filt.T, cmap=cmap_UV)
    KR_MIN=1
    KR_MAX=30
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_ylim(ax.get_ylim()[::-1])
    ax.axis('off')
    
    ax.xaxis.set_major_locator(NullLocator())
    ax.yaxis.set_major_locator(NullLocator())
    plt.savefig(savename, bbox_inches='tight', pad_inches=0, dpi=500)
    plt.close()
    return

def et2path(et):
    savepath = '{}/phd_thesis/img/00_pre'.format(gitpath)
    fmt = '%Y_%jT%H_%M'
    tstr = datetime.strftime(tc.et2datetime(et), fmt)
    return '{}/{}.png'.format(savepath, tstr)
    
chpinfo = np.genfromtxt('{}/uvis_low_passes/low_passes_proc_chapters.txt'.format(gitpath), delimiter=',', dtype='U')

for fctr in range(chpinfo.shape[0]):

    file = '{}/UVIS/PDS_UVIS{}'.format(datapath, chpinfo[fctr,0])
    us.getCalibIntegData(file, LASP=False if not 'COUVIS_0060' in file else True)
    
    if 'SPLIT' in chpinfo[fctr,1]:
        minlist, maxlist = us.getSplitLimits(sens=5)
        for sctr in range(len(minlist)):
            if 'FIRST' in chpinfo[fctr,1] and sctr:
                continue
            data = np.copy(us.int_data[int(minlist[sctr]):int(maxlist[sctr]), 1:-1])
            makeplot(data, et2path(us.ettimes[int(minlist[sctr])]))
    else:
        data = np.copy(us.int_data[:, 1:-1])
        makeplot(data, et2path(us.ettimes[0]))
        
    
    
    
    
    
