import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime, timedelta
import glob
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import spiceypy as spice
import time_conversions as tc

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

class INCAPA(object):
    
    def __init__(self, magobj):
        self.magobj = magobj
        self.incaimgpath = '{}/MIMI_INCA/CO-S-MIMI-4-INCA-CALIB-V1.0/DATA/ICIMG0_I'.format(datapath)
        self.calibpath = '{}/MIMI_INCA/CO-S-MIMI-4-INCA-CALIB-V1.0/CALIBRATION/MIMI_CALIBRATION_0011.csv'.format(datapath)
        
        # initialize SPICE
        self.initSpice()
        # get calibration energies
        self.getEnergies()
        
        # set plot limits (high to low energy bins)
        self.lim = np.array([
                [1e-2,1],
                [4e-2,5e-1],
                [1e-1,2],
                [1e-1,3],
                [1e-1,5],
                [1e-1,9],
                [1e0,5e1],
                [1e0,9e2],
                ])
        
    # set up SPICE
    def initSpice(self):
        spice.kclear()
        spice.furnsh('{}/SPICE/metakernels/cassini_generic.mk'.format(datapath))
        print('Number of loaded kernels:', spice.ktotal('ALL'))

    def setStart(self, yyyy, doy, hh, mm, dt=1, alt=1):
        self.YEAR = yyyy
        self.DOY = doy
        self.HOUR = hh
        self.MIN = mm
        self.thisdt = datetime.strptime('{}-{}-{}-{}'.format(self.YEAR, self.DOY, self.HOUR, self.MIN), '%Y-%j-%H-%M')
        self.thiset = tc.datetime2et(self.thisdt)
        self.deltat = dt #hours after start
        self.alt = alt
        return    
    
    # get energy calibration
    def getEnergies(self):
        self.fullcalib = pd.read_csv(self.calibpath, usecols=np.arange(15))
        # select data we want
        cond = {}
        cond['Purpose'] = 'ENERGY'
        cond['Data_Type'] = 'IMG'
        cond['Channel'] = 'ION - TOF'
        cond['Particle'] = 'H+'
        cond['End_Year'] = 2030
        
        self.calib = self.fullcalib.copy(deep=True)
        for ccc in [*cond]:
            self.calib = self.calib[self.calib[ccc] == cond[ccc]]
    
    # load datafiles
    def loadData(self, charged=1, species='H'):
        print('Loading raw INCA data...')
        daystrings = [datetime.strftime(self.thisdt+timedelta(hours=iii), '%Y%j') for iii in [-24,0,24]]
        self.fulldata = pd.DataFrame()
        for ddd in daystrings:
            try:
                thisfile = glob.glob('{}/*/*{}*.csv'.format(self.incaimgpath, ddd))[0]
            except:
                continue
            tmp = pd.read_csv(thisfile)
            self.fulldata = self.fulldata.append(tmp)
        if not len(self.fulldata):
            raise Exception('No data files')
        
        # select data we want
        cond = {}
        cond['Purpose'] = 'sci'
        cond['High_Resolution'] = 'mTOF'
        cond['Species'] = species
        cond['Neutral_Ion'] = charged      
        
        self.data = self.fulldata.copy(deep=True)
        for ccc in [*cond]:
            self.data = self.data[self.data[ccc] == cond[ccc]]
        self.data = self.data[(self.data['Start_Ephemeris_s'] > self.thiset) &
                              (self.data['End_Ephemeris_s'] < self.thiset+3600*self.deltat)]
    
    # load raw image data      
    def getImages(self):
        print('Assembling images...')
        self.pixels = self.data.loc[:,'Num_Rows'].iloc[0]
        self.images = np.zeros((len(self.data[(self.data['Row_ID'] == 0) & (self.data['TOF'] == 0)]), 8,
                                self.pixels, self.pixels))
        for tof in range(8):
            for rrr in range(self.pixels):
                for ccc in range(self.pixels):
                    tmp = self.data[(self.data['Row_ID'] == rrr) & (self.data['TOF'] == tof)]
                    self.images[:,tof,rrr,ccc] = tmp.loc[:,'Col_{}'.format(ccc)]
        cond = (self.data['Row_ID'] == 0) & (self.data['TOF'] == 0)
        self.etstart = self.data[cond].loc[:,'Start_Ephemeris_s'].as_matrix()
        self.etstop = self.data[cond].loc[:,'End_Ephemeris_s'].as_matrix()
        self.etcenter = self.data[cond].loc[:,['Start_Ephemeris_s','End_Ephemeris_s']].mean(axis=1).as_matrix()
        
    # viewing vectors in KSM frame
    def getINCAViewdirs(self):
        print('Calculating viewing vectors...')
        self.INCA_boresight = np.array([0,0,1], dtype=np.float)
        # according to user guide
        theta = np.radians(np.linspace(-60, 60, num=self.pixels+1))
        theta = theta[:-1]+np.diff(theta)/2
        phi = np.radians(np.linspace(-45, 45, num=self.pixels+1))
        phi = phi[:-1]+np.diff(phi)/2
        self.viewdirs_cent = np.zeros((self.pixels, self.pixels, 3))
        for ttt in range(self.pixels):
            for ppp in range(self.pixels):
                x_doc = np.cos(theta[ttt])*np.cos(phi[ppp])
                y_doc = np.cos(theta[ttt])*np.sin(phi[ppp])
                z_doc = np.sin(theta[ttt])
                
                # switch from documentation to SPICE defined orientation
                z = x_doc
                y = z_doc
                x = -y_doc # why does there need to be a minus here?
                
                self.viewdirs_cent[ttt,ppp,:] = [x,y,z]
        # convert to KRTP
        self.viewdirs_cent_KRTP = np.zeros((len(self.etcenter),)+self.viewdirs_cent.shape)
        for eee in range(len(self.etcenter)):
            for xxx in range(self.pixels):
                for yyy in range(self.pixels):
                        tmp = spice.pxform('CASSINI_MIMI_INCA', 'CASSINI_KRTP', self.etstart[eee])
                        KRTP = np.matmul(tmp, self.viewdirs_cent[xxx,yyy,:])
                        self.viewdirs_cent_KRTP[eee,xxx,yyy,:] = KRTP
        # convert to KSM
        self.viewdirs_cent_KSM = np.zeros((len(self.etcenter),)+self.viewdirs_cent.shape)
        for eee in range(len(self.etcenter)):
            for xxx in range(self.pixels):
                for yyy in range(self.pixels):
                        tmp = spice.pxform('CASSINI_MIMI_INCA', 'CASSINI_KSM', self.etstart[eee])
                        KSM = np.matmul(tmp, self.viewdirs_cent[xxx,yyy,:])
                        self.viewdirs_cent_KSM[eee,xxx,yyy,:] = KSM
                                    
    def getMAG(self):
        self.fullmag = self.magobj.fullmag
        if len(self.etstart) > 1:
            self.mag_KRTP = np.full((len(self.etcenter), 3), np.nan)
            for ttt in range(len(self.etcenter)):
                thisdt = tc.et2datetime(self.etstart[ttt])
                delta = timedelta(minutes=10)
                selec = self.fullmag[(self.fullmag['Datetime']>thisdt-delta) & 
                                     (self.fullmag['Datetime']<thisdt+delta)]
                avg = selec.median(axis=0)
                self.mag_KRTP[ttt,0] = avg['BX_KRTP(nT)']
                self.mag_KRTP[ttt,1] = avg['BY_KRTP(nT)']
                self.mag_KRTP[ttt,2] = avg['BZ_KRTP(nT)']
    
    def getPAMaps(self):
        print('Calculating pitch angle maps...')
        self.pamaps = np.full((self.images.shape[0],
                               self.images.shape[2],
                               self.images.shape[3]), np.nan)
        for iii in range(self.images.shape[0]):
            for xxx in range(self.images.shape[2]):
                for yyy in range(self.images.shape[3]):
                    view = self.viewdirs_cent_KRTP[iii,xxx,yyy,:]
                    mag = self.mag_KRTP[iii,:]
                    self.pamaps[iii,xxx,yyy] = 180-np.degrees(np.arccos(np.dot(view, mag)/np.linalg.norm(view)/np.linalg.norm(mag)))
                    
    def getPADist(self, binsize=3):
        print('Calculating pitch angle distributions...')
        self.padists = np.full((self.images.shape[0], self.images.shape[1], int(180/binsize)), np.nan)
        self.pabins = np.linspace(0, 180, num=int(180/binsize)+1)
        for iii in range(self.images.shape[0]): #images
            tmp = np.digitize(self.pamaps[iii,:,:], self.pabins)-1
            for ttt in range(self.images.shape[1]): #tof
                for ctr in range(len(self.pabins)-1):
                    val = np.where(tmp==ctr)
                    self.padists[iii, ttt, ctr] = np.nanmean(self.images[iii,ttt,:,:][val])
                    
    def getBeamSpec(self):
        self.beamspec = np.full((self.images.shape[:2]), np.nan)
        for iii in range(self.images.shape[0]):
            for ttt in range(self.images.shape[1]):
                tmp = np.where(self.pamaps[iii,:,:]>160)
                self.beamspec[iii,ttt] = np.nanmean(self.images[iii,ttt,:,:][tmp])
                
                    
    def update(self, charged=1, species='H'):
        self.loadData(charged=charged, species=species)
        if len(self.data):
            self.getImages()
            self.getMAG()
            self.getINCAViewdirs()
            self.getPAMaps()
            self.getPADist()
            self.getBeamSpec()
        else:
            self.etstart = [self.thiset-3600*5]
            self.etstop = [self.thiset+3600*5]
            self.getMAG()
        
    def plot(self, plotsavepath, species='H'):
        print('Plotting images...')
        fmt = '%Y_%j'
        tmp = datetime.strftime(tc.et2datetime(self.etstart[0]), fmt)
        plotsavepath = '{}/{}'.format(plotsavepath, tmp)
        if not os.path.exists(plotsavepath):
            os.makedirs(plotsavepath)
            
        for ttt in range(1,8):
            num = self.images.shape[0]
            cols = int(np.floor(np.sqrt(num)))
            rows = int(np.ceil(num/cols))
            
            gs = gridspec.GridSpec(rows, cols+1, width_ratios=(1,)*cols+(0.07,),
                                   wspace=0.05)
            fig = plt.figure()
            fig.set_size_inches(4*cols, 3*rows)
            
            data = np.copy(self.images)
            data[data==0] = np.nan
            cmax = self.lim[ttt,1]
            cmin = self.lim[ttt,0]
            
            for iii in range(len(self.images)):
                ax = plt.subplot(gs[iii//cols, iii%cols])
                ax.set_facecolor('dimgrey')
                cont = ax.contour(self.pamaps[iii,:,:].T, levels=np.arange(30,180,30, dtype=np.int), colors='gold')
                ax.clabel(cont, inline=1, fontsize=10, fmt='%d$^\circ$')
                quad = ax.pcolormesh(self.images[iii,ttt,:,:].T, cmap=cmap_UV)
                quad.set_norm(mcolors.LogNorm(cmin, cmax))
                fmt = '%H:%M'
                tmp = '{}-{}'.format(datetime.strftime(tc.et2datetime(self.etstart[iii]), fmt),
                                     datetime.strftime(tc.et2datetime(self.etstop[iii]), fmt))
                ax.set_title(tmp)
                ax.set_xticks([])
                ax.set_yticks([])
                if not iii:
                    low = self.calib[self.calib['Channel_Index'].astype(int)==ttt]['Low']
                    hi = self.calib[self.calib['Channel_Index'].astype(int)==ttt]['Hi']
                    fmt = '%Y-%j'
                    tmp = '{}, {}-{} keV'.format(datetime.strftime(tc.et2datetime(self.etstart[iii]), fmt),
                           int(low), int(hi))
                    plt.suptitle(tmp, y=0.93, fontsize=15)
                    plt.colorbar(quad, cax=plt.subplot(gs[0,-1]))
            fmt = '%Y_%j'
            tmp = datetime.strftime(tc.et2datetime(self.etstart[0]), fmt)
            plt.savefig('{}/inca_{}_{}_{}.png'.format(plotsavepath, tmp, int(ttt), species), bbox_inches='tight', dpi=500)
            plt.close()
            
    def plotLarge(self, plotsavepath, species='H', fp_intens=None, ettimes=None, eqward=None):
        print('Plotting large image...')
        fmt = '%Y_%j'
        tmp = datetime.strftime(tc.et2datetime(self.etstart[0]), fmt)
        plotsavepath = '{}/{}'.format(plotsavepath, tmp)
        if not os.path.exists(plotsavepath):
            os.makedirs(plotsavepath)
            
        if (not fp_intens is None) and (not ettimes is None):
            plot_fp = True
        else:
            plot_fp = False
            
        data = np.copy(self.images)
        data[data==0] = np.nan
            
        cols = data[::self.alt].shape[0]
        rows = 6
        tttiter = iter(range(rows))
        
        if cols > 16:
            return False
        
        gs = gridspec.GridSpec(rows+2 if plot_fp else rows, cols+2, width_ratios=(1,)*cols+(0.1,0.1,),
                               height_ratios=(1,)*rows+(0.2,1.5,) if plot_fp else (1,)*rows,
                               wspace=0.07, hspace=0.07)
        fig = plt.figure()
        mult = 0.35
        fig.set_size_inches(mult*3*(cols+0.2), mult*4*(rows+1.7))
        
        for ttt in range(1, data.shape[1]-1):
            cmax = self.lim[ttt,1]
            cmin = self.lim[ttt,0]
            thisrow = next(tttiter)
            
            for iii in range(data[::self.alt].shape[0]):
                ax = plt.subplot(gs[thisrow, iii])
                ax.set_facecolor('dimgrey')
                cont = ax.contour(self.pamaps[::self.alt][iii,:,:],
                                  levels=np.arange(30,180,30, dtype=np.int),
                                  colors='gold')
                ax.clabel(cont, inline=1, fontsize=8, fmt='%d$^\circ$')
                quad = ax.pcolormesh(data[::self.alt][iii,ttt,:,:], cmap=cmap_UV)
                quad.set_norm(mcolors.LogNorm(cmin, cmax))
                if not thisrow:
                    fmt = '%H:%M'
                    tmp = '{}'.format(datetime.strftime(tc.et2datetime(self.etstart[::self.alt][iii]), fmt))
                    ax.set_title(tmp)
                ax.set_xticks([])
                ax.set_yticks([])
                if not iii:
                    low = self.calib[self.calib['Channel_Index'].astype(int)==ttt]['Low']
                    hi = self.calib[self.calib['Channel_Index'].astype(int)==ttt]['Hi']
                    fmt = '%Y-%j'
                    tmp = '{}-{} keV'.format(int(low), int(hi))
                    ax.set_ylabel(tmp)
                    plt.colorbar(quad, cax=plt.subplot(gs[thisrow,-1]))
                        
            fmt = '%Y_%j'
            tmp = datetime.strftime(tc.et2datetime(self.etstart[0]), fmt)
        plt.text(0.96, 0.6, 'Differential particle flux (1/s/sr/cm$^2$/keV)',
                 va='center', ha='right', rotation=270, fontsize=12,
                 transform=fig.transFigure, color='k')
        
        if plot_fp:
            ax = plt.subplot(gs[-1, :-2])
            ax.plot(tc.et2datetime(ettimes), fp_intens, 'k-', lw=2)
            ax.set_xlim(tc.et2datetime([self.etstart[0], self.etstop[-1]]))
            ax.set_ylim([0, 1.15*np.nanmax(fp_intens)])
            ax.set_ylabel('Intensity (kR)')
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
            ax.grid('on')
            if not eqward is None:
                left = 'poleward' if eqward else 'equatorward'
                right = 'equatorward' if eqward else 'poleward'
                ax.text(0.05, 0.9, left, ha='left', va='top', fontweight='bold',
                        transform = ax.transAxes)
                ax.text(0.95, 0.9, right, ha='right', va='top', fontweight='bold',
                        transform = ax.transAxes)
            ax.set_xlabel('UTC')
        
        plt.savefig('{}/inca_{}_{}.png'.format(plotsavepath, tmp, species), bbox_inches='tight', dpi=500)
        plt.savefig('{}/inca_{}_{}.pdf'.format(plotsavepath, tmp, species), bbox_inches='tight')
        plt.savefig('{}/inca_{}_{}_lowres.png'.format(plotsavepath, tmp, species), bbox_inches='tight', dpi=200)
        plt.close()
        return True
    
    def plotLargeSunangle(self, plotsavepath):
        print('Plotting large sunangle image...')
        fmt = '%Y_%j'
        tmp = datetime.strftime(tc.et2datetime(self.etstart[0]), fmt)
        plotsavepath = '{}/{}'.format(plotsavepath, tmp)
        if not os.path.exists(plotsavepath):
            os.makedirs(plotsavepath)
            
        dirs = np.copy(self.viewdirs_cent_KSM)
        sundirection = np.array([1,0,0])
        data = np.zeros((dirs.shape[0], 16, 16))
        for iii in range(data.shape[0]):
            for jjj in range(16):
                for kkk in range(16):
                    tmp = dirs[iii,jjj,kkk,:]
                    data[iii,jjj,kkk] = np.degrees(np.arccos(np.dot(sundirection, tmp)))
            
        cols = data.shape[0]
        
        gs = gridspec.GridSpec(1, cols+2, width_ratios=(1,)*cols+(0.1,0.1,),
                               wspace=0.07, hspace=0.07)
        fig = plt.figure()
        mult = 0.35
        fig.set_size_inches(mult*3*cols, mult*4)
        
        for iii in range(data.shape[0]):
            ax = plt.subplot(gs[0, iii])
            ax.set_facecolor('dimgrey')
            cont = ax.contour(self.pamaps[iii,:,:],
                              levels=np.arange(30,180,30, dtype=np.int),
                              colors='gold')
            ax.clabel(cont, inline=1, fontsize=8, fmt='%d$^\circ$')
            quad = ax.pcolormesh(data[iii,:,:], cmap=cmap_UV,
                                 vmin=0, vmax=180)
            fmt = '%H:%M'
            tmp = '{}'.format(datetime.strftime(tc.et2datetime(self.etstart[iii]), fmt))
            ax.set_title(tmp)
            ax.set_xticks([])
            ax.set_yticks([])
            if not iii:
                plt.colorbar(quad, cax=plt.subplot(gs[0,-1]))
                    
        fmt = '%Y_%j'
        tmp = datetime.strftime(tc.et2datetime(self.etstart[0]), fmt)
        plt.text(0.93, 0.5, 'Angle to sun',
                 va='center', ha='right', rotation=270, fontsize=12,
                 transform=fig.transFigure, color='k')
        plt.savefig('{}/inca_{}_sunangle.png'.format(plotsavepath, tmp), bbox_inches='tight', dpi=500)
        plt.savefig('{}/inca_{}_sunangle.pdf'.format(plotsavepath, tmp), bbox_inches='tight')
        plt.savefig('{}/inca_{}_sunangle_lowres.png'.format(plotsavepath, tmp), bbox_inches='tight', dpi=200)
        plt.close()
        return True
        
        
if __name__=='__main__':
    inio = INCAPA()
    #inio.setStart(2017,8,15)
    # keep dt below 2-3 hours to avoid massively sized plots
    inio.setStart(2017,9,11,15, dt=1.5)
    inio.update(charged = 0)
    
    #%%
    # =============================================================================
    # Plotting all images for each energy level
    # =============================================================================
    
    #    
    #for ttt in range(8):
    #    fig = plt.figure()
    #    ax = plt.subplot()
    #    quad = ax.pcolormesh(inio.padists[:,ttt,:].T)
    #    plt.colorbar(quad)
    #    plt.show()
    
    
    

