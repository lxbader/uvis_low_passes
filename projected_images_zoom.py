# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime
import get_image
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
cd = cassinipy.CassiniData(datapath)
samplepath = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_LOW_PASSES_RES8'

#%%
files = ['2017_059T10_50_03.fits',
         '2017_167T21_17_11.fits']

tmin = [110, 10]
tmax = [240, 75]
rmin = [5, 10]
rmax = [30, 25]

savepath = '{}/UVIS_low_passes/paper'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)
    

fig = plt.figure()
mult = 5
fig.set_size_inches(mult*2.2, mult)
gs = gridspec.GridSpec(1,len(files)+1, width_ratios=(1,0.4,)+(0.05,), wspace=0)

for panel in range(0,len(files)):
    ax = plt.subplot(gs[0,panel], projection='polar')
    
    samplefile = '{}/{}'.format(samplepath, files[panel])
    
    fmt = '%Y_%jT%H_%M_%S'
    thisdt = datetime.strptime(samplefile.split('\\')[-1].split('/')[-1].split('.')[0], fmt)
    thiset = tc.datetime2et(thisdt)
    thisind = (imglist['ET_START']-thiset).abs().argmin()
    
    data, _, _ = get_image.getRedUVIS(samplefile)
    data[data<0.1] = 0.1
    thisimg = imglist.loc[thisind]
    fmt = '%Y-%j, %H:%M:%S'
    title = '{}\n{}$\,$s, {}'.format(datetime.strftime(thisdt, fmt), thisimg['EXP'], thisimg['HEMISPHERE'])
    
    # plot
    KR_MIN=0.5
    KR_MAX=30
    
    lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
    quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_facecolor('gray')
    
    # plot colorbar
    if not panel:
        cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,-1]), extend='both')
        cbar.set_label('Intensity (kR)', labelpad=10, fontsize=11, rotation=270)
        cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,KR_MAX+1,10)))
        cbar.ax.tick_params(labelsize=10)
    
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        tmp = np.degrees(ticks[iii])
        if tmp > tmax[panel]:
            continue
        else:
            if tmp < tmin[panel]%360:
                continue
        txt = ax.text(ticks[iii], rmax[panel]-3, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                      ha='center',va='center', zorder=5)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    
#    # PPO phase angle
#    ppo_n = thisimg['PPO_PHASE_N']
#    ppo_s = thisimg['PPO_PHASE_S']
#    if np.isfinite(ppo_n):
#        ax.plot([ppo_n,ppo_n], [0,30], color=myred, lw=2, zorder=5)
#        ax.plot([ppo_n,ppo_n], [0,30], color='w', lw=3, zorder=3)
#        txt = ax.text(ppo_n+0.12, 27, 'N', color='w', fontsize=14, fontweight='bold',
#                      ha='center',va='center')
#        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myred)])
#    if np.isfinite(ppo_s):
#        ax.plot([ppo_s,ppo_s], [0,30], color=myblue, lw=2, zorder=5)
#        ax.plot([ppo_s,ppo_s], [0,30], color='w', lw=3, zorder=3)
#        txt = ax.text(ppo_s+0.12, 27, 'S', color='w', fontsize=14, fontweight='bold',
#                      ha='center',va='center')
#        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myblue)])
    # Cassini orbit
    delta = 3600*24*1
    _, colat_n, colat_s, loct = cd.get_ionfootp(np.linspace(thiset-delta, thiset+delta, num=2000))
    ax.plot(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s,
            c='w', ls='-', lw=1.5, zorder=2)
    # Cassini orbit direct
    _, colat_n, colat_s, loct = cd.get_ionfootp(np.linspace(thisimg['ET_START'],
                                                            thisimg['ET_STOP'], num=1000))
    ax.plot(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s,
            c='crimson', ls='-', lw=1.5, zorder=3)
    # arrow
    tmp = (thisimg['ET_START']+thisimg['ET_STOP'])/2
    tmp = [tmp-120, tmp+120]
    _, colat_n, colat_s, loct = cd.get_ionfootp(tmp)
    colat = colat_n if thisimg['HEMISPHERE']=='North' else colat_s
    arrowstyle = '->,head_length=9, head_width=3.75'
    ax.add_patch(patches.FancyArrowPatch((loct[0]/12*np.pi, colat[0]),
                                       (loct[1]/12*np.pi, colat[1]),
                                       arrowstyle=arrowstyle,
                                       color='crimson',
                                       linewidth=2,
                                       zorder=3))

#    # Cassini location print
#    _, rs, lat, loct = cd.get_locations([thiset], refframe='KRTP')
#    ax.text(0, 0, '{0:.2f} Rs\n{1:.2f}$^\circ$ {2}\n{3:.2f} LT'.format(
#                    rs, np.abs(lat), thisimg['HEMISPHERE'][0], loct),
#            transform=ax.transAxes)
#
#    txt = ax.text(0, 1, '({})'.format('abcd'[panel]),
#                  transform=ax.transAxes, ha='center', va='center',
#                  color='k', fontweight='bold', fontsize=20)
#    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
    
    ax.set_xticks(ticks)
    ax.set_xticks(np.linspace(0, 2*np.pi, 25), minor=True)
    ax.set_xticklabels([])
    
    ax.set_yticks([10,20,30])
    ax.set_yticklabels([])
    
    ax.grid('on', color='0.8', linewidth=1, which='major')
    ax.grid('on', color='0.8', linewidth=0.5, which='minor')
    
#    ax.set_ylim([10,30])
    ax.set_rorigin(0)
    ax.set_rmax(rmax[panel])
    ax.set_rmin(rmin[panel])
    
    ax.set_title(title)
    
    ax.set_theta_zero_location("N")
    ax.set_thetamin(tmin[panel])
    ax.set_thetamax(tmax[panel])

                
#plt.savefig('{}/projections_2_zoom.png'.format(savepath, tmp), bbox_inches='tight', dpi=400)
plt.savefig('{}/projections_2_zoom_lowres.png'.format(savepath, tmp), bbox_inches='tight', dpi=200)
plt.close()

